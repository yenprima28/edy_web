-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2017 at 05:17 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sma_edy_2`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id` int(10) UNSIGNED NOT NULL,
  `nis` varchar(20) NOT NULL,
  `ijin` int(2) UNSIGNED NOT NULL,
  `alpa` int(2) UNSIGNED NOT NULL,
  `sakit` int(2) UNSIGNED NOT NULL,
  `smt` int(2) UNSIGNED NOT NULL,
  `tahun` smallint(4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id`, `nis`, `ijin`, `alpa`, `sakit`, `smt`, `tahun`) VALUES
(5, '101', 1, 1, 1, 1, 2017),
(6, '114', 1, 3, 2, 1, 2017),
(8, '115', 1, 3, 3, 1, 2017);

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(30) NOT NULL COMMENT 'nis/nip',
  `password` varchar(64) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id`, `username`, `password`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'administrator'),
(2, '222', '7d8fecd6998e07a85463a6275a6608bc', 'guru'),
(3, '223', '7d8fecd6998e07a85463a6275a6608bc', 'guru'),
(4, '224', '1ae8dee426dae787794f7e750ba2eedd', 'pengajar'),
(5, '114', '827ccb0eea8a706c4c34a16891f84e7b', 'siswa'),
(6, '1950', 'a6c7ddbbecabd6d3feba7d62f464475b', 'guru'),
(7, '1950', 'fc4e56a92ea43a155ad4232d86d6f555', 'guru'),
(8, '113', 'ac43724f16e9241d990427ab7c8f4228', 'siswa'),
(9, '195707111984031008', '8388cb6a6cd23f12ae799df2f78138ed', 'guru'),
(10, '0002533563', '1fdfd362a49bfc05c5f9780f98dd626e', 'siswa'),
(12, '12331231231', 'a8f0c4dca844c75dc1af1fb45c8c6e7f', 'guru'),
(13, '0002533563', 'a8f0c4dca844c75dc1af1fb45c8c6e7f', 'siswa'),
(14, '999', '38f43f040dfd3a4347f2bd0b34834f45', 'siswa'),
(15, '998', '38f43f040dfd3a4347f2bd0b34834f45', 'siswa'),
(16, '990', '81aab59c53a4caa340702f880e8f6000', 'pengajar');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ekskul`
--

CREATE TABLE `ekskul` (
  `id_ekskul` varchar(5) NOT NULL,
  `ekskul` varchar(100) NOT NULL,
  `jadwal` date DEFAULT NULL,
  `lokasi` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ekskul`
--

INSERT INTO `ekskul` (`id_ekskul`, `ekskul`, `jadwal`, `lokasi`) VALUES
('E0001', 'Futsal', NULL, NULL),
('E0002', 'Sepak Bola', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ekskul_siswa`
--

CREATE TABLE `ekskul_siswa` (
  `id` int(10) UNSIGNED NOT NULL,
  `nis` varchar(12) NOT NULL,
  `id_ekskul` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ekskul_siswa`
--

INSERT INTO `ekskul_siswa` (`id`, `nis`, `id_ekskul`) VALUES
(1, '998', 'E0001');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kd_jabatan` varchar(5) NOT NULL,
  `jabatan` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kd_jabatan`, `jabatan`) VALUES
('J001', 'GURU'),
('J002', 'WALI KELAS');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan_pengajar`
--

CREATE TABLE `jabatan_pengajar` (
  `id` int(10) UNSIGNED NOT NULL,
  `nip` varchar(26) NOT NULL,
  `kd_jabatan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan_pengajar`
--

INSERT INTO `jabatan_pengajar` (`id`, `nip`, `kd_jabatan`) VALUES
(1, '222', 'J001'),
(2, '222', 'J002'),
(3, '223', 'J001'),
(4, '224', 'J001'),
(5, '990', 'j002');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `kd_kelas` varchar(5) NOT NULL,
  `kelas` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kd_kelas`, `kelas`) VALUES
('K001', '1 IPA 1'),
('K002', '1 IPA 2'),
('K003', '1 IPA 3'),
('K004', '2 IPA 2');

-- --------------------------------------------------------

--
-- Table structure for table `kelas_siswa`
--

CREATE TABLE `kelas_siswa` (
  `id` int(10) UNSIGNED NOT NULL,
  `kd_kelas` varchar(5) NOT NULL,
  `nis` varchar(20) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `smt` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_siswa`
--

INSERT INTO `kelas_siswa` (`id`, `kd_kelas`, `nis`, `tahun`, `smt`) VALUES
(2, 'K001', '111', '2017', 1),
(4, 'K002', '114', '2017', 1),
(5, 'K001', '112', '2017', 1),
(6, 'K001', '113', '2017', 1),
(7, 'K001', '115', '2017', 1),
(8, 'K003', '114', '2017', 2),
(9, 'K002', '115', '2017', 1),
(10, 'K002', '101', '2017', 1),
(11, 'K004', '999', '2017', 0),
(12, 'K004', '998', '2017', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `kd_mapel` varchar(6) NOT NULL,
  `mapel` varchar(30) NOT NULL,
  `kkm` int(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`kd_mapel`, `mapel`, `kkm`) VALUES
('M001', 'MATEMATIKA', 75),
('M002', 'FISIKA', 70),
('M003', 'KIMIA', 75);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_siswa`
--

CREATE TABLE `nilai_siswa` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_mapel` varchar(5) NOT NULL,
  `pengetahuan_grade` char(1) DEFAULT NULL,
  `keterampilan_grade` char(1) DEFAULT NULL,
  `pengetahuan_angka` float DEFAULT NULL,
  `keterampilan_angka` float DEFAULT NULL,
  `nis` varchar(10) NOT NULL,
  `tahun` varchar(10) NOT NULL,
  `semester` int(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_siswa`
--

INSERT INTO `nilai_siswa` (`id`, `id_mapel`, `pengetahuan_grade`, `keterampilan_grade`, `pengetahuan_angka`, `keterampilan_angka`, `nis`, `tahun`, `semester`) VALUES
(1, 'M001', 'A', 'A', 99, 99, '111', '2017', 1),
(2, 'M001', 'A', 'A', 98, 89, '112', '2017', 1),
(3, 'M001', 'A', 'A', 98, 89, '114', '2017', 1),
(4, 'M002', 'A', 'A', 90, 89, '114', '2017', 1),
(12, 'M001', 'B', 'C', 82, 76, '115', '2017', 1),
(13, 'M001', 'B', 'C', 82, 76, '113', '2017', 1),
(14, 'M001', 'D', 'B', 14, 80, '101', '2017', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengajar`
--

CREATE TABLE `pengajar` (
  `nip` varchar(30) NOT NULL,
  `nama` varchar(80) NOT NULL,
  `jk` char(1) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `ttl` varchar(11) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajar`
--

INSERT INTO `pengajar` (`nip`, `nama`, `jk`, `alamat`, `ttl`, `gambar`) VALUES
('12331231231', 'asdasd', 'L', 'asdasd', '2000-02-29', 'Chrysanthemum.jpg'),
('1950', 'asd', 'L', 'asd', '1950-05-14', 'Patrick1.png'),
('195707111984031008', 'karlin', 'L', 'asd', '1957-07-11', 'Desert.jpg'),
('222', 'asd', 'L', 'asd', '2017-06-11', 'Penguins.jpg'),
('223', 'asd1', 'L', 'asdd', '2017-06-11', 'Penguins.jpg'),
('224', 'asd', 'L', 'asd', '2017-06-16', 'Lighthouse.jpg'),
('990', 'qqq', 'P', 'qqq', '2017-06-16', '');

-- --------------------------------------------------------

--
-- Table structure for table `pengajar_mapel`
--

CREATE TABLE `pengajar_mapel` (
  `id` int(10) UNSIGNED NOT NULL,
  `kd_mapel` varchar(5) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `kelas` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajar_mapel`
--

INSERT INTO `pengajar_mapel` (`id`, `kd_mapel`, `nip`, `kelas`) VALUES
(2, 'M001', '222', 'K001'),
(3, 'M002', '223', 'K001'),
(10, 'M001', '224', 'K001'),
(11, 'M001', '224', 'K002'),
(12, 'M002', '222', 'K001');

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `nis` varchar(20) NOT NULL,
  `prestasi` varchar(100) NOT NULL,
  `detail_prestasi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`id`, `nis`, `prestasi`, `detail_prestasi`) VALUES
(1, '101', 'asd', 'asd'),
(2, '101', 'asd 2', 'asd'),
(3, '101', 'asd3333', 'asdasdqwed asdasdw'),
(4, '998', 'asd3333', 'asdasdqwed asdasdw');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nis` varchar(20) NOT NULL,
  `nama` varchar(80) NOT NULL,
  `ttl` date NOT NULL,
  `jk` char(1) NOT NULL,
  `alamat` varchar(254) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nis`, `nama`, `ttl`, `jk`, `alamat`, `gambar`) VALUES
('0002533563', 'Achmad Robby', '2000-02-29', 'L', 'asd', 'Chrysanthemum.jpg'),
('101', 'ppp', '2017-06-16', 'L', 'asd', 'asd.jpg'),
('111', 'siswa111', '2017-06-11', 'L', 'asd', 'asd.jpg'),
('112', 'siswa112', '2017-06-11', 'L', 'asd', 'asd.jpg'),
('113', 'ert', '2017-07-14', 'L', 'asd', 'Real-Madrid-FC-vector-logo.png'),
('114', 'YEN PRIMA RUEH', '2017-07-04', 'L', 'asd', 'Kim_Beom-p21.jpg'),
('115', 'YEN PRIMA RUEH 2', '2017-07-04', 'L', 'asd', 'Kim_Beom-p21.jpg'),
('998', 'iso', '2017-08-25', 'L', 'asd', ''),
('999', 'sni', '2017-08-25', 'P', 'asd', '');

-- --------------------------------------------------------

--
-- Table structure for table `smt_aktif`
--

CREATE TABLE `smt_aktif` (
  `id` int(10) UNSIGNED NOT NULL,
  `smt` int(1) UNSIGNED NOT NULL,
  `tahun_pelajaran` varchar(4) NOT NULL,
  `status` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smt_aktif`
--

INSERT INTO `smt_aktif` (`id`, `smt`, `tahun_pelajaran`, `status`) VALUES
(1, 2, '2017', 'Tidak Aktif'),
(2, 3, '2017', 'Tidak Aktif'),
(3, 1, '2017', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `wali_kelas`
--

CREATE TABLE `wali_kelas` (
  `kd_kelas` varchar(5) NOT NULL,
  `nip` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wali_kelas`
--

INSERT INTO `wali_kelas` (`kd_kelas`, `nip`) VALUES
('K001', '222'),
('K002', '224');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nis` (`nis`);

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ekskul`
--
ALTER TABLE `ekskul`
  ADD PRIMARY KEY (`id_ekskul`);

--
-- Indexes for table `ekskul_siswa`
--
ALTER TABLE `ekskul_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nis` (`nis`),
  ADD KEY `id_ekskul` (`id_ekskul`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kd_jabatan`);

--
-- Indexes for table `jabatan_pengajar`
--
ALTER TABLE `jabatan_pengajar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nip` (`nip`),
  ADD KEY `kd_jabatan` (`kd_jabatan`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kd_kelas`);

--
-- Indexes for table `kelas_siswa`
--
ALTER TABLE `kelas_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kd_kelas` (`kd_kelas`),
  ADD KEY `nis` (`nis`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`kd_mapel`);

--
-- Indexes for table `nilai_siswa`
--
ALTER TABLE `nilai_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_mapel` (`id_mapel`),
  ADD KEY `nis` (`nis`);

--
-- Indexes for table `pengajar`
--
ALTER TABLE `pengajar`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `pengajar_mapel`
--
ALTER TABLE `pengajar_mapel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kd_mapel` (`kd_mapel`),
  ADD KEY `kelas` (`kelas`),
  ADD KEY `nip` (`nip`) USING BTREE;

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nis` (`nis`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `smt_aktif`
--
ALTER TABLE `smt_aktif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wali_kelas`
--
ALTER TABLE `wali_kelas`
  ADD PRIMARY KEY (`kd_kelas`),
  ADD KEY `nip` (`nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ekskul_siswa`
--
ALTER TABLE `ekskul_siswa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jabatan_pengajar`
--
ALTER TABLE `jabatan_pengajar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kelas_siswa`
--
ALTER TABLE `kelas_siswa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `nilai_siswa`
--
ALTER TABLE `nilai_siswa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `pengajar_mapel`
--
ALTER TABLE `pengajar_mapel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `smt_aktif`
--
ALTER TABLE `smt_aktif`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `absensi`
--
ALTER TABLE `absensi`
  ADD CONSTRAINT `absensi_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ekskul_siswa`
--
ALTER TABLE `ekskul_siswa`
  ADD CONSTRAINT `ekskul_siswa_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ekskul_siswa_ibfk_2` FOREIGN KEY (`id_ekskul`) REFERENCES `ekskul` (`id_ekskul`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jabatan_pengajar`
--
ALTER TABLE `jabatan_pengajar`
  ADD CONSTRAINT `jabatan_pengajar_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `pengajar` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jabatan_pengajar_ibfk_2` FOREIGN KEY (`kd_jabatan`) REFERENCES `jabatan` (`kd_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kelas_siswa`
--
ALTER TABLE `kelas_siswa`
  ADD CONSTRAINT `kelas_siswa_ibfk_1` FOREIGN KEY (`kd_kelas`) REFERENCES `kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_siswa_ibfk_2` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nilai_siswa`
--
ALTER TABLE `nilai_siswa`
  ADD CONSTRAINT `nilai_siswa_ibfk_1` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`kd_mapel`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nilai_siswa_ibfk_2` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengajar_mapel`
--
ALTER TABLE `pengajar_mapel`
  ADD CONSTRAINT `pengajar_mapel_ibfk_1` FOREIGN KEY (`kd_mapel`) REFERENCES `mapel` (`kd_mapel`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengajar_mapel_ibfk_2` FOREIGN KEY (`nip`) REFERENCES `pengajar` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengajar_mapel_ibfk_3` FOREIGN KEY (`kelas`) REFERENCES `kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD CONSTRAINT `prestasi_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wali_kelas`
--
ALTER TABLE `wali_kelas`
  ADD CONSTRAINT `wali_kelas_ibfk_1` FOREIGN KEY (`kd_kelas`) REFERENCES `kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wali_kelas_ibfk_2` FOREIGN KEY (`nip`) REFERENCES `pengajar` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

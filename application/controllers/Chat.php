<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {
	
	public function __construct(){
		parent:: __construct();
		date_default_timezone_set('Asia/Jakarta');
	}
	
	function tampil_pesan(){
		
		$this->db->limit(50);
		$this->db->order_by('id', 'ASC');
		$pesan = $this->db->get('chat')->result();
		
		foreach($pesan as $ls){
			
			echo '<span style="color: #a9a9a9;font-size: 14px">[',$ls->waktu,']:</span>';
			echo '<span style="color: #a9a9a9;font-size: 14px">[nis/nik]:</span>';
			echo '[<b>',$ls->username,'</b>]';
			echo ' : ',$ls->pesan;
			echo '<br/>';			
			
		}
		
		
		
	}
	
	public function kirim_pesan(){
		
		//$this->load->library('form_validation');		
        //
		////$this->form_validation->set_rules('nis', 'NIS', 'required|trim');
		//$this->form_validation->set_rules('chat_text', 'Nama Lengkap', 'required|trim');		
        //
		//if ($this->form_validation->run() === FALSE)
		//{
		//	echo 'ERROR: Gagal pesan dikirim';
        //
		//}
		//else
		//{									
			
			//$pesan = $this->input->post('')
			
			$data = array(
				'username' => $_SESSION['username'],
				//'pesan' => 'asd',
				'pesan' => $_POST['pesans'],
				'waktu' => date('Y-m-d H:i:s')
			);
			
			$this->db->insert('chat', $data);
			
		//}
	}
	
	public function siswa()
	{
		$data['title'] = 'Chatting';
		$this->load->view('template/header', $data);
		$this->load->view('template/side-menu-siswa');
		$this->load->view('siswa/chat');
		$this->load->view('template/footer');	
	}
	
	public function guru()
	{	
		$this->load->view('pengajar/chat');	
	}
	
	public function wali_kelas()
	{	
		$this->load->view('siswa/chat');	
	}
	
}
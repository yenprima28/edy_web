<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wali_kelas extends CI_Controller {
	
    public $smt_aktif;
    public $tahun_smt;
    public $kelas_ampu;
    
    
	public function __construct()
	{
		parent::__construct();
		//load model disini
		$this->load->model(
			array
			(				
				'walikelas_model',
                'semester_model',
                'Pengajar_model'
			)
		);
		// $this->load->model('Pengajar_model');
		//load helper dan library disini
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('form_validation');
        
        //fungsi
        $this->set_semester_aktif();
	}
    
    public function set_semester_aktif()
    {
        $data['semester_aktif'] = $this->semester_model->get_semester_aktif();
        $data['kelas_ampu'] = $this->walikelas_model->get_kelas_ampu_by_nip_walikelas();
        
        $this->smt_aktif = $data['semester_aktif'][0]->smt;
        $this->tahun_smt = $data['semester_aktif'][0]->tahun_pelajaran;
        $this->kelas_ampu = $data['kelas_ampu']['kd_kelas'];
        
        $this->session->set_userdata('smt_aktif', (int) $this->smt_aktif);
        
        //var_dump( $data['semester_aktif'][0]->smt );
        
        // var_dump($this->kelas_ampu);
        // var_dump($data['kelas_ampu']);
        
        //echo $this->smt_aktif;
        //echo $this->tahun_smt;
        
        // var_dump( $data['semester_aktif'] );
    }
	
	public function index()
	{
		$this->load->view('template/header');
		$this->load->view('template/side-menu-walikelas');
		$this->load->view('wali_kelas/index');
		$this->load->view('template/footer');
		
		//var_dump($this->session->username);
	}
	
	public function kelola_naik_kelas()
	{
		$data['title'] = 'Kelola Kenaikan Kelas Siswa';
		$data['data_siswa'] = $this->walikelas_model->get_data_siswa_diampu($this->smt_aktif, $this->tahun_smt);		
         // $data['nama_kelas'] = $data['data_siswa'][0]->kelas;        
        
        //var_dump($data['nama_kelas']);
        
		if( $data['data_siswa'] == null )
        {
            $data['nama_kelas'] = '-';
        } 
        else 
        {
            $data['nama_kelas'] = $data['data_siswa'][0]->kelas;        
        }
		
		//var_dump($data['data_siswa']);
		
		$this->load->view('template/header');
		$this->load->view('template/side-menu-walikelas');
		$this->load->view('wali_kelas/kenaikan_kelas', $data);
		$this->load->view('template/footer');
	}
	
	public function tampil_data_siswa_ampu()
	{
		//$data['no'] = 1;
		if( $data['data_siswa'] = $this->walikelas_model->get_data_siswa_diampu($this->smt_aktif, $this->tahun_smt) )
        {
            echo json_encode($data);            
        }
        else
        {            
            echo json_encode('Data gagal di load');
        }
		
	}
	
    public function get_mapel_list()
    {
        $data['list_mapel'] = $this->walikelas_model->get_all_mapel();
        $data['smt_tahun'] = array
        (
            'smt' => $this->smt_aktif,
            'tahun' => $this->tahun_smt,
            'kelas_ampu' => $this->kelas_ampu
        );
        
        echo json_encode($data);
    }
    
    public function download_nilai()
    {        
        $data['nilai_siswa'] = $this->walikelas_model->get_all_nilai();
        
        echo json_encode($data);
    }
    
	public function kelola_nilai_siswa()
	{
		$data['title'] = 'Kelola Nilai Siswa';
		$data['no'] = 1;		
        //$data['mapel'] = json_encode( $this->walikelas_model->get_all_mapel() );
		//$data['data_siswa'] = $this->walikelas_model->get_nilai_siswa_perkelas_by_mapel($this->smt_aktif, $this->tahun_smt, $data['mapel'][0]->kd_mapel);
        $data['nama_kelas'] = '-';        
        
        $data['smt_aktif'] = $this->smt_aktif;
		
		//var_dump($data);
		
		$this->load->view('template/header');
		$this->load->view('template/side-menu-walikelas');
		$this->load->view('wali_kelas/kelola_nilai_siswa', $data);
		$this->load->view('template/footer');
	}
	
	public function kelola_absensi_siswa()
	{
		$data['title'] = 'Kelola Absensi Siswa';
		$data['no'] = 1;
		//$data['data_siswa'] = $this->walikelas_model->get_data_siswa_diampu($this->smt_aktif, $this->tahun_smt);
		//$data['nama_kelas'] = $data['data_siswa'][0]->kelas;
		$data['nama_kelas'] = '-';
		
		//var_dump($data['data_siswa']);
		
		$this->load->view('template/header');
		$this->load->view('template/side-menu-walikelas');
		$this->load->view('wali_kelas/kelola_absensi_siswa', $data);
		$this->load->view('template/footer');
	}    
	
	public function get_data_kelas()
	{
		$data['daftar_kelas'] = $this->walikelas_model->get_data_kelas();
		
		echo json_encode($data);
	}
    
    public function naikkan_kelas_siswa()
    {
        $bool = false;
        
        if( $this->walikelas_model->naik_kelas($this->smt_aktif, $this->tahun_smt) )
        {
            $bool = true;
        }
        
        echo json_encode($bool);
    }
	
    public function get_data_siswa_diampu()
    {
        $data = $this->walikelas_model->get_data_siswa_diampu($this->smt_aktif, $this->tahun_smt);
        
        echo json_encode($data);
        
        //var_dump( $data );
    }
    
    public function get_absensi_siswa()
    {
        $data = $this->walikelas_model->get_absensi($this->smt_aktif, $this->tahun_smt);
        
        echo json_encode($data);        
    }
    
     public function get_ekskul_siswa()
    {
        $data = $this->walikelas_model->get_siswa_ekskul();
        
        echo json_encode($data);        
    }
    
    public function get_smt_aktif()
    {
        $array = array
        (
            'smt' => $this->smt_aktif,
            'tahun' => $this->tahun_smt
        );
        
        echo json_encode($array);
    }
    
    public function tambah_data_absensi()
    {
        if( $this->walikelas_model->insert_absen($this->smt_aktif, $this->tahun_smt) )
        {
            echo json_encode(true);
        }
        else
        {            
            echo json_encode(false);
        }
    }
    
    public function get_data_siswa()
    {
        $data = $this->walikelas_model->get_siswa();
        
        if( $data )
        {
            echo json_encode($data);
        }
        else 
        {           
            echo json_encode(false);
        }
    }
    
    public function kelola_ekskul_siswa()
    {
        $data['title'] = 'Kelola Eksul Siswa';
		$data['no'] = 1;
        $data['nama_kelas'] = '-';
        
        $data['smt_aktif'] = $this->smt_aktif;
		
		//var_dump($data);
		
		$this->load->view('template/header');
		$this->load->view('template/side-menu-walikelas');
		$this->load->view('wali_kelas/kelola_ekskul_siswa', $data);
		$this->load->view('template/footer');
    }
    
    public function tambah_data_ekskul()
    {
        if( $this->walikelas_model->insert_siswa_ekskul() )
        {
            echo json_encode(true);
        }
        else
        {            
            echo json_encode(false);
        }
    }
    
    public function get_all_ekskul()
    {
        $data = $this->walikelas_model->get_ekskul_list();

           echo json_encode($data);
    }
    
    public function kelola_prestasi_siswa()
    {
        $data['title'] = 'Kelola Prestasi Siswa';
		$data['no'] = 1;
        $data['nama_kelas'] = '-';
        
        $data['smt_aktif'] = $this->smt_aktif;
		
		//var_dump($data);
		
		$this->load->view('template/header');
		$this->load->view('template/side-menu-walikelas');
		$this->load->view('wali_kelas/kelola_prestasi_siswa', $data);
		$this->load->view('template/footer');
    }
    
    public function get_prestasi_siswa()
    {
        if( $data = $this->walikelas_model->get_prestasi() )
        {
            echo json_encode($data); 
        }
        else
        {
            echo json_encode(false);
        }
                
    }
    
    public function tambah_data_prestasi()
    {
        if( $this->walikelas_model->insert_prestasi() )
        {
            echo json_encode(true);
        }
        else
        {            
            echo json_encode(false);
        }
    }    
    
    public function guru_mode()
    {
        $this->session->set_userdata('level', 'guru');
        
        redirect('login');
    }
    
}

/**
	public function index()
	{
		if($this->cek_login_session() == FALSE){
			redirect('login/login_auth');
		}
		
		$data = array(
			'title' => 'Index',
			'title_dashboard' => 'Dashboard'			
		);
		
		$this->load->view('wali_kelas/index', $data);
	}
	
	public function cek_login_session(){
		
		if($this->session->level == ''){
			return FALSE;
		}
		
		return TRUE;
	}
	
	public function kelola_naik_kelas(){
		
		$data = array(
			'title' => 'Kenaikan Kelas',
			'title_dashboard' => 'Kenaikan Kelas'			
		);
		
		$this->load->view('wali_kelas/kenaikan_kelas', $data);
		
	}
	
	public function naik_kelas($id){
		
	}
	
	public function kelola_jurusan_siswa(){
		
	}
	
	public function atur_jurusan($id){
		
	}
	
	public function kelola_absensi_siswa(){
		$data = array(
			'title' => 'Absensi Siswa',
			'title_dashboard' => 'Kelola Absensi Siswa'			
		);
		
		$this->load->view('wali_kelas/kelola_absensi_siswa', $data);
	}
	
	public function absensi_siswa($id){
		
	}
	
	public function kelola_ekskul_siswa(){
		$data = array(
			'title' => 'Ekskul Siswa',
			'title_dashboard' => 'Kelola Ekskul Siswa'			
		);
		
		$this->load->view('wali_kelas/kelola_ekskul_siswa', $data);
	}
	
	public function atur_siswa_ekskul($id){
		
	}
	
	public function kelola_prestasi_siswa(){
		
		$data = array(
			'title' => 'Prestasi Siswa',
			'title_dashboard' => 'Kelola Prestasi Siswa'			
		);
		
		$this->load->view('wali_kelas/kelola_prestasi_siswa', $data);
		
	}
	
	public function atur_prestasi_siswa($id){
		
	}
	
}
**/

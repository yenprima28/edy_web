<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('siswa_model');
		$this->load->helper('url');
		$this->load->library('form_validation', 'session');
	}
	
	public function index()
	{
		//if($this->cek_login_session() == FALSE){
		//	redirect('login/login_auth');
		//}		
		
		$data['title'] = 'Index';		
		
		$this->load->view('template/header', $data);
		$this->load->view('template/side-menu-siswa');
		$this->load->view('siswa/index', $data);		
		$this->load->view('template/footer');
	}
	
	//public function cek_login_session(){
	//	
	//	if($this->session->level == ''){
	//		return FALSE;
	//	}
	//	
	//	return TRUE;
	//}
	
	public function profil()
	{
		$data['profil'] = $this->siswa_model->get_profil();
		$data['title'] = 'Profil Siswa';		
		
		$this->load->view('template/header', $data);
		$this->load->view('template/side-menu-siswa');
		$this->load->view('siswa/profil', $data);
		$this->load->view('template/footer');		
	}
	
	public function nilai_saya()
	{			
		$data['nilai'] = $this->siswa_model->get_nilai();
		$data['title'] = 'Raport Siswa';
		$data['no'] = 1;		
		
		//var_dump($data);
		//var_dump($_SESSION);
		
		$this->load->view('template/header', $data);
		$this->load->view('template/side-menu-siswa');
		$this->load->view('siswa/raport', $data);
		$this->load->view('template/footer');		
	}
    
    public function ganti_password()
    {
        $data['title'] = 'Ganti Password';
        
        $this->form_validation->set_rules('nis_nip', 'NIS', 'required|trim');
        $this->form_validation->set_rules('old_pass', 'Password Sekarang', 'required|trim');
        $this->form_validation->set_rules('new_pass', 'Password Baru', 'required|trim');
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header', $data);
            $this->load->view('template/side-menu-siswa');
            $this->load->view('siswa/ganti_password', $data);
            $this->load->view('template/footer');
		}
		else 
		{
			if( $this->siswa_model->ganti_password() )
			{				
				$this->load->view('template/header', $data);
				$this->load->view('template/side-menu-siswa');
				$this->load->view('admin/sukses');
				$this->load->view('siswa/ganti_password', $data);
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header', $data);
				$this->load->view('template/side-menu-siswa');
				$this->load->view('admin/gagal');
				$this->load->view('siswa/ganti_password', $data);
				$this->load->view('template/footer');
			}
			
		}
       
    }
    
    /**Prestasi**/
    
    public function prestasiku()
    {        
        $data['prestasi'] = $this->siswa_model->prestasiku();        
        
        $this->load->view('template/header');
		$this->load->view('template/side-menu-siswa');		
		$this->load->view('siswa/prestasiku', $data);
		$this->load->view('template/footer');
    }
    
    /**Prestasi**/
    
        /**Prestasi**/
    
    public function ekskulku()
    {        
        $data['prestasi'] = $this->siswa_model->ekskulku();
        
        $this->load->view('template/header');
		$this->load->view('template/side-menu-siswa');		
		$this->load->view('siswa/ekskulku', $data);
		$this->load->view('template/footer');
    }
    
    /**Prestasi**/

}

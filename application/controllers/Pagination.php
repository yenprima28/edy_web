<?php

class Pagination extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('pagination_model');
		
		$this->load->helper('url');		
		$this->load->library('pagination');		
	}
	
	public function view()
	{		
		//echo $this->pagination_model->count_siswa();
		
		$config['base_url']  = base_url('pagination/view');
		$config['total_rows'] = $this->pagination_model->count_siswa();
		$config['per_page']  = 2;
		$config['uri_segment']  = 3;
		//$config['display_pages'] = FALSE;		
		$config['full_tag_open'] = '<ul class="pagination">';		
		$config['full_tag_close'] = '</ul>';		
		$config['first_link'] = FALSE;		
		$config['last_link'] = FALSE;		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$this->pagination->initialize($config);
		
		$data['page'] = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
		$data['siswa'] = $this->pagination_model->get_siswa($config['per_page'], $data['page']);
		$data['links'] = $this->pagination->create_links();				
		
		$data['title'] = 'Pagination';
		$data['no'] = 1;
		$this->load->view('template/header', $data);
		$this->load->view('template/side-menu-admin');
		$this->load->view('pagination', $data);
		$this->load->view('template/footer');				
	}
	
}

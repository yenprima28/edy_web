<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends CI_Controller {
	
	public function __construct()
	{
		parent:: __construct();
		
		$this->load->model('Semester_model');		
		$this->load->model('Kelas_model');		
		$this->load->model('Pengajar_model');		
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->library('session');
	}
	
	public function index()
	{
		
		//if($this->cek_login_session() == FALSE){
		//	redirect('login/login_auth');
		//}
		
				
		$data['title'] = 'Index';		
		$data['semester_aktif'] = $this->Semester_model->get_semester_aktif();		
		
        // var_dump( $data );
        
		$this->load->view('template/header', $data);
		$this->load->view('template/side-menu-pengajar');		
		$this->load->view('pengajar/index', $data);
		$this->load->view('template/footer');
	}
	
	public function daftar_nilai_siswa()
	{
		//$data['title'] = 'Daftar Nilai Siswa';
		$data['mapel'] = $this->Pengajar_model->get_all_mapel_pengajar();	
				
		$this->load->view('template/header', $data);
		$this->load->view('template/side-menu-pengajar');		
		$this->load->view('pengajar/daftar_nilai_siswa', $data);
		$this->load->view('template/footer');		
	}
	
	public function get_daftar_kelas_pengajar_mapel()
	{				
		$data['kelas'] = $this->Pengajar_model->get_all_kelas_pengajar_by_mapel();	
		
		echo json_encode($data);
	}
	
	public function get_daftar_siswa_mapel_kelas_no_nilai()
	{
		//buat ujicoba
		$data['siswa'] = $this->Pengajar_model->get_siswa_mapel_kelas_null();			
		
		echo json_encode($data);
	}
	
	//menampilkan siswa yang sudah ada nilainya
	public function get_daftar_siswa_mapel_kelas()
	{
		//buat ujicoba
		$data['siswa'] = $this->Pengajar_model->get_siswa_mapel_kelas();			
		
		echo json_encode($data);
	}
	
	public function get_nama_siswa()
	{
		$data['siswa'] = $this->Pengajar_model->get_nama_siswa();
		
		echo json_encode($data);
	}
	
	public function get_nilai_siswa()
	{
		$data['nilai_siswa'] = $this->Pengajar_model->get_nilai_siswa_by_mapel();
		
		echo json_encode($data);
	}
	
	public function get_nilai_siswa_by_nis()
	{
		$data['nilai_siswa_nis'] = $this->Pengajar_model->get_nilai_siswa_by_nis();
		
		echo json_encode($data);
	}
	
	public function get_level_guru()
	{
		$data['level_guru'] = $this->Pengajar_model->get_level();
		
		var_dump($data);
		
		echo count($data['level_guru']);
	}
	
	public function input_nilai_siswa(){
	
		$data = array(
			'title' => 'Tambah Nilai Siswa',
			'title_dashboard' => 'Input Nilai Siswa',
			'error' => '',
			
			'nis' => $this->uri->segment(3)
		);
		
		//$sql = "SELECT nama_lengkap
		//		FROM siswa				
		//		WHERE siswa.nis = $data[nis]
		//		";
		
		$sql = "SELECT siswa.nama_lengkap, siswa.nis,
					nilai_siswa_mapel.pengetahuan_angka, nilai_siswa_mapel.pengetahuan_grade,
					nilai_siswa_mapel.keterampilan_angka, nilai_siswa_mapel.keterampilan_grade
				FROM siswa
				INNER JOIN nilai_siswa_mapel
				ON siswa.nis = nilai_siswa_mapel.nis
				WHERE nilai_siswa_mapel.nis = $data[nis]				
			   ";
		
		$data['siswa'] = $this->db->query($sql)->result();
		
		$this->load->view('pengajar/input_nilai_siswa', $data);
	
	}
	
	public function input_nilai_siswa_proses()
	{									
		$data['nilai'] = $this->Pengajar_model->input_nilai_siswa();
		
		$bool = FALSE;
		
		if( $data['nilai'] == TRUE )
		{
			$bool = TRUE;
		}
		
		echo json_encode($bool);
	}
	
	public function status_pengisian_nilai()
	{
		$data['status_pengisian'] = $this->Pengajar_model->count_siswa_by_mapel_kelas_smt_tahun();
		
		echo json_encode($data);
	}
	
	public function count_siswa_kelas()
	{
		$data['count_siswa_perkelas'] = $this->Pengajar_model->count_siswa_by_kelas();
		
		echo json_encode($data);
	}
	
    public function walikelas_mode()
    {
        // var_dump( $this->session );
        $this->session->set_userdata('level', 'wali kelas');
        
        redirect('login');
    }
    
    public function ganti_password()
    {
        $data['title'] = 'Ganti Password';
        
        $this->form_validation->set_rules('nis_nip', 'NIS', 'required|trim');
        $this->form_validation->set_rules('old_pass', 'Password Sekarang', 'required|trim');
        $this->form_validation->set_rules('new_pass', 'Password Baru', 'required|trim');
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header', $data);
            $this->load->view('template/side-menu-pengajar');
            $this->load->view('pengajar/ganti_password', $data);
            $this->load->view('template/footer');
		}
		else 
		{
			if( $this->Pengajar_model->ganti_password() )
			{				
				$this->load->view('template/header', $data);
				$this->load->view('template/side-menu-pengajar');
				$this->load->view('admin/sukses');
				$this->load->view('siswa/ganti_password', $data);
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header', $data);
				$this->load->view('template/side-menu-pengajar');
				$this->load->view('admin/gagal');
				$this->load->view('siswa/ganti_password', $data);
				$this->load->view('template/footer');
			}
			
		}
       
    }
    
}

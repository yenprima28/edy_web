<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	public function __construct()
	{
		parent:: __construct();
		//load model disini
		$this->load->model(
			array
			(
				'admin_model',				
				'siswa_model',				
				'pengajar_model',
				'mapel_model',
				'kelas_model',
				'ekskul_model',
				'semester_model',
				'walikelas_model'
			)
		);
		
		//load helper dan library disini
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('form_validation');	
	}
	
	public function index()
	{
		$this->load->view('template/header-admin');
		$this->load->view('template/side-menu-admin');
		$this->load->view('admin/index');
		$this->load->view('template/footer');
	}
	
	//Siswa
	public function tambah_data_siswa()
	{		
		$data['title'] = 'Tambah Data Siswa';
		
		$this->form_validation->set_rules('nis', 'NIS', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|trim');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('ttl', 'Tempat/Tanggal Lahir', 'required|trim');		
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/tambah_data_siswa');			
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			
			if($this->siswa_model->tambah_siswa())
			{					
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/tambah_data_siswa');
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_data_siswa');
				$this->load->view('template/footer');
			}
			
		}
		
	}
	
	public function daftar_siswa()
	{
		$data['title'] = 'Daftar Seluruh Siswa';		
		$data['siswa'] = $this->admin_model->get_all_siswa();
		// $data['no'] = 1;
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');		
		$this->load->view('admin/daftar_siswa', $data);
		$this->load->view('template/footer');
	}
    
    public function edit_data_siswa()
    {
        $data['title'] = 'Edit Data Siswa';
		$data['siswa'] = $this->admin_model->get_siswa_by_nis();
		$data['no'] = 1;
        
		$this->form_validation->set_rules('nis', 'NIS', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|trim');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('ttl', 'Tempat/Tanggal Lahir', 'required|trim');		
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/edit_siswa');			
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			
			if( $this->admin_model->update_data_siswa() )
			{					
				// $this->load->view('template/header-admin', $data);
				// $this->load->view('template/side-menu-admin');
				// $this->load->view('admin/sukses');
				// $this->load->view('admin/tambah_data_siswa');
				// $this->load->view('template/footer');
                redirect('admin/daftar_siswa');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_data_siswa');
				$this->load->view('template/footer');
			}
			
		}
    }
    
    public function hapus_siswa()
    {        
        if( $this->admin_model->delete_siswa() )
        {
            redirect('admin/daftar_siswa');
        }
    }
	//Siswa	
	
	//Pengajar
	public function tambah_data_pengajar()
	{
		$data['title'] = 'Tambah Data Pengajar';
		
		$this->form_validation->set_rules('nip', 'NIP', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|trim');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('ttl', 'Tempat/Tanggal Lahir', 'required|trim');
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/tambah_data_pengajar');			
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			
			if($this->admin_model->tambah_pengajar())
			{				
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/tambah_data_pengajar');
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_data_pengajar');
				$this->load->view('template/footer');
			}
			
		}
	}
	
	public function daftar_pengajar()
	{
		$data['title'] = 'Daftar Pengajar';
		$data['pengajar'] = $this->admin_model->get_all_pengajar();
		$data['no'] = 1;
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');		
		$this->load->view('admin/daftar_pengajar', $data);
		$this->load->view('template/footer');
	}	
    
    public function hapus_pengajar()
    {
        if( $this->admin_model->delete_pengajar() )
        {
            redirect('admin/daftar_pengajar');
        }
    }
	
	public function kelola_jabatan_pengajar()
	{
		$data['title'] = 'Kelola Jabatan Pengajar';		
		$data['no'] = 1;
		$data['pengajar'] = $this->admin_model->get_all_pengajar();
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');
		$this->load->view('admin/kelola_jabatan_pengajar', $data);
		$this->load->view('template/footer');
	}
	
	public function set_jabatan()
	{		
		$data['title'] = 'Kelola Jabatan Pengajar';		
		$data['no'] = 1;
		$data['pengajar'] = $this->admin_model->get_all_pengajar();
		
		if($this->admin_model->set_jabatan())
		{
			//echo $jabatan,'/',$nip,':sukses ditambahkan jabatan';
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/sukses');
			$this->load->view('admin/kelola_jabatan_pengajar', $data);
			$this->load->view('template/footer');;
		} 
		else 
		{
			//echo 'Jabatan sudah ada, silahkan edit/hapus melalui menu <b>daftar jabatan pengajar</b>';
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/gagal');
			$this->load->view('admin/kelola_jabatan_pengajar', $data);
			$this->load->view('template/footer');
		}
		
	}
	
	public function kelola_kelas_pengajar()
	{
		$data['title'] = 'Kelola Kelas Pengajar';		
		$data['no'] = 1;		
		$data['pengajar'] = $this->admin_model->get_all_pengajar_mapel_kelas();		
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');
		$this->load->view('admin/kelola_kelas_pengajar', $data);
		$this->load->view('template/footer');
	}
	
	public function set_kelas_pengajar()
	{		
		$data['title'] = 'Kelola Kelas Pengajar';		
		$data['no'] = 1;
		$data['pengajar'] = $this->admin_model->get_all_pengajar();		
		
		
		
	}
	
    /**Testing**/
    
    public function daftar_jabatan_pengajar_test()
    {
        //untuk mengambild daftar pengajar yg sudah memiliki jabatan
        $data['pengajar'] = $this->admin_model->get_all_pengajar_with_jabatan_test();
        
        echo json_encode($data);
        
        // var_dump($data);
    }
    
    public function daftar_jabatan_pengajar_test1()
    {
        //untuk mengambild daftar pengajar yg sudah memiliki jabatan
        $data['pengajar'] = $this->admin_model->get_all_jabatan_pengajar_test();
        
        echo json_encode($data);
        
        // var_dump($data);
    }
    
    /**Testing**/
    
	public function daftar_jabatan_pengajar()
	{
		$data['title'] = 'Daftar Pengajar (Sudah mempunyai jabatan)';
		$data['no'] = 1;
		
		$data['pengajar'] = $this->admin_model->get_all_pengajar_with_jabatan();
		
		//var_dump($data['pengajar']);
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');		
		$this->load->view('admin/daftar_jabatan_pengajar', $data);
		$this->load->view('template/footer');
	}
	
	public function cari_pengajar()
	{
		$data['kelas_mapel'] = $this->admin_model->cari_data_pengajar_kelas_mapel();
		
		$this->form_validation->set_rules('nip', 'NIP', 'required|trim');		
		
		//echo json_encode($data);
		//var_dump($data['kelas_mapel']);
		if($this->form_validation->run() === FALSE)
		{	
			$array = array
			(
				'pesan' => 'NIP Belum di inputkan',
				'status' => false
			);
			
			echo json_encode($array);
		}
		else 
		{
			$data = array
			(
				//'pengajar' => $this->admin_model->cari_data_pengajar(),
				'kelas_mapel' => $this->admin_model->cari_data_pengajar_kelas_mapel(),
				'status' => true
			);
			
			if($data['kelas_mapel'] == null)
			{
				$data['pesan'] = 'Coeg';
				$data['status'] = false;
			}
			
			echo json_encode($data);
		}
	}
	
	public function tambah_pengajar_kelas()
	{
		$data['title'] = 'Tambah Data Pengajar Kelas Mapel';
		$data['kelas'] = $this->admin_model->get_all_kelas();
		$data['mapel'] = $this->admin_model->get_all_mapel();
		
		$this->form_validation->set_rules('nip', 'NIP', 'required|trim');
		//$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|trim');
		$this->form_validation->set_rules('kelas', 'Kelas', 'required');
		$this->form_validation->set_rules('mapel', 'Mata Pelajarn', 'required');		
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/tambah_data_pengajar_kelas_mapel');			
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			
			if($this->admin_model->tambah_kelas_mapel_pengajar())
			{				
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/tambah_data_pengajar_kelas_mapel');
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_data_pengajar_kelas_mapel');
				$this->load->view('template/footer');
			}
			
		}
	}
	
    public function edit_data_pengajar()
    {
        $data['title'] = 'Edit Data Pengajar';
		$data['pengajar'] = $this->admin_model->get_pengajar_by_nip();
		$data['no'] = 1;
        
        // var_dump( $data['pengajar'][0]->nip );
        
		$this->form_validation->set_rules('nip', 'NIP', 'required|trim');
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|trim');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');		
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');		
		$this->form_validation->set_rules('ttl', 'TTL', 'required');
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/edit_pengajar');	
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			// $nip = $this->uri->segment(3);
            
			if( $this->admin_model->update_data_pengajar() )
			{				
				// $this->load->view('template/header-admin', $data);
				// $this->load->view('template/side-menu-admin');
				// $this->load->view('admin/sukses');
				// $this->load->view('admin/daftar_pengajar', $data);
				// $this->load->view('template/footer');
                redirect('admin/daftar_pengajar');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/daftar_pengajar');
				$this->load->view('template/footer');
			}
			
		}
    }
    
	//Pengajar
	
	//Wali Kelas
	public function daftar_wali_kelas()
	{
		$data['title'] = 'Daftar Seluruh Wali Kelas';		
		$data['walikelas'] = $this->walikelas_model->get_walikelas();
		$data['no'] = 1;
		
		//var_dump($data['walikelas']);
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');		
		$this->load->view('admin/daftar_wali_kelas', $data);
		$this->load->view('template/footer');
	}
	//Wali Kelas
	
	//Mapel
	public function tambah_data_mapel()
	{
		$data['title'] = 'Tambah Data Mata Pelajaran';
		
		$this->form_validation->set_rules('kd_mapel', 'ID Mata Pelajaran', 'required|trim');
		$this->form_validation->set_rules('mapel', 'Nama Mata Pelajaran', 'required|trim');
		$this->form_validation->set_rules('kkm', 'KKM Mata Pelajaran', 'required|trim');		
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/tambah_data_mapel');	
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			
			if($this->admin_model->tambah_mapel())
			{				
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/tambah_data_mapel');
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_data_mapel');
				$this->load->view('template/footer');
			}
			
		}
	}
	
	public function daftar_mapel()
	{
		$data['title'] = 'Daftar Seluruh Mata Pelajaran';		
		$data['mapel'] = $this->mapel_model->get_all_mapel();
		$data['no'] = 1;
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');		
		$this->load->view('admin/daftar_mapel', $data);
		$this->load->view('template/footer');
	}
    
    public function edit_data_mapel()
    {
        $data['title'] = 'Edit Data Mata Pelajaran';
		$data['mapel'] = $this->admin_model->get_mapel_by_id();
		$data['no'] = 1;
        
		$this->form_validation->set_rules('id_mapel', 'ID Mata Pelajaran', 'required|trim');
		$this->form_validation->set_rules('mapel', 'Nama Mata Pelajaran', 'required|trim');
		$this->form_validation->set_rules('kkm', 'KKM Mata Pelajaran', 'required|trim');		
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/edit_mapel');	
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			
			if($this->admin_model->update_mapel())
			{				
				// $this->load->view('template/header-admin', $data);
				// $this->load->view('template/side-menu-admin');
				// $this->load->view('admin/sukses');
				// $this->load->view('admin/daftar_mapel');
				// $this->load->view('template/footer');
                redirect('admin/daftar_mapel');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_data_mapel');
				$this->load->view('template/footer');
			}
			
		}
    }
	
    public function hapus_mapel()
    {        
        if( $this->admin_model->delete_mapel() )
        {
            redirect('admin/daftar_mapel');
        }
    }
    
	public function daftar_pengajar_mapel()
	{
		$data['title'] = 'Daftar Pengajar Mata Pelajaran';		
		$data['pengajar_mapel'] = $this->mapel_model->get_pengajar_mapel();
		$data['no'] = 1;
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');		
		$this->load->view('admin/daftar_pengajar_mapel', $data);
		$this->load->view('template/footer');
	}
	
	public function kelola_pengajar_mapel()
	{
		$data['title'] = 'Kelola Pengajar Mata Pelajaran';		
		$data['pengajar'] = $this->pengajar_model->get_all_pengajar();
		$data['mapel'] = $this->mapel_model->get_all_mapel();
		
		//$this->form_validation->set_rules('kelas', 'Kelas', 'required');		
		$this->form_validation->set_rules('mapel', 'mapel', 'required');		
		$this->form_validation->set_rules('pengajar[]', 'Pengajar', 'required');		
		
		if($this->form_validation->run() === FALSE)
		{			
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');		
			$this->load->view('admin/kelola_pengajar_mapel', $data);
			$this->load->view('template/footer');
		}
		else 
		{							
			if($this->mapel_model->set_pengajar_mapel())
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');		
				$this->load->view('admin/sukses');		
				$this->load->view('admin/kelola_pengajar_mapel', $data);
				$this->load->view('template/footer');
			} 
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');		
				$this->load->view('admin/gagal');		
				$this->load->view('admin/kelola_pengajar_mapel', $data);
				$this->load->view('template/footer');
			}
		}
		
	}
	//Mapel
	
	//Kelas
	public function tambah_data_kelas()
	{
		$data['title'] = 'Tambah Data Kelas';
		
		$this->form_validation->set_rules('kd_kelas', 'ID Kelas', 'required|trim');
		$this->form_validation->set_rules('kelas', 'Nama Kelas', 'required');		
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/tambah_data_kelas');	
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			
			if($this->kelas_model->tambah_kelas())
			{				
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/tambah_data_kelas');
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_data_kelas');
				$this->load->view('template/footer');
			}
			
		}
	}
	
	public function daftar_kelas()
	{
		$data['title'] = 'Daftar Seluruh Kelas';		
		$data['kelas'] = $this->kelas_model->get_all_kelas();
		$data['no'] = 1;
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');		
		$this->load->view('admin/daftar_kelas', $data);
		$this->load->view('template/footer');
	}
    
    public function edit_data_kelas()
    {
        $data['title'] = 'Edit Data Kelas';
		$data['kelas'] = $this->admin_model->get_kelas_by_id();
		$data['no'] = 1;
        
		$this->form_validation->set_rules('kd_kelas', 'ID Kelas', 'required|trim');
		$this->form_validation->set_rules('kelas', 'Nama Kelas', 'required');		
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/edit_kelas');	
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			
			if($this->admin_model->update_kelas())
			{				
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/tambah_data_kelas');
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_data_kelas');
				$this->load->view('template/footer');
			}
			
		}
    }
	
	public function daftar_siswa_kelas()
	{		
		//$nama_kelas = $this->admin_model->getNamaKelas();
		//$data['title'] = 'Daftar Seluruh Siswa Kelas ' . $nama_kelas['kelas'];
		$data['siswa_kelas'] = $this->admin_model->get_all_siswa_perkelas_coeg();
		$data['no'] = 1;
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');		
		$this->load->view('admin/daftar_siswa_kelas', $data);
		$this->load->view('template/footer');
	}
	
	public function cek_nis_siswa()
	{
		$data['nis_siswa'] = $this->admin_model->cek_nis_siswa();
		
		echo json_encode($data);
	}
	
	public function tambah_siswa_perkelas()
	{
		$data['title'] = 'Tambah Siswa Perkelas';
		$data['kelas'] = $this->kelas_model->get_all_kelas();
		$data['no'] = 1;
										
		$this->form_validation->set_rules('nis', 'NIS', 'required|trim');
		$this->form_validation->set_rules('kelas', 'Kelas', 'required');				
		
		if($this->form_validation->run() === FALSE)
		{				
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');		
			$this->load->view('admin/tambah_siswa_kelas_ini', $data);
			$this->load->view('template/footer');
		}
		else
		{
			
			//var_dump($this->input->post('kelas'));
			if($this->kelas_model->tambah_dalam_kelas())
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');		
				$this->load->view('admin/sukses');
				$this->load->view('admin/tambah_siswa_kelas_ini', $data);
				$this->load->view('template/footer');
			}
			else
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');		
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_siswa_kelas_ini', $data);
				$this->load->view('template/footer');
			}
		}					
	}
	//Kelas
	
	//Ekstrakulikuler
	public function tambah_data_ekskul()
	{
		$data['title'] = 'Tambah Data Ekstrakulikuler';
		
		$this->form_validation->set_rules('id_ekskul', 'ID Ekstrakulikuler', 'required|trim');
		$this->form_validation->set_rules('ekskul', 'Nama Ekstrakulikuler', 'required|trim');		
		$this->form_validation->set_rules('jadwal', 'Jadwal Ekstrakulikuler', 'required|trim');		
		$this->form_validation->set_rules('lokasi', 'Lokasi Ekstrakulikuler', 'required|trim');		
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/tambah_data_ekskul');	
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			
			if($this->admin_model->tambah_ekskul())
			{				
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/tambah_data_ekskul');
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_data_ekskul');
				$this->load->view('template/footer');
			}
			
		}
	}
	
	public function daftar_ekskul()
	{
		$data['title'] = 'Daftar Seluruh Ekstrakulikuler';		
		$data['ekskul'] = $this->ekskul_model->get_all_ekskul();
		$data['no'] = 1;
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');		
		$this->load->view('admin/daftar_ekskul', $data);
		$this->load->view('template/footer');
	}
    
    public function edit_data_ekskul()
    {
        $data['title'] = 'Edit Data Ekstrakulikuler';
        $data['ekskul'] = $this->admin_model->get_ekskul_by_id();
		$data['no'] = 1;
		
		$this->form_validation->set_rules('id_ekskul', 'ID Ekstrakulikuler', 'required|trim');
		$this->form_validation->set_rules('ekskul', 'Nama Ekstrakulikuler', 'required|trim');		
		$this->form_validation->set_rules('jadwal', 'Jadwal Ekstrakulikuler', 'required|trim');		
		$this->form_validation->set_rules('lokasi', 'Lokasi Ekstrakulikuler', 'required|trim');		
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/edit_ekskul');	
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';
			
			if($this->admin_model->update_ekskul())
			{				
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/edit_ekskul');
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/edit_ekskul');
				$this->load->view('template/footer');
			}
			
		}
    }
    
    public function hapus_ekskul()
    {        
        if( $this->admin_model->delete_ekskul() )
        {
            redirect('admin/daftar_ekskul');
        }
    }
    
	//Ekstrakulikuler
	
	//Semester
	public function tambah_data_tahun_pelajaran()
	{
		$data['title'] = 'Tambah Data Tahun Pelajaran';		
		$data['no'] = 1;
		
		$this->form_validation->set_rules('tahun_pelajaran', 'Tahun Pelajaran', 'required|trim');
		$this->form_validation->set_rules('smt', 'Semester', 'required|trim');		
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
			$this->load->view('template/side-menu-admin');
			$this->load->view('admin/tambah_data_tahun_pelajaran');	
			$this->load->view('template/footer');
		}
		else 
		{
			//$data['info_sukses'] = 'Data Mahasiswa berhasil ditambah';			
			if($this->admin_model->tambah_tahun_smt())
			{				
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/tambah_data_tahun_pelajaran');
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/tambah_data_tahun_pelajaran');
				$this->load->view('template/footer');
			}
			
		}		
	}
	
	public function daftar_tahun_pelajaran()
	{
		$data['title'] = 'Daftar Tahun Pelajaran';		
		$data['semester_aktif'] = $this->semester_model->get_all_semester();
		$data['no'] = 1;
		
		$this->load->view('template/header-admin', $data);
		$this->load->view('template/side-menu-admin');		
		$this->load->view('admin/daftar_tahun_pelajaran', $data);
		$this->load->view('template/footer');
	}
	
	public function aktifkan_semester($id)
	{				
		//cek dahulu apakah ada semester yang sedang aktif
		$jumlah_semester_aktif = $this->semester_model->hitung_semester_aktif();		
		
		if($jumlah_semester_aktif >= 1)
		{
			
			echo 'ERROR: Ada semester yang sudah aktif';
			
		} else 
		{
        
			$this->semester_model->aktifkan($id);
			
			redirect('admin/daftar_tahun_pelajaran');
			
		}
		
	}
	
	public function matikan_semester()
	{	
		$this->semester_model->matikan();
		
		redirect('admin/daftar_tahun_pelajaran');
	}
	//Semester
	
    //Kelola Akun Siswa dan Pengajar
    public function reset_password_siswa_pengajar()
    {
        $data['title'] = 'Reset Password Siswa/Pengajar';		
        
        $this->form_validation->set_rules('nis_nip', 'NIS/NIP', 'required|trim');
        $this->form_validation->set_rules('new_pass', 'Password Baru', 'required|trim');
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
            $this->load->view('template/side-menu-admin');
            $this->load->view('admin/reset_password', $data);
            $this->load->view('template/footer');
		}
		else 
		{
			if( $this->admin_model->reset_password() )
			{				
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/reset_password', $data);
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/reset_password', $data);
				$this->load->view('template/footer');
			}
			
		}		
    }
    //Kelola Akun Siswa dan Pengajar    
    
    //Kelola Akun Admin
    public function reset_admin_pass()
    {
        $data['title'] = 'Reset Password Admin';
        
        $this->form_validation->set_rules('nis_nip', 'NIS/NIP', 'required|trim');
        $this->form_validation->set_rules('old_pass', 'Password Sekarang', 'required|trim');
        $this->form_validation->set_rules('new_pass', 'Password Baru', 'required|trim');
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('template/header-admin', $data);
            $this->load->view('template/side-menu-admin');
            $this->load->view('admin/ganti_password', $data);
            $this->load->view('template/footer');
		}
		else 
		{
			if( $this->admin_model->ganti_password() )
			{				
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/sukses');
				$this->load->view('admin/ganti_password', $data);
				$this->load->view('template/footer');
			}
			else 
			{
				$this->load->view('template/header-admin', $data);
				$this->load->view('template/side-menu-admin');
				$this->load->view('admin/gagal');
				$this->load->view('admin/ganti_password', $data);
				$this->load->view('template/footer');
			}
			
		}		
    }
    //Kelola Akun Admin
    
	//pagination
	public function paging()
	{		
		$this->load->library('pagination');

		$config['base_url'] = base_url('admin/daftar_siswa');
		$config['total_rows'] = 8;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);

		echo $this->pagination->create_links();
	}	
}	

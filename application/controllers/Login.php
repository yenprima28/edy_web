<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();     
		$this->load->model('login_model');
		
		$this->load->helper('url');
		$this->load->library('form_validation','session');        
    }   
	
	public function index()
	{		
        $this->cek_variabel_session();
	}	
    
	public function validasi()
	{	
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|md5|trim');
		
		if($this->form_validation->run() === false)
		{		
			$this->load->view('login');	
		} 
		else 
		{
			
			//2017-06-04
			//cho md5($this->input->post('password'));
			//echo $_POST['password'];
			//echo md5('2017-06-04');
			//Login
			if( $this->login_model->login() )
			{
				// $level = $this->login_model->get_level()['level'];
				$level = $this->login_model->get_level();
				
                var_dump( $level );                
                
				if( isset($level) )
				{	
					// var_dump( $this->set_login_session( $level) );
                    $this->set_login_session( $level );                                        
                    $this->redirect_to();
				}
			}
			else
			{
				echo '<script>','alert("Login Gagal")', '</script>';
				$this->load->view('login');
			}
		}				
	}
		
	public function set_login_session( $level )
	{
		$array = array
		(
			'username' => $this->input->post('username'),
			'level' => $level,
			'isLogin' => true
		);
		
		var_dump($array);
		
		$this->session->set_userdata($array);
	}
    
    public function cek_variabel_session()
    {  
        $username = $this->session->has_userdata('username');
        $level = $this->session->has_userdata('level');
        $isLogin = $this->session->has_userdata('isLogin');
        
        if( $username == false || $level == false || $isLogin == false )
        {
            $this->validasi();
        }
        else 
        {
            $this->redirect_to();
        }
    }
	
	public function redirect_to()
	{
		if($this->session->level == null)
		{
			return FALSE;
		} else if($this->session->level == 'administrator')
        {
			redirect('Admin');
		} else if($this->session->level == 'wali kelas')
        {
			redirect('wali_kelas');
		} else if($this->session->level == 'guru')
        {
			redirect('guru');
		} else 
        {
			//Siswa
			redirect('siswa');			
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		
		redirect('login');	
	}
	
	public function isLogin()
	{
		if(isset($this->session->level))
		{						
			$this->redirect_to();
		} 
		else
		{
			return false;
		}
	}
	
    public function login_testing_area()
    {
        
        var_dump( $this->login_model->get_level() );
        
    }
    
}

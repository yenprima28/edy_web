<?php

class Walikelas_model extends CI_Model {
	
	function __construct(){
		$this->load->database();
	}
	
	public function get_walikelas()
	{
		//SELECT pengajar.nip, pengajar.nama, kelas.kelas
		//FROM wali_kelas
		//INNER JOIN pengajar
		//	ON wali_kelas.nip = pengajar.nip
		//INNER JOIN kelas
		//	ON wali_kelas.kd_kelas	= kelas.kd_kelas
			
		$this->db->select('pengajar.nip, pengajar.nama, kelas.kelas, ');
		$this->db->from('wali_kelas');
		$this->db->join('pengajar', 'wali_kelas.nip = pengajar.nip');
		$this->db->join('kelas', 'wali_kelas.kd_kelas = kelas.kd_kelas');
		return $this->db->get()->result();
	}
	
	public function get_kelas_ampu_by_nip_walikelas()
	{
		$nip = $this->session->username;
		
		$this->db->select('wali_kelas.kd_kelas');
		$this->db->from('wali_kelas');		
		$this->db->where('nip', $nip);
		return $this->db->get()->row_array();
	}
	
	public function get_data_siswa_diampu($smt_aktif, $tahun_smt)
	{
		$kelas_ampu = $this->get_kelas_ampu_by_nip_walikelas();
		
		$this->db->select('siswa.nis, siswa.nama, kelas.kelas, kelas.kd_kelas');
		$this->db->from('kelas_siswa');
		$this->db->join('siswa','kelas_siswa.nis = siswa.nis');
		$this->db->join('kelas','kelas_siswa.kd_kelas = kelas.kd_kelas');
		$this->db->where('kelas_siswa.kd_kelas', $kelas_ampu['kd_kelas']);
		$this->db->where('kelas_siswa.smt', $smt_aktif);
		$this->db->where('kelas_siswa.tahun', $tahun_smt);
		$query = $this->db->get()->result();
        
        if( $query != null )
        {
            return $query;
        }
        
        return false;
		//return $this->db->get()->result();
	}
	
    public function get_data_kelas()
    {
        return $this->db->get('kelas')->result();
    }
    
    public function naik_kelas($smt_aktif, $tahun_smt)
    {        
        $nis = $this->input->post('nis');
        $kelas_baru = $this->input->post('kelas_baru');
        
        $data = array
        (
            'nis' => $nis,
            'kd_kelas' => $kelas_baru,
            'smt' => $smt_aktif,
            'tahun' => $tahun_smt
        );
        
        $cari = false;
        
        if( $this->db->get_where('kelas_siswa', $data)->num_rows() == 0 )
        {
            if( $this->db->insert('kelas_siswa', $data) )
            {
                return TRUE;
            }
        }                
        
        return FALSE;
    }
    
    public function get_nilai_siswa_perkelas_by_mapel($smt_aktif, $tahun_smt, $mapel)
    {
        $kelas_ampu = $this->get_kelas_ampu_by_nip_walikelas();

        $str_query = 'nilai_siswa.pengetahuan_grade, nilai_siswa.pengetahuan_angka, nilai_siswa.keterampilan_grade, nilai_siswa.keterampilan_angka, ';
        $str_query .= 'siswa.nis, siswa.nama, kelas.kelas, ';
        $str_query .= 'mapel.mapel, ';
        $str_query .= 'kelas.kelas';
        
        $this->db->select($str_query);        
		$this->db->from('nilai_siswa');
		$this->db->join('kelas_siswa','nilai_siswa.nis = kelas_siswa.nis');
		$this->db->join('kelas','kelas_siswa.kd_kelas = kelas.kd_kelas');
		$this->db->join('mapel','nilai_siswa.id_mapel = mapel.kd_mapel');
		$this->db->join('siswa','nilai_siswa.nis = siswa.nis');
		$this->db->where('nilai_siswa.id_mapel', $mapel);
		$this->db->where('kelas_siswa.kd_kelas', $kelas_ampu['kd_kelas']);
		$this->db->where('nilai_siswa.semester', $smt_aktif);
		$this->db->where('nilai_siswa.tahun', $tahun_smt);        
		$query = $this->db->get('')->result();
        
        if( $query != null )
        {
            return $query;
        }
        
        return false;
    }
    
    public function get_all_mapel()
    {
        $this->db->distinct();
        return $this->db->get('mapel')->result();
    }
    
    public function get_all_nilai()
    {
        $kd_mapel = $this->uri->segment(3);
        $smt = $this->uri->segment(4);
        $tahun = $this->uri->segment(5);
        $kelas = $this->uri->segment(6);
        
        $str_query = '';
        
        $str_query = 'nilai_siswa.pengetahuan_grade, nilai_siswa.pengetahuan_angka, nilai_siswa.keterampilan_grade, nilai_siswa.keterampilan_angka, ';
        $str_query .= 'siswa.nis, siswa.nama, kelas.kelas, ';
        $str_query .= 'mapel.mapel, ';
        $str_query .= 'kelas.kelas';
        
        $this->db->select($str_query);        
		$this->db->from('nilai_siswa');
		$this->db->join('kelas_siswa','nilai_siswa.nis = kelas_siswa.nis');
		$this->db->join('kelas','kelas_siswa.kd_kelas = kelas.kd_kelas');
		$this->db->join('mapel','nilai_siswa.id_mapel = mapel.kd_mapel');
		$this->db->join('siswa','nilai_siswa.nis = siswa.nis');
		$this->db->where('nilai_siswa.id_mapel', $kd_mapel);
		$this->db->where('kelas_siswa.kd_kelas', $kelas);
		$this->db->where('nilai_siswa.semester', $smt);
		$this->db->where('nilai_siswa.tahun', $tahun);        
		$query = $this->db->get('')->result();
        
        if( $query != null )
        {
            return $query;
        }
        
        return false;
    }
    
    public function get_absensi($smt_aktif, $tahun_smt)
    {
        $nis = $this->input->post('nis');        
        
        $this->db->where('nis', $nis);
        $this->db->where('smt', $smt_aktif);
        $this->db->where('tahun', $tahun_smt);
        $query = $this->db->get('absensi')->result();
        
        if( $query != null )
        {
            return $query;
        }
        
        return false;
    }    
    
    public function insert_absen($smt_aktif, $tahun_smt)
    {
        $data = array
        (
            'nis' => $this->input->post('nis'),
            'ijin' => $this->input->post('ijin'),
            'sakit' => $this->input->post('sakit'),
            'alpa' => $this->input->post('alpa'),
            'smt' => $smt_aktif,
            'tahun' => $tahun_smt
        );
        
        $data_update = array
        (            
            'ijin' => $this->input->post('ijin'),
            'sakit' => $this->input->post('sakit'),
            'alpa' => $this->input->post('alpa'),            
        );
        
        $cari_data = array
        (
            'nis' => $this->input->post('nis'),            
            'smt' => $smt_aktif,
            'tahun' => $tahun_smt
        );
        
        $cari = $this->db->get_where('absensi', $cari_data)->num_rows();
        
        if($cari >= 1)
        {
            //update data
            $this->db->set($data_update);
            $this->db->where('nis', $data['nis']);
            if( $this->db->update('absensi', $data_update) )
            {
                return true;
            }
        }
        else
        {
            //entry data
            if( $this->db->insert('absensi', $data) )
            {
                return true;
            }
        }        
        
        return false;
    }
    
    public function get_siswa()
    {
        $nis = $this->input->post('nis');
        
        $this->db->where('nis', $nis);
        $query = $this->db->get('siswa')->result();
        
        if( $query )
        {
            return $query;
        }
        
        return false;
    }
    
    public function get_ekskul_list()
    {
        return $this->db->get('ekskul')->result();
    }
    
    public function get_siswa_ekskul()
    {
        $nis = $this->input->post('nis');
        
        $this->db->where('nis', $nis);
        $query = $this->db->get('ekskul_siswa')->result();
        
        if( $query )
        {
            return $query;
        }
        
        return false;
    }
    
    public function get_prestasi()
    {        
        $nis = $this->input->post('nis');
        
        $this->db->where('nis', $nis);
        $query = $this->db->get('prestasi')->result();
        
        if( $query )
        {
            return $query;
        }
        
        return false;
    }
    
    public function insert_prestasi()
    {
        $data = array
        (
            'nis' => $this->input->post('nis'),
            'prestasi' => $this->input->post('prestasi'),
            'detail_prestasi' => $this->input->post('detail_prestasi')
        );
        
        if( $this->db->insert('prestasi', $data) )
        {
            return true;
        }
        
        return false;
    }
    
    public function insert_siswa_ekskul()
    {        
        
        $id_ekskul = $this->input->post('id_ekskul');
        
        if( strpos($id_ekskul, ';')  !== false )
        {
            $explode = explode(';', $id_ekskul);
            
            foreach( $explode as $list )
            {
                
                $data = array
                (
                    'nis' => $this->input->post('nis'),
                    'id_ekskul' => $list
                );
                
                $this->db->insert('ekskul_siswa', $data);
            }
            
            return true;                                
        } 
        else 
        {
            $data = array
            (
                'nis' => $this->input->post('nis'),
                'id_ekskul' => $id_ekskul
            );
            
            if( $this->db->insert('ekskul_siswa', $data) )
            {
                return true;
            }
        }
        
        return false;
    }
    
}

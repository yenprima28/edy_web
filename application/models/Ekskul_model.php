<?php

class Ekskul_model extends CI_Model {
	
	function __construct(){
		$this->load->database();
	}
	
	public function tambah_ekskul()
	{
		$data = array(
			'id_ekskul' => $this->input->post('id_ekskul'),
			'ekskul'	=> $this->input->post('ekskul')			
		);
				
		if($this->db->get_where('ekskul', array('id_ekskul'=>$data['id_ekskul']))->num_rows() == 0)
		{
			$this->db->insert('ekskul', $data);			
			return TRUE;
		}
		
		return FALSE;
	}
	
	function get_all_ekskul(){
		return $this->db->get('ekskul')->result();
	}
	
	function get_ekskul_by_id($id){
		$this->db->where('id_ekskul', $id);
		$query = $this->db->get('ekskul');
		
		return $query->result();
	}
	
	function update_data_ekskul($id, $data){
		
		$this->db->set($data);
		$this->db->where('id_ekskul', $id);		
		$query = $this->db->update('ekskul', $data);
		
		if($query)
			return TRUE;
		
		return FALSE;
	}
	
	function delete_ekskul($id){
		$this->db->where('id_ekskul', $id);		
		$query = $this->db->delete('ekskul');
		
		if($query)
			return TRUE;		
		
		return FALSE;
	}
	
}
<?php

class Pagination_model extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();		
	}
	
	public function get_siswa($limit, $start)
	{
		$this->db->limit($limit, $start);
		return $this->db->get('siswa')->result();
	}
	
	public function count_siswa()
	{
		return $this->db->get('siswa')->num_rows();
	}
	
}
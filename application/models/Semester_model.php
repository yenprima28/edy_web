<?php

class Semester_model extends CI_Model {
	
	function __construct(){
		$this->load->database();
	}
	
	function get_all_semester()    
    {		
		//enam semester
		$this->db->limit(6);
		$this->db->order_by('id', 'DESC');
		return $this->db->get('smt_aktif')->result();
	}	
	
	function get_semester_aktif()
    {		
		//enam semester
		$this->db->where('status', 'Aktif');
		return $this->db->get('smt_aktif')->result();
	}	
	
	function hitung_semester_aktif()
    {
		$this->db->where('status', 'Aktif');		
		$query = $this->db->count_all_results('smt_aktif');
		
		return $query;	
	}
	
	function aktifkan($id)
    {
		$data = array(
				 'status' => 'Aktif'
		);
				
		$this->db->where('id', $id);
		$this->db->update('smt_aktif', $data);
	}
	
	function matikan($id)
    {
		$data = array(
				 'status' => 'Tidak Aktif'
		);
				
		$this->db->update('smt_aktif', $data);
	}
	
}

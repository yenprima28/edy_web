<?php

class Mapel_model extends CI_Model {
	
	public function __construct()
	{		
		$this->load->database();
	}
	
	public function tambah_mapel()
	{
		$data = array(
			'id_mapel' 	 => $this->input->post('id_mapel'),
			'nama_mapel' => $this->input->post('mapel'),
			'kkm' 		 => $this->input->post('kkm')
		);
				
		if($this->db->get_where('mapel', array('id_mapel'=>$data['id_mapel']))->num_rows() == 0)
		{
			$this->db->insert('mapel', $data);			
			return TRUE;
		}
				
		return FALSE;				
	}
	
	public function set_pengajar_mapel()
	{
		foreach($this->input->post('pengajar') as $ls)
		{
			$nip = $ls;		
		}
		
		$data = array(
			'id_mapel' 	  => $this->input->post('mapel'),
			'id_pengajar' => $nip
		);		
		
		if($this->db->get_where('pengajar_mapel', $data)->num_rows() == 0)
		{
			$this->db->insert('pengajar_mapel', $data);			
			return TRUE;
		}
				
		return FALSE;				
	}
	
	function get_pengajar_mapel()
	{
		return $this->db->get('pengajar_mapel')->result();
	}
	
	function get_all_mapel()
	{
		return $this->db->get('mapel')->result();
	}
	
	function get_mapel_by_id($id){
		
		$this->db->where('id_mapel', $id);
		$query = $this->db->get('mapel');
		
		return $query->result();
		
	}	
	
	function update_data_mapel($id, $data){
		
		$this->db->set($data);
		$this->db->where('id_mapel', $id);		
		$query = $this->db->update('mapel', $data);
		
		if($query)
			return TRUE;
		
		return FALSE;
	}
}

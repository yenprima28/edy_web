<?php

class Kelas_model extends CI_Model {
	
	function __construct(){
		$this->load->database();
	}
	
	function tambah_kelas()
	{
		$data = array(
			'kd_kelas' 	=> $this->input->post('kd_kelas'),
			'kelas' 	=> $this->input->post('kelas')			
		);
				
		if($this->db->get_where('kelas', array('kd_kelas'=>$data['kd_kelas']))->num_rows() == 0)
		{
			$this->db->insert('kelas', $data);			
			return TRUE;
		}
				
		return FALSE;				
	}
	
	function getNamaKelas()
	{
		$id_kelas = $this->uri->segment(3);
		
		$data = $this->db->get_where('kelas', array('id_kelas' => $id_kelas));
		
		return $data->row_array();
	}
	
	function get_all_kelas(){
		return $this->db->get('kelas')->result();
	}
	
	function get_kelas_by_id($id){
		$this->db->where('id_kelas', $id);
		$query = $this->db->get('kelas');
		
		return $query->result();
	}
	
	function insert_kelas($data){
		
		$this->db->insert('kelas', $data);
		
	}
	
	function tambah_dalam_kelas(){
		
		$data = array
		(
			'kd_kelas' => $this->input->post('kelas'),
			'nis' => $this->input->post('nis'),
			'tahun' => '2017'
		);
		
		$cari = $this->db->get_where('kelas_siswa', $data)->num_rows();
		
		if($cari == 0)
		{
			$this->db->insert('kelas_siswa', $data);
			return TRUE;
		}
		
		return FALSE;
	}
	
	function insert_mapel_kelas($data){
		
		$this->db->insert('mapel_kelas', $data);		
		
	}
	
	function update_data_kelas($id, $data){
		
		$this->db->set($data);
		$this->db->where('id_kelas', $id);		
		$query = $this->db->update('kelas', $data);
		
		if($query)
			return TRUE;
		
		return FALSE;
	}
	
	function get_all_siswa_kelas()
	{				
		$kelas    = $this->input->post('kelas');
		$mapel    = $this->input->post('mapel');		
		
		$this->db->select('siswa.nis');
		$this->db->select('siswa.nama');
		$this->db->select('nilai_siswa.pengetahuan_angka');
		$this->db->select('nilai_siswa.pengetahuan_grade');
		$this->db->select('nilai_siswa.keterampilan_angka');
		$this->db->select('nilai_siswa.keterampilan_grade');
		$this->db->from('siswa');
		$this->db->join('kelas_siswa', 'siswa.nis = kelas_siswa.nis');
		$this->db->join('nilai_siswa', 'siswa.nis = nilai_siswa.nis');
		$this->db->where('kelas_siswa.kd_kelas', $kelas);
		$query = $this->db->get();

		return $query->result();
	}
	
}
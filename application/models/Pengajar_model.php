<?php

class Pengajar_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
	}	
		
	function get_all_mapel_pengajar()
	{
		$nip = $this->session->username;
		
		$this->db->select('mapel.mapel, mapel.kd_mapel');
		$this->db->distinct('mapel.mapel, mapel.kd_mapel');		
		$this->db->from('pengajar_mapel');
		$this->db->join('mapel', 'mapel.kd_mapel = pengajar_mapel.kd_mapel');
		$this->db->where('pengajar_mapel.nip', $nip);
		return $this->db->get()->result();
		//$this->db->join('pengajar', 'mapel.kd_mapel');
	}
	
	function get_all_kelas_mapel_pengajar()
	{
		$nip = $this->session->username;
		$kd_mapel = $this->input->post('mapel');
		
		$this->db->select('kelas.kd_kelas, kelas.kelas');
		$this->db->from('pengajar_mapel');
		$this->db->join('mapel', 'mapel.kd_mapel = pengajar_mapel.kd_mapel');
		$this->db->join('kelas', 'kelas.kd_kelas = pengajar_mapel.kelas');
		$this->db->where('pengajar_mapel.nip', $nip);
		$this->db->where('pengajar_mapel.kd_mapel', $kd_mapel);		
		return $this->db->get()->result();
		//$this->db->join('pengajar', 'mapel.kd_mapel');
	}
	
	function get_siswa_mapel_kelas()
	{	
		$kd_mapel = $this->input->post('mapel');		
		$kd_kelas = $this->input->post('kelas');
		//$kd_mapel = 'M001';
		//$kd_kelas = 'K001';
		
		$this->db->select('siswa.nis, siswa.nama, mapel.mapel, kelas.kelas, nilai_siswa.pengetahuan_angka, 
							nilai_siswa.pengetahuan_grade, nilai_siswa.id, nilai_siswa.keterampilan_angka, 
							nilai_siswa.keterampilan_grade');		
		$this->db->from('nilai_siswa');
		$this->db->join('siswa', 'nilai_siswa.nis = siswa.nis');
		$this->db->join('mapel', 'nilai_siswa.id_mapel = mapel.kd_mapel');
		$this->db->join('kelas_siswa', 'siswa.nis = kelas_siswa.nis');
		$this->db->join('kelas', 'kelas_siswa.kd_kelas = kelas.kd_kelas');		
		$this->db->where('nilai_siswa.id_mapel', $kd_mapel);
		$this->db->where('kelas.kd_kelas', $kd_kelas);
		return $this->db->get()->result();		
	}  
	
	//get seluruh siswa berdasarkan kelas
	function get_siswa_mapel_kelas_null()
	{			
		$kd_kelas = $this->input->post('kelas');		
		//$kd_kelas = 'K001';
		
		//$this->db->select('siswa.nis, siswa.nama, mapel.mapel, kelas.kelas, nilai_siswa.pengetahuan_angka, 
		//					nilai_siswa.pengetahuan_grade, nilai_siswa.id, nilai_siswa.keterampilan_angka, 
		//					nilai_siswa.keterampilan_grade');		
		//$this->db->from('nilai_siswa');
		//$this->db->join('siswa', 'nilai_siswa.nis = siswa.nis');
		//$this->db->join('mapel', 'nilai_siswa.id_mapel = mapel.kd_mapel');
		//$this->db->join('kelas_siswa', 'siswa.nis = kelas_siswa.nis');
		//$this->db->join('kelas', 'kelas_siswa.kd_kelas = kelas.kd_kelas');		
		//$this->db->where('kelas.kd_kelas', $kd_kelas);
		//return $this->db->get()->result();		
		
		$this->db->select("siswa.nis, siswa.nama, kelas.kelas");
		$this->db->from("siswa");
		$this->db->join("kelas_siswa", "siswa.nis = kelas_siswa.nis");
		$this->db->join("kelas", "kelas_siswa.kd_kelas = kelas.kd_kelas");
		$this->db->where('kelas.kd_kelas', $kd_kelas);
		return $this->db->get()->result();		
	}
	
	public function get_all_kelas_pengajar_by_mapel()
	{
		$kd_mapel = $this->input->post('mapel');
		//$kd_mapel = 'M001';
		$nip = $this->session->username;
		
		$this->db->select('kelas.kd_kelas, kelas.kelas');		
		$this->db->from('pengajar_mapel');		
		$this->db->join('mapel', 'pengajar_mapel.kd_mapel = mapel.kd_mapel');		
		$this->db->join('kelas', 'pengajar_mapel.kelas = kelas.kd_kelas');
		$this->db->where('pengajar_mapel.kd_mapel', $kd_mapel);
		$this->db->where('pengajar_mapel.nip', $nip);
		return $this->db->get()->result();
	}
	
	//fungsi untuk memeriksa apakah nilai sudah terisi atau belum
	function cek_nilai_terisi($nis, $data){
		
		//$nilai_siswa = $this->get_nilai_siswa_mapel($nis);
		
		$this->db->where('nis', $nis);
		$nilai_siswa = $this->db->get('nilai_siswa_mapel')->result();		
		
		if($data['pengetahuan_angka'] == null AND 
		   $data['keterampilan_angka'] == null){
			   
			   $this->insert_nilai_siswa($data);
			   
		} else {
			
			$this->update_nilai_siswa($data);
			
		}
		
		//var_dump($nilai_siswa);
		
	}
	
	function input_nilai_siswa()
	{
		
		$data = array(
			'id_mapel' => $this->input->post('id_mapel'),
			'nis' => $this->input->post('nis'),
			'pengetahuan_angka' => $this->input->post('pengetahuan_angka'),
			'keterampilan_angka' => $this->input->post('keterampilan_angka'),
			'pengetahuan_grade' => $this->input->post('pengetahuan_grade'),
			'keterampilan_grade' => $this->input->post('keterampilan_grade'),
			'tahun' => $this->input->post('tahun'),
			'semester' => $this->input->post('semester')
				
		);
		
		$cari_data = $this->db->get_where('nilai_siswa', 
				array(
					'id_mapel' => $data['id_mapel'],
					'nis' => $data['nis'],
					'tahun' => $data['tahun'],
					'semester' => $data['semester']
				)
			)->num_rows();
		
		if( $cari_data >= 1 )
		{
			$this->update_nilai_siswa();			
			
			return TRUE;			
		}
		else 
		{
			$this->db->insert('nilai_siswa', $data);
			
			return TRUE;
		}
		
		return FALSE;
	}
	
	function update_nilai_siswa(){		
		
		$nis = $this->input->post('nis');
		$id_mapel = $this->input->post('id_mapel');
		$tahun = $this->input->post('tahun');
		$semester = $this->input->post('semester');
		
		$array_data = array
		(		
			'pengetahuan_angka' => $this->input->post('pengetahuan_angka'),
			'keterampilan_angka' => $this->input->post('keterampilan_angka'),
			'pengetahuan_grade' => $this->input->post('pengetahuan_grade'),
			'keterampilan_grade' => $this->input->post('keterampilan_grade')
		);
		
		//var_dump($array_data);
		
		$this->db->set($array_data);
		$this->db->where('nis', $nis);		
		$this->db->where('id_mapel', $id_mapel);		
		$this->db->where('tahun', $tahun);		
		$this->db->where('semester', $semester);		
		$query = $this->db->update('nilai_siswa', $array_data);
		
		if( $query )
		{
			return TRUE;
		}
		
		return FALSE;
		
	}
	
	function get_nama_siswa()
	{
		$nis = $this->input->post('nis');
		
		$this->db->select('nama');
		$this->db->from('siswa');
		$this->db->where('nis', $nis);
		return $this->db->get()->row_array();
	}
	
	function get_nilai_siswa_by_mapel()
	{
		$mapel = $this->input->post('mapel');
		$nis = $this->input->post('nis');
		
		$fields = 		
			'pengetahuan_grade, ' . 'pengetahuan_angka, ' .
			'keterampilan_grade, ' . 'keterampilan_angka, '
		;
		
		$this->db->select($fields);
		$this->db->from('nilai_siswa');
		$this->db->where('id_mapel', $mapel);
		$this->db->where('nis', $nis);
		return $this->db->get()->row_array();
	}
	
	function get_nilai_siswa_by_nis()
	{
		$nis = $this->input->post('nis');
		$id_mapel = $this->input->post('id_mapel');
		$tahun = 2017;//$this->input->post('tahun');
		$semester = 1;//$this->input->post('semester');
		
		$fields = 		
			'pengetahuan_grade, ' . 'pengetahuan_angka, ' .
			'keterampilan_grade, ' . 'keterampilan_angka, '
		;
		
		$this->db->select($fields);
		$this->db->from('nilai_siswa');
		$this->db->where('id_mapel', $id_mapel);
		$this->db->where('nis', $nis);
		return $this->db->get()->row_array();
	}
	
	function get_level()
	{
		$nip = $this->session->username;
		
		$this->db->where('nip', $nip);
		return $this->db->get('jabatan_pengajar')->result_array();
	}
    
    public function get_gambar()
    {
        $nip = $this->session->username;
        
        $this->db->select('gambar');
        $this->db->where('nip', $nip);
        return $this->db->get('pengajar')->result();
    }
	
	public function count_siswa_by_mapel_kelas_smt_tahun()
	{
		$kelas = $this->input->post('kelas');
		$id_mapel = $this->input->post('id_mapel');
		$tahun = $this->input->post('tahun');
		$semester = $this->input->post('semester');
		
		// $kelas = 'K001';
		// $id_mapel = 'M001';
		// $tahun = '2017';
		// $semester = '1';
		
		$this->db->select('*');
		$this->db->from('nilai_siswa');
		$this->db->join('kelas_siswa', 'nilai_siswa.nis = kelas_siswa.nis');
		$this->db->where('nilai_siswa.id_mapel', $id_mapel);
		$this->db->where('kelas_siswa.kd_kelas', $kelas);
		$this->db->where('nilai_siswa.tahun', $tahun);
		$this->db->where('nilai_siswa.semester', $semester);
		return $this->db->get()->num_rows();
	}
	
	public function count_siswa_by_kelas()
	{
		$kelas = $this->input->post('kelas');
		
		$this->db->where('kd_kelas', $kelas);
		return $this->db->get('kelas_siswa')->num_rows();
	}
    
    public function ganti_password()
    {
        $nis_nip = $this->input->post('nis_nip');
        $old_pass = md5( $this->input->post('old_pass') );        
        $new_pass = md5( $this->input->post('new_pass') );        
        
        $data = array
        (
            'password' => $new_pass
        );
        
        $this->db->set($data);
        $this->db->where('username', $nis_nip);
        $this->db->where('password', $old_pass);
        $query = $this->db->update('akun', $data);
        
        if( $query )
        {
            return true;
        }
        
        return false;
    }
}

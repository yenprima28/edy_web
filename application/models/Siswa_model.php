<?php

class Siswa_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function get_nilai()
	{
		$nis = $this->session->username;
		
		//$this->db->select('siswa.nama_lengkap, mapel.nama_mapel');
		$this->db->select('*, siswa.nama, mapel.kkm');
		$this->db->from('nilai_siswa');
		$this->db->join('siswa', 'nilai_siswa.nis = siswa.nis');
		$this->db->join('mapel', ' nilai_siswa.id_mapel = mapel.kd_mapel');
		$this->db->where('nilai_siswa.nis', $nis);
		$query = $this->db->get();
		
		return $query->result_array();		
	}
	
	function get_profil()
	{
		$nis = $this->session->username;
		
		return $this->db->get_where('siswa', array('nis'=>$nis));
	}
	
	function tambah_siswa()
	{
		$data = array
		(
			'nis' 		=> $this->input->post('nis'),
			'nama' 		=> $this->input->post('nama'),
			'jk' 		=> $this->input->post('jk'),
			'alamat' 	=> $this->input->post('alamat'),
			'ttl' 		=> $this->input->post('ttl')				
		);
		
		//upload_gambar			
		$config['upload_path'] 		= './assets/foto/siswa/';
		$config['allowed_types']	= 'jpg|png|jpeg';
		$config['max_size']			= 1024;
		
		$this->load->library('upload', $config);			
		$this->upload->do_upload('gambar');
		
		//ambil nama gambar
		$nama_gbr = $this->upload->data('file_name');
		$data['gambar'] = $nama_gbr;
		
		
		//register akun siswa
		//username = nis
		//password = tanggal lahir siswa			
		$data_reg = array
		(
			'username' => $this->input->post('nis'),				
			'password' => md5($this->input->post('ttl')),
			'level' => 'siswa'
		);
		
		if($this->db->get_where('siswa', array('nis'=>$data['nis']))->num_rows() == 0)
		{
			$this->db->insert('siswa', $data);
			//data akun untuk login siswa
			$this->db->insert('akun', $data_reg);
			return TRUE;
		}
				
		return FALSE;				
	}	
	
	public function cek_siswa($nis)
	{
		return $this->db->get_where('siswa', array('nis' => $nis));
	}
	
	function get_all_siswa()
	{ 	
		return $this->db->get('siswa')->result();
	}
	
	function get_siswa_by_nis($nis)
	{	
		$this->db->where('nis', $nis);
		return $this->db->get('siswa')->result();	
	}
	
	function get_all_siswa_by_mapel($id_mapel)
	{
		$this->db->where('id_mapel', $id_mapel);
		return $this->db->get('nilai_siswa_mapel')->result();	
	}
	
	public function get_gambar()
    {
        $nis = $this->session->username;
        
        $this->db->select('gambar');
        $this->db->where('nis', $nis);
        return $this->db->get('siswa')->result();
    }
    
    public function ganti_password()
    {
        $nis_nip = $this->input->post('nis_nip');
        $old_pass = md5( $this->input->post('old_pass') );        
        $new_pass = md5( $this->input->post('new_pass') );        
        
        $data = array
        (
            'password' => $new_pass
        );
        
        $this->db->set($data);
        $this->db->where('username', $nis_nip);
        $this->db->where('password', $old_pass);
        $query = $this->db->update('akun', $data);
        
        if( $query )
        {
            return true;
        }
        
        return false;
    }
    
    /**Prestasi**/
    
    public function prestasiku()
    {
        $nis = $this->session->username;
        
        $this->db->where('nis', $nis);
        $query = $this->db->get('prestasi')->result();
        
        if( $query )
        {
            return $query;
        }
        
        return false;
    }
    
    /**Prestasi**/
    
    /**Ekskul**/
    
    public function ekskulku()
    {
        $nis = $this->session->username;
        
        $this->db->select('ekskul.ekskul, ekskul.jadwal, ekskul.lokasi');
        $this->db->from('ekskul_siswa');
        $this->db->join('ekskul', 'ekskul_siswa.id_ekskul = ekskul.id_ekskul');
        $this->db->where('ekskul_siswa.nis', $nis);
        $query = $this->db->get()->result();
        
        if( $query )
        {
            return $query;
        }
        
        return false;
    }
    
    /**Ekskul**/
}

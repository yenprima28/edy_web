<?php

class Admin_model extends CI_Model {
	
	function __construct()
	{
		parent::__construct();
	}

	public function tambah_pengajar()
	{
		$data = array(
			'nip' 		=> $this->input->post('nip'),
			'nama' 		=> $this->input->post('nama'),
			'jk' 		=> $this->input->post('jk'),
			'alamat'	=> $this->input->post('alamat'),
			'ttl' 		=> $this->input->post('ttl')				
		);
		
		//upload_gambar			
		$config['upload_path'] 		= './assets/foto/pengajar/';
		$config['allowed_types']	= 'jpg|png|jpeg';
		$config['max_size']			= 1024;
		
		$this->load->library('upload', $config);			
		$this->upload->do_upload('gambar');
		
		//ambil nama gambar
		$nama_gbr = $this->upload->data('file_name');
		$data['gambar'] = $nama_gbr;
		
		
		//register akun pengajar
		//username = nip
		//password = tanggal lahir pengajar			
		$data_reg = array(
			'username' => $this->input->post('nip'),				
			'password' => md5($this->input->post('ttl')),
			'level' => 'pengajar'			
		);
		
		if($this->db->get_where('pengajar', array('nip'=>$data['nip']))->num_rows() == 0)
		{
			$this->db->insert('pengajar', $data);
			//data akun untuk login pengajar
			$this->db->insert('akun', $data_reg);
			return TRUE;
		}
				
		return FALSE;				
	}
	
	public function set_jabatan()
	{
		$data = array
		(
			'nip'	 	 => $this->uri->segment(4),
			'kd_jabatan' => $this->uri->segment(3)
		);
				
		$cari = $this->db->get_where('jabatan_pengajar', $data)->num_rows();
		
		if($cari === 0){
			$this->db->insert('jabatan_pengajar', $data);
			return true;
		}
		
		return false;
	}
	
	public function get_all_pengajar_with_jabatan()
	{
		//SQL 
		/**SELECT
				pengajar.nip, pengajar.nama, jabatan.jabatan
			FROM jabatan_pengajar
			JOIN pengajar
				ON pengajar.nip = jabatan_pengajar.nip
			JOIN jabatan
				ON jabatan.kd_jabatan = jabatan_pengajar.kd_jabatan
			WHERE
				jabatan_pengajar.nip = pengajar.nip;
		*/
		
		//$this->db->select('pengajar.nip', 'pengajar.nama', 'jabatan.jabatan');
		//$this->db->from('jabatan_pengajar');
		//$this->db->join('pengajar', 'pengajar.nip = jabatan_pengajar.nip');
		//$this->db->join('jabatan', 'jabatan.kd_jabatan = jabatan_pengajar.kd_jabatan');
		//$this->db->where('jabatan_pengajar.nip', 'pengajar.nip');
		//return $this->db->get()->result();
				// DISTINCT (pengajar.nip), pengajar.nama, jabatan.jabatan
		$sql = "SELECT
				pengajar.nip, pengajar.nama, jabatan.jabatan
			FROM jabatan_pengajar
			JOIN pengajar
				ON pengajar.nip = jabatan_pengajar.nip
			JOIN jabatan
				ON jabatan.kd_jabatan = jabatan_pengajar.kd_jabatan
			WHERE
				jabatan_pengajar.nip = pengajar.nip";
				
		return $this->db->query($sql)->result();
	}
    
    /**Testing**/
    public function get_all_pengajar_with_jabatan_test()
	{			
        $this->db->select('pengajar.nip, pengajar.nama');
        $this->db->distinct();
        $this->db->from('jabatan_pengajar');
        $this->db->join('pengajar', 'pengajar.nip = jabatan_pengajar.nip ');
        $this->db->join('jabatan', 'jabatan.kd_jabatan = jabatan_pengajar.kd_jabatan ');        
        $query = $this->db->get()->result();
        
        if( $query )
        {
            return $query;
        }
        
        return false;
	}
    
    public function get_all_jabatan_pengajar_test()
    {
        $nip = $this->input->post('nip');
        
        // return $nip;
        $this->db->select('jabatan.jabatan');
        // $this->db->distinct('');
        $this->db->from('jabatan_pengajar');
        $this->db->join('pengajar', 'pengajar.nip = jabatan_pengajar.nip ');
        $this->db->join('jabatan', 'jabatan.kd_jabatan = jabatan_pengajar.kd_jabatan ');        
        $this->db->where('jabatan_pengajar.nip', $nip);
        $query = $this->db->get()->result();
        
        if( $query )
        {
            return $query;
        }
        
        return false;
    }
    /**Testing**/
	
	function get_all_pengajar()
	{
		return $this->db->get('pengajar')->result();
	}
	
	function get_pengajar_by_nip()
	{		
        $nip = $this->uri->segment(3);
        
		$this->db->where('nip', $nip);
		$query = $this->db->get('pengajar');
		
        if( $query )
        {
            return $query->result();
        }
        
		return false;
	}
		
	function update_data_pengajar()
	{	
        $data = array
        (
            'nip' => $this->input->post('nip'),
            'nama' => $this->input->post('nama'),
            'jk' => $this->input->post('jk'),
            'alamat' => $this->input->post('alamat'),
            'ttl' => $this->input->post('ttl')            
        );
        
		$this->db->set($data);
		$this->db->where('nip', $data['nip']);
		$query = $this->db->update('pengajar', $data);
		
		return $query ? true : false;
	}
    
    public function delete_pengajar()
    {
        $nip = $this->uri->segment(3);
        
        $this->db->where('nip', $nip);
        $query = $this->db->delete('pengajar');
        
        return $query ? true : false;
    }
    
    function update_data_siswa()
	{	
        $data = array
        (
            'nis' => $this->input->post('nis'),
            'nama' => $this->input->post('nama'),
            'jk' => $this->input->post('jk'),
            'alamat' => $this->input->post('alamat'),
            'ttl' => $this->input->post('ttl')            
        );
        
		$this->db->set($data);
		$this->db->where('nis', $data['nis']);
		$query = $this->db->update('siswa', $data);
		
		return $query ? true : false;
	}
    
    public function delete_siswa()
    {
        $nis = $this->uri->segment(3);
        
        $this->db->where('nis', $nis);
        $query = $this->db->delete('siswa');
        
        return $query ? true : false;
    }
	
	public function get_all_pengajar_mapel_kelas()
	{
		$this->db->select('pengajar_mapel.id');
		$this->db->select('pengajar.nip');
		$this->db->select('pengajar.nama');
		$this->db->select('kelas.kelas');
		$this->db->select('mapel.mapel');		
		$this->db->from('pengajar_mapel');
		$this->db->join('pengajar', 'pengajar_mapel.nip = pengajar.nip');
		$this->db->join('kelas', 'pengajar_mapel.kelas = kelas.kd_kelas');
		$this->db->join('mapel', 'pengajar_mapel.kd_mapel = mapel.kd_mapel');
		$this->db->order_by('nip', 'ASC');		
		return $this->db->get()->result();
	}
	
	public function set_kelas_mapel_pengajar()
	{
		$kelas = $this->input->post('kelas');
		$mapel = $this->input->post('mapel');
		$nip = $this->input->post('nip');
		
		$data = array
		(
			'kd_mapel' => $mapel,
			'kelas' => $kelas,
			'nip' => $nip
		);
		
		$cari = $this->db->get_where('pengajar_mapel', $data)->num_rows();
		
		if($cari === 0){
			$this->db->insert('pengajar_mapel', $data);
			return true;
		}
		
		return false;
	}
	
	public function cari_data_pengajar()
	{
		$nip = $this->input->post('nip');		
		
		$data = array
		(
			'nip' => $nip
		);
		
		return $this->db->get_where('pengajar', $data)->row_array();
	}
	
	public function cari_data_pengajar_kelas_mapel()
	{
		$nip = $this->input->post('nip');				
		
		$this->db->select('pengajar.nip');
		$this->db->select('pengajar.nama');
		$this->db->select('kelas.kelas');
		$this->db->select('mapel.mapel');		
		$this->db->from('pengajar_mapel');
		$this->db->join('pengajar', 'pengajar_mapel.nip = pengajar.nip');
		$this->db->join('kelas', 'pengajar_mapel.kelas = kelas.kd_kelas');
		$this->db->join('mapel', 'pengajar_mapel.kd_mapel = mapel.kd_mapel');
		$this->db->where('pengajar_mapel.nip', $nip);
		return $this->db->get()->result_array();
		
		//return $this->db->get_where('pengajar_mapel', $data)->row_array();
	}
	
	public function tambah_kelas_mapel_pengajar()
	{
		$data = array(
			'nip' 		=> $this->input->post('nip'),			
			'kelas' 	=> $this->input->post('kelas'),
			'kd_mapel'	=> $this->input->post('mapel')		
		);
						
		if($this->db->get_where('pengajar_mapel', $data)->num_rows() == 0)
		{
			$this->db->insert('pengajar_mapel', $data);			
			return TRUE;
		}
				
		return FALSE;				
	}
	/**Pengajar**/
	
	/**Kelas**/
	function get_all_kelas()
	{
		return $this->db->get('kelas')->result();
	}
    
    public function get_kelas_by_id()
    {
        $kd_kelas = $this->uri->segment(3);
        
        $this->db->where('kd_kelas', $kd_kelas);
        $query = $this->db->get('kelas')->result();
        
        return $query ? $query : false;
    }
    
    public function update_kelas()
    {
        $kd_kelas = $this->uri->segment(3);
        
        $data = array
        (
            'kd_kelas' => $this->input->post('kd_kelas'),
            'kelas' => $this->input->post('kelas')
        );
        
        $this->db->set($data);
		$this->db->where('kd_kelas', $data['kd_kelas']);
		$query = $this->db->update('kelas', $data);
        
        return $query ? true : false;
    }
	
	function get_all_siswa_perkelas_coeg()
	{
		$kd_kelas = $this->uri->segment(3);
		
		$this->db->select('siswa.nis, siswa.nama');
		$this->db->from('kelas_siswa');
		$this->db->join('siswa', 'kelas_siswa.nis = siswa.nis');		
		$this->db->where('kelas_siswa.kd_kelas', $kd_kelas);
		return $this->db->get()->result();
	}
	
	function get_all_siswa_perkelas()
	{
		$data_siswa = 'siswa.nis, siswa.nama, nilai_siswa.pengetahuan_grade, nilai_siswa.keterampilan_grade, nilai_siswa.keterampilan_angka, nilai_siswa.pengetahuan_angka';
		
		$kd_kelas = $this->uri->segment(3);
		
		$this->db->select($data_siswa);
		$this->db->from('nilai_siswa');
		$this->db->join('siswa', 'nilai_siswa.nis = siswa.nis');
		$this->db->join('mapel', 'nilai_siswa.id_mapel = mapel.kd_mapel');
		$this->db->join('kelas_siswa', 'nilai_siswa.nis = kelas_siswa.nis');
		//$this->db->join('kelas', 'kelas_siswa.kd_kelas = kelas.kd_kelas');
		$this->db->where('kd_kelas', $kd_kelas);
		
		return $this->db->get()->result();
	}
	/**Kelas**/
	
	/**Mapel**/
	function get_all_mapel()
	{
		return $this->db->get('mapel')->result();
	}
    
    public function get_mapel_by_id()
    {
        $id_mapel = $this->uri->segment(3);
        
        $this->db->where('kd_mapel', $id_mapel);
        $query = $this->db->get('mapel')->result();
        
        return $query ? $query : false;
    }
    
    public function tambah_mapel()
	{
		$data = array(
			'kd_mapel' 	 => $this->input->post('kd_mapel'),
			'mapel' => $this->input->post('mapel'),
			'kkm' 		 => $this->input->post('kkm')
		);
				
		if($this->db->get_where('mapel', array('kd_mapel'=>$data['kd_mapel']))->num_rows() == 0)
		{
			$this->db->insert('mapel', $data);			
			return TRUE;
		}
				
		return FALSE;				
	}
    
    public function update_mapel()
    {
        $data = array
        (
            'kd_mapel' => $this->input->post('id_mapel'),
            'mapel' => $this->input->post('mapel'),
            'kkm' => $this->input->post('kkm')
        );
        
		$this->db->set($data);
		$this->db->where('kd_mapel', $data['kd_mapel']);
		$query = $this->db->update('mapel', $data);
		
		return $query ? true : false;
    }
    
    public function delete_mapel()
    {
        $kd_mapel = $this->uri->segment(3);
        
        $this->db->where('kd_mapel', $kd_mapel);
        $query = $this->db->delete('mapel');
        
        return $query ? true : false;
    }
	/**Mapel**/
	
	/**Siswa**/
	public function get_all_siswa()
	{
		return $this->db->get('siswa')->result();
	}
    
    public function get_siswa_by_nis()
    {
        $nis = $this->uri->segment(3);
        
        $this->db->where('nis', $nis);
        $query = $this->db->get('siswa')->result();
        
        return $query ? $query : false;
    }
	
	public function cek_nis_siswa()
	{
		$nis = $this->input->post('nis');
		
		return $this->db->get_where('siswa', array('nis' => $nis))->row_array();
	}
	
	public function tambah_nilai_siswa_null()
	{
		$nis = $this->input->post('nis');
		
		//$this->db->insert()
	}
	/**Siswa**/
	
	/**Semester**/
	public function tambah_tahun_smt()
	{
		$data = array(
			'tahun_pelajaran' 		=> $this->input->post('tahun_pelajaran'),
			'smt' 					=> $this->input->post('smt'),
			'status'				=> 'Tidak Aktif'
		);
						
		if($this->db->get_where('smt_aktif', $data)->num_rows() == 0)
		{
			$this->db->insert('smt_aktif', $data);			
			return TRUE;
		}
				
		return FALSE;		
	}
	/**Semester**/
    
    /**Kelola Akun Siswa dan Pengaja**/
    public function reset_password()
    {
        $nis_nip = $this->input->post('nis_nip');
        $new_pass = md5( $this->input->post('new_pass') );        
        
        $data = array
        (
            'password' => $new_pass
        );
        
        $this->db->set($data);
        $this->db->where('username', $nis_nip);
        $query = $this->db->update('akun', $data);
        
        if( $query )
        {
            return true;
        }
        
        return false;
    }
    
    public function ganti_password()
    {
        $nis_nip = $this->input->post('nis_nip');
        $old_pass = md5( $this->input->post('old_pass') );        
        $new_pass = md5( $this->input->post('new_pass') );        
        
        $data = array
        (
            'password' => $new_pass
        );
        
        $this->db->set($data);
        $this->db->where('username', $nis_nip);
        $this->db->where('password', $old_pass);
        $query = $this->db->update('akun', $data);
        
        if( $query )
        {
            return true;
        }
        
        return false;
    }
    /**Kelola Akun Siswa dan Pengaja**/
	
    /**Ekskul**/
    
    public function tambah_ekskul()
    {
        $data = array(
			'id_ekskul' => $this->input->post('id_ekskul'),
			'ekskul'	=> $this->input->post('ekskul'),
			'jadwal'	=> $this->input->post('jadwal'),
			'lokasi'	=> $this->input->post('lokasi')
		);
				
		if($this->db->get_where('ekskul', array('id_ekskul'=>$data['id_ekskul']))->num_rows() == 0)
		{
			$this->db->insert('ekskul', $data);			
			return true;
		}
		
		return false;
    }
    
    public function get_ekskul_by_id()
    {
        $id_ekskul = $this->uri->segment(3);
        
        $this->db->where('id_ekskul', $id_ekskul);
        $query = $this->db->get('ekskul')->result();
        
        return $query ? $query : false;
    }
    
    public function update_ekskul()
    {
        $data = array
        (
            'id_ekskul' => $this->input->post('id_ekskul'),
            'ekskul' => $this->input->post('ekskul'),
            'jadwal' => $this->input->post('jadwal'),
            'lokasi' => $this->input->post('lokasi')
        );
        
		$this->db->set($data);
		$this->db->where('id_ekskul', $data['id_ekskul']);
		$query = $this->db->update('ekskul', $data);
		
		return $query ? true : false;
    }
    
    public function delete_ekskul()
    {
        $id_ekskul = $this->uri->segment(3);
        
        $this->db->where('id_ekskul', $id_ekskul);
        $query = $this->db->delete('ekskul');
        
        return $query ? true : false;
    }
    
    /**Ekskul**/
}

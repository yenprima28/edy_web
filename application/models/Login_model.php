<?php

class Login_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function login()
	{	
		if($this->get_user())
		{
			return TRUE;
		}
		
		return FALSE;
	}
	
	public function get_user()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		
		$query = $this->db->get('akun');
        
		if($query->num_rows() == 1)
		{			
			return TRUE;			
		}
		
		return FALSE;
	}
	
	public function get_level()
	{
		$username = $this->input->post('username');		
		// $username = '990';
		
		$this->db->where('username', $username);		
		$this->db->select('level');
		$query = $this->db->get('akun');
		
        // Jika level adalah pengajar
        $query = $query->result_array();
        
        $arrJabatan = [];
        
        if(  $query[0]['level'] == 'pengajar' )
        {
            if( $this->get_jabatan() )
            {
                $arrJabatan = $this->get_jabatan();
            }
            
            if( $arrJabatan[0] )
            {
                if( $arrJabatan[0]['kd_jabatan'] = 'j002' )
                {
                    return 'wali kelas';
                }
                
                return 'guru';
            }
        } 
        // else 
        // {            
        return $query[0]['level'];
        // }
		// return $query->row_array();
	}
    
    public function get_jabatan()
    {
        $nip = $this->input->post('username');		
        // $nip = '990';
		
		$this->db->where('nip', $nip);
		$this->db->select('kd_jabatan');
		$query = $this->db->get('jabatan_pengajar');
        
        if( $query )
        {
            return $query->result_array();
        }
        
        return false;
    }
	
	public function count_level()
	{
		$nip = $this->input->post('username');		
		
		$this->db->where('nip', $nip);		
		$this->db->select('kd_jabatan');
		$query = $this->db->get('jabatan_pengajar');
		
		return $query->num_rows();
	}
}

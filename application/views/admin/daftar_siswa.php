
    <!-- Main content -->
    <section class="content">
    <!--Testing : Include CSS DataTable -->
    
    <!--Testing : Include CSS DataTable -->
      <!-- Default box -->
      <div class="box">             
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
                <table class="table table-striped table-bordered" id='list_siswa'>
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama</th>
                            <th>TTL</th>
                            <th>Alamat</th>
                            <th>Jenis Kelamin</th>
                            <th> Aksi </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php             
                    $no = 1;
                    foreach($siswa as $ls): 
                    ?>
                    
                    <tr>											                      
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $ls->nama; ?></td>
                      <td><?php echo $ls->ttl; ?></td>
                      <td><?php echo $ls->alamat; ?></td>
                      <td><?php echo $ls->jk; ?></td>
                      <td style="width: 5px">
                        <a href='<?= base_url('admin/edit_data_siswa/' . $ls->nis); ?>' class='btn btn-primary'> Edit </a>
                      </td>
                      <td style="width: 5px">
                        <a href='<?= base_url('admin/hapus_siswa/' . $ls->nis); ?>' class='btn btn-danger'> Hapus </a>
                      </td>
                    </tr>
                    
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
			
            <!-- /.box-body -->
            <div class="box-footer clearfix">           
            </div>          
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    
    <!-- Script JS -->
    <script>
    
    function runJquery()
    {
        // $(document).ready(function()
        // {            
                      
           $('#list_siswa').DataTable();
           
        // });
    }    
    
    </script>
    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
			
			<?php echo validation_errors(); ?>						

			<?php echo form_open('admin/tambah_siswa_perkelas'); ?> 
			
			<div class="box-body">
				<div class="form-group">
					<label for="nis"> NIS </label>					  					  				
						<input type="text" class="form-control" name='nis' id="nis" placeholder="Masukkan NIS...">											
				</div>				
				
				<input type="button" id='cek_data' class="btn btn-primary" value='Cek Data'>
				<div id='info_nis'></div>				
			             
				<label> Kelas </label>
				<select class="form-control select2" style="width: 100%;" name='kelas'>
					<option selected="selected"> --Pilih-- </option>
					<?php foreach($kelas as $ls): ?>				  
						<option value='<?= $ls->kd_kelas; ?>'> <?= $ls->kelas; ?> </option>				  
					<?php endforeach; ?>
				</select>								
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
				<input type="submit" name='submit' class="btn btn-primary" value='Simpan Data'>
            </div>
            <!-- </form> -->			
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
	
	<script>
	
	function runJquery()
	{
		
		$('#cek_data').click(function(e)
		{
			e.preventDefault();
			
			var nis = $('#nis').val();
			
			$.post('<?php echo base_url('admin/cek_nis_siswa'); ?>',
			{
				nis: nis
			}, 
			function(data)
			{
				console.log(data);
				
				var dataJsonParse = $.parseJSON(data);
				
				$('#info_nis').empty();
				
				if(dataJsonParse.nis_siswa != null)
				{
					$('#info_nis').append('Data ditemukan! Atas Nama : '+dataJsonParse.nis_siswa.nama);
				} 
				else 
				{					
					$('#info_nis').append('Data tidak ditemukan');
					
				}	
			});
		});
		
	}
	
	</script>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
              <table class="table table-bordered" id='list_mapel'>
                <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Kode Mapel</th>
                      <th>Nama Mapel</th>
                      <th>KKM</th>
                      <th> Aksi </th>
                      <th> </th>
                    </tr>
                </thead>
                <tbody>
				<?php				
				foreach($mapel as $ls): 
				?>
                
				<tr>											
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $ls->kd_mapel; ?></td>                 
                  <td><?php echo $ls->mapel; ?></td>
                  <td><?php echo $ls->kkm; ?></td>
				  <td style="width: 5px">							
						<a href='<?php echo base_url() . 'admin/edit_data_mapel/' . $ls->kd_mapel; ?>' class="btn btn-warning"> <i class='fa fa-edit'> Edit </i> </a>																	
  				  </td>
                  <td style="width: 5px">		
					<a href='<?php echo base_url() . 'admin/hapus_mapel/' . $ls->kd_mapel; ?>' class="btn btn-danger"> <i class='fa fa-delete'> Hapus </i> </a>
			      </td>
                </tr>                
				
				<?php
				endforeach;
				?>
                </tbody>
              </table>
            </div>
			
			
			
            <!-- /.box-body -->
            <div class="box-footer clearfix">              
            </div>          
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->
	
    </section>
    <!-- /.content -->
    
    <script>
    
    function runJquery()
    {
        $('#list_mapel').DataTable();
    }
    
    </script>
    
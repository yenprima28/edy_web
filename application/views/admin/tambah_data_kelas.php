    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>            
						
			<?php echo validation_errors(); ?>			
			
			 <?php echo form_open('admin/tambah_data_kelas'); ?> 
              <div class="box-body">
				<div class="form-group">
                  <label for="kd_kelas"> ID Kelas </label>
                  <input type="text" class="form-control" name='kd_kelas' id="kd_kelas" placeholder="Masukkan Nama Mapel...">
                </div>
                <div class="form-group">
                  <label for="kelas"> Kelas </label>
                  <input type="text" class="form-control" name='kelas' id="kelas" placeholder="Masukkan Nama Mapel...">
                </div>                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form"> -->
						
			<?php echo validation_errors(); ?>			
			
			
              <div class="box-body">
				<table class="table table-bordered">
					<tr>
					  <th style="width: 10px">#</th>
					  <th>NIP</th>
					  <th>Nama</th>                       
					  <th>Kelas</th>                       
					  <th>Mapel</th>				  
					  <th>Aksi</th>				  
					</tr>
					<?php foreach($pengajar as $ls): ?>
					
					<tr>											
					  <td><?php echo $no++; ?></td>
					  <td><?php echo $ls->nip; ?></td>                 
					  <td><?php echo $ls->nama; ?></td>					  					  
					  <td><?php echo $ls->kelas; ?></td>					  					  
					  <td><?php echo $ls->mapel; ?></td>					  					  					  
					  <td><a href='<?php echo base_url('admin/delete_Kelas_Mapel/' . $ls->id); ?>'>Hapus</a></td>
					</tr>                				
					<?php endforeach; ?>					
				</table>
					
				</div>
				<!-- /.box-body -->
				
				<div class="box-footer">			
				</div>
				<!-- </form> -->					
			</div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
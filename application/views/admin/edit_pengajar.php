    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form"> -->
						
			<?php echo validation_errors(); ?>			
			
			 <?php echo form_open('admin/edit_data_pengajar'); ?> 
			 <!--<?php foreach($pengajar as $ls): ?>-->
              <div class="box-body">
                <div class="form-group">
                  <label for="nip"> NIP </label>
                  <input type="text" class="form-control" name='nip' value='<?= $ls->nip; ?>' placeholder="Masukkan NIP...">
                </div>
                <div class="form-group">
                  <label for="nama"> Nama Lengkap </label>
                  <input type="text" class="form-control" name='nama' value='<?= $ls->nama; ?>' placeholder="Masukkan Nama Lengkap...">
                </div>
				<div class="form-group">
					<label> Jenis Kelamin </label>
					<select class="form-control select2" style="width: 100%;" name='jk'>
					<?php if($ls->jk == 'L'){ ?>
					  <option> --Pilih-- </option>
					  <option value='L' selected> Laki - Laki</option>
					  <option value='P'> Perempuan </option>					  
					<?php } else if($ls->jk == 'P'){ ?>
					  <option> --Pilih-- </option>
					  <option value='L'> Laki - Laki</option>
					  <option value='P' selected> Perempuan </option>					  
					<?php } else { ?>
					  <option selected> --Pilih-- </option>
					  <option value='L' selected> Laki - Laki</option>
					  <option value='P'> Perempuan </option>					  					
					<?php } ?>
					</select>
				</div>
                <div class="form-group">
                  <label for="alamat"> Alamat </label>
                  <textarea class="form-control" name='alamat' placeholder="Alamat..."><?= $ls->alamat; ?></textarea>
                </div>
				<div class="form-group">
					<label> TTL </label>
					<h6 class="help-block"> <i> Format : Tahun/Bulan/Tanggal </i> </h6>
					<div class="input-group date">
					  <div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					  </div>
					  <input type="text" class="form-control pull-right" name='ttl' id="datepicker" data-date-format='yyyy-mm-dd' value='<?= $ls->ttl; ?>'>
					</div>
                </div>                
                <div class="form-group">
				  <img src='<?= base_url('assets/foto/pengajar/') . $ls->gambar; ?>' width='200' height='200'></img><br/>

                  <label for="exampleInputFile"> Gambar Pengajar </label>
                  <input type="file" name='gambar' id="exampleInputFile" '>				  
                  <!--<p class="help-block"> Example block-level help text here. </p>-->
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<!--<?php endforeach; ?>-->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
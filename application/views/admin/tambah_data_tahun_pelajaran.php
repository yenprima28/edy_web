    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>            
						
			<?php echo validation_errors(); ?>						
			
			 <?php echo form_open('admin/tambah_data_tahun_pelajaran'); ?> 
              <div class="box-body">
				<div class="form-group">
                  <label for="tahun_pelajaran"> Tahun Pelajaran </label>
                  <input type="text" class="form-control" name='tahun_pelajaran' id="tahun_pelajaran" placeholder="Masukkan tahun pelajaran...">
                </div>
                <div class="form-group">
                  <label for="smt"> Semester </label>
                  <input type="text" class="form-control" name='smt' id="smt" placeholder="Masukkan semester...">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->  
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
              <table class="table table-bordered" id='list_pengajar'>
                <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>NIP</th>
                      <th>Nama</th>
                      <th>Jenis Kelamin</th>
                      <th>Alamat</th>
                      <th>TTL</th>
                      <th> Aksi </th>
                      <th> </th>
                    </tr>
                </thead>
                <tbody>
				<?php				
				foreach($pengajar as $ls): 
				?>
                
				<tr>											
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $ls->nip; ?></td>                 
                  <td><?php echo $ls->nama; ?></td>                 
                  <td><?php echo $ls->jk; ?></td>                                   
                  <td><?php echo $ls->alamat; ?></td>                                   
                  <td><?php echo $ls->ttl; ?></td>                                   
				  <td style="width: 5px">	
					<a href='<?php echo base_url() . 'admin/edit_data_pengajar/' . $ls->nip; ?>' class="btn btn-warning"> <i class='fa fa-edit'> Edit </i> </a>														
  				  </td>
                  <td style="width: 5px">		
					<a href='<?php echo base_url() . 'admin/hapus_pengajar/' . $ls->nip; ?>' class="btn btn-danger"> <i class='fa fa-delete'> Hapus </i> </a>
			      </td>
                </tr>                
				
				<?php
				endforeach;
				?>
                </tbody>
              </table>
            </div>
			
			
			
            <!-- /.box-body -->
            <div class="box-footer clearfix">    
            </div>          
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->	
	
    </section>
    <!-- /.content -->
    
    <script>
    
    function runJquery()
    {
        $('#list_pengajar').DataTable();
    }
    
    </script>
    
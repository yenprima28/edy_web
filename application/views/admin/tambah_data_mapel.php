    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>            
						
			<?php echo validation_errors(); ?>						
			
			 <?php echo form_open('admin/tambah_data_mapel'); ?> 
              <div class="box-body">
				<div class="form-group">
                  <label for="id_mapel"> ID Mata Pelajaran </label>
                  <input type="text" class="form-control" name='kd_mapel' id="id_mapel" placeholder="Masukkan Nama Mapel...">
                </div>
                <div class="form-group">
                  <label for="mapel"> Mata Pelajaran </label>
                  <input type="text" class="form-control" name='mapel' id="mapel" placeholder="Masukkan Nama Mapel...">
                </div>
                <div class="form-group">
                  <label for="kkm"> KKM </label>
                  <input type="text" class="form-control" name='kkm' id="kkm" placeholder="Masukkan Nilai KKM...">
                </div>				
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->  
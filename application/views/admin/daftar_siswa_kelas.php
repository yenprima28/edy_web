    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php //echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
			  <!--
			  <p>
				<a href='<?php echo base_url(); ?>admin/tambah_siswa_perkelas' class='btn btn-primary'> Tambah Siswa </a>
			  </p>
			  -->
              <table class="table table-bordered" id='list_siswa'>
                <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>NIS</th>
                      <th>Nama Siswa</th>
                      <th> Aksi </th>
                    </tr>
                </thead>
                <tbody>
				<?php
				$no = 1;
				foreach($siswa_kelas as $ls): 
				?>
                
				<tr>											
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $ls->nis; ?></td>                 
                  <td><?php echo $ls->nama; ?></td>                                   								 
                  <td style="width: 5px">		
						<a href='<?php echo base_url('admin/hapus_siswa/' . $ls->nis); ?>' class="btn btn-danger"> Hapus </a>
			      </td>
                </tr>                
				<?php
				endforeach;
				?>
				</tbody>
              </table>
            </div>
			
			
			
            <!-- /.box-body -->
            <div class="box-footer clearfix">            
            </div>          
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    
    <script>
    
    function runJquery()
    {
        $('#list_siswa').DataTable();
    }
    
    </script>
    
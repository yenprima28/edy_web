    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form"> -->
						
			<?php echo validation_errors(); ?>			
			
			 <?php echo form_open('admin/edit_data_kelas/'); ?> 
			 <!--<?php foreach($kelas as $ls): ?>-->
              <div class="box-body">
                <div class="form-group">
                  <label for="id_kelas"> ID Kelas </label>
                  <input type="text" class="form-control" name='kd_kelas' value='<?= $ls->kd_kelas; ?>'>
                </div>
                <div class="form-group">
                  <label for="kelas"> Nama Kelas </label>
                  <input type="text" class="form-control" name='kelas' value='<?= $ls->kelas; ?>'>
                </div>				
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<!--<?php endforeach; ?>-->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

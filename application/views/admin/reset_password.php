    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
			
			<?php echo validation_errors(); ?>						

			<?php echo form_open('admin/reset_password_siswa_pengajar'); ?> 
			
			<div class="box-body">
				<div class="form-group">
					<label for="nis_nip"> NIS/NIP </label>
					<input type="text" class="form-control" name='nis_nip' id="nis_nip_id" placeholder="Masukkan NIS/NIP...">											
				</div>				
                <div class="form-group">
					<label for="new_pass"> Password Baru </label>
					<input type="text" class="form-control" name='new_pass' id="new_pass_id" placeholder="Masukkan Password Baru...">											
				</div>				
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
				<input type="submit" name='submit' class="btn btn-primary" value='Simpan Data'>
            </div>
            <!-- </form> -->			
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
		
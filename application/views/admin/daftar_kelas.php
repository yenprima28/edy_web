    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
              <table class="table table-bordered" id='list_kelas'>
                <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>ID Kelas</th>
                      <th>Nama Kelas</th>
                      <th> Aksi </th>
                      <th> </th>
                      <th> </th>
                    </tr>
                </thead>
                <tbody>
				<?php foreach($kelas as $ls): ?>                
				<tr>											
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $ls->kd_kelas; ?></td>                 
                  <td><?php echo $ls->kelas; ?></td>                                   
				  <td style="width: 5px">							
						<a href='<?php echo base_url() . 'admin/daftar_siswa_kelas/' . $ls->kd_kelas; ?>' class="btn btn-info"> <i class='fa fa-edit'> Daftar Siswa </i> </a>
				  </td>
				  <td style="width: 5px">							
						<a href='<?php echo base_url() . 'admin/edit_data_kelas/' . $ls->kd_kelas; ?>' class="btn btn-warning"> <i class='fa fa-edit'> Edit </i> </a>
  				  </td>						  				  
                  <td style="width: 5px">		
						<a href='<?php echo base_url() . 'admin/hapus_siswa/' . $ls->kd_kelas; ?>' class="btn btn-danger"> <i class='fa fa-delete'> Hapus </i> </a>
			      </td>
                </tr>                
				
				<?php endforeach; ?>
                </tbody>
              </table>
            </div>
			
            <!-- /.box-body -->
            <div class="box-footer clearfix">              
            </div>          
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->  
    
    <script>
    
    function runJquery()
    {
        $('#list_kelas').DataTable();
    }
    
    </script>
    
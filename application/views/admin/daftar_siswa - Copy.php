<!--header-->
<?php include_once 'includes/header.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php include_once 'includes/header-top.php'; ?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <?php include_once 'includes/side_menu.php'; ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator
        <small>Anda Coeg Sekalehh...:v</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title_dashboard; ?></h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th colspan='2'> Aksi </th>
                </tr>
				<?php
				$no = 1;
				foreach($siswa as $ls): 
				?>
                
				<tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $ls->nama_lengkap; ?></td>                 
                  <td><?php echo $ls->nama_lengkap; ?></td>                 
                  <td style="width: 5px">					
						<a href='<?php echo base_url() . 'admin/update_siswa/' . $ls->nis; ?>' class="btn btn-warning"> <i class='fa fa-edit'> Edit </i> </a>					
  				  </td>
                  <td style="width: 5px">		
					<a href='<?php echo base_url() . 'admin/hapus_siswa/' . $ls->nis; ?>' class="btn btn-danger"> <i class='fa fa-delete'> Hapus </i> </a>
			      </td>
                </tr>                
				
				<?php
				endforeach;
				?>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>          
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include_once 'includes/footer.php'; ?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

  <?php include_once 'includes/footer-js.php'; ?>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form"> -->
						
			<?php echo validation_errors(); ?>			
			
			 <?php echo form_open('admin/edit_data_ekskul'); ?> 
			 <!--<?php foreach($ekskul as $ls): ?>-->
              <div class="box-body">
                <div class="form-group">
                  <label for="id_ekskul"> ID Ekskul </label>
                  <input type="text" class="form-control" name='id_ekskul' value='<?= $ls->id_ekskul; ?>' placeholder="Masukkan NIP...">
                </div>
                <div class="form-group">
                  <label for="ekskul"> Nama Ekskul </label>
                  <input type="text" class="form-control" name='ekskul' value='<?= $ls->ekskul; ?>' placeholder="Masukkan Nama Lengkap...">
                </div>				
                <div class="form-group">
                  <label for="jadwal"> Jadwal </label>
                  <input type="text" class="form-control" name='jadwal' value='<?= $ls->jadwal; ?>' placeholder="Masukkan Jadwal Ekskul...">
                </div>
                <div class="form-group">
                  <label for="lokasi"> Lokasi </label>
                  <input type="text" class="form-control" name='lokasi' value='<?= $ls->lokasi; ?>' placeholder="Masukkan Lokasi Ekskul...">
                </div>
              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<!--<?php endforeach; ?>-->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

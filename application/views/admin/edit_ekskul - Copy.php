	<!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>            
						
			<?php echo validation_errors(); ?>			
			
			<?php echo form_open('admin/tambah_data_ekskul'); ?> 
              <div class="box-body">
                <div class="form-group">
                  <label for="id_ekskul"> ID Ekskul </label>
                  <input type="text" class="form-control" name='id_ekskul' placeholder="Masukkan ID Ekskul...">
                </div>
                <div class="form-group">
                  <label for="ekskul"> Nama Ekskul </label>
                  <input type="text" class="form-control" name='ekskul' placeholder="Masukkan Nama Ekskul...">
                </div>				
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include_once 'includes/footer.php'; ?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

  <?php include_once 'includes/footer-js.php'; ?>
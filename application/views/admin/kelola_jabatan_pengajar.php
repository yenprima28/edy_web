    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form"> -->
						
			<?php echo validation_errors(); ?>			
						
			<?php echo form_open('admin/kelola_jabatan_pengajar'); ?> 
              <div class="box-body">
				<table class="table table-bordered">
					<tr>
					  <th style="width: 10px">#</th>
					  <th>NIP</th>
					  <th>Nama</th>                                    
					  <th colspan='2'> Atur Jabatan </th>                  
					</tr>
					<?php foreach($pengajar as $ls): ?>
					
					<tr>											
					  <td><?php echo $no++; ?></td>
					  <td><?php echo $ls->nip; ?></td>                 
					  <td><?php echo $ls->nama; ?></td>
					  <td style="width: 5px">
						<a href='<?php echo base_url() . 'admin/set_jabatan/j001/' . $ls->nip; ?>' class="btn btn-default"> <i class='fa fa-edit'> Guru </i> </a>														
					  </td>
					  <td style="width: 5px">		
						<a href='<?php echo base_url() . 'admin/set_jabatan/j002/' . $ls->nip; ?>' class="btn btn-default"> <i class='fa fa-delete'> Wali Kelas </i> </a>
					  </td>				  
					</tr>                
				
					<?php endforeach; ?>
				</table>
					
				</div>
				<!-- /.box-body -->

				<div class="box-footer">					
			  </div>
				<!-- </form> -->
			<?php echo form_close(); ?>
			</div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
						
			<?php echo validation_errors(); ?>						
			
			 <?php echo form_open('admin/kelola_pengajar_mapel'); ?> 
              <div class="box-body">				
				<!--Kelas
				<div class="form-group">
					<label for="kelas"> Pilih Kelas </label>
					<select name='Kelas' class="form-control">
						<option value='K001'>X A</option>
						<option value='K002'>X B</option>
						<option value=''>XI IPA A</option>
						<option value=''>XI IPA B</option>
						<option value=''>XI IPS A</option>
						<option value=''>XI IPS B</option>
					</select>
				</div>
				-->
				<!--Kategori Mapel
				<div class="form-group">
					<label for="kat_mapel"> Pilih Kategori Mapel </label>
					<select name='kat_mapel' class="form-control">
						<option value=''>IPA</option>
						<option value=''>IPS</option>						
					</select>
				</div>
				-->
				<div class="form-group">
					<label for="mapel"> Pilih Mapel </label>
					<select name='mapel' class="form-control">
					<?php foreach($mapel as $ls): ?>
						<option value='<?php echo $ls->id_mapel; ?>'><?php echo $ls->nama_mapel; ?></option>
					<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
                  <label for="pengajar[]" class="custom-control custom-checkbox"> Pilih Pengajar </label>				  
				  <br/>
				  
				  <?php foreach($pengajar as $ls): ?>
                  <input type="radio" class="custom-control-input" name='pengajar[]' id="pengajar" value='<?php echo $ls->nip; ?>'>
				  <span class="custom-control-indicator"></span>
				  <span class="custom-control-description"><?php echo $ls->nama_lengkap; ?></span>
				  <br/>
				  <?php endforeach; ?>
				  
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
			  <!--
			  <p>
				<a href='<?php echo base_url(); ?>admin/tambah_siswa_perkelas' class='btn btn-primary'> Tambah Siswa </a>
			  </p>
			  -->
              <table class="table table-bordered" id='list_tahun'>
                <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Tahun</th>
                      <th>Semester</th>
                      <th>Status</th>
                      <th> Aksi </th>
                      <th> </th>
                    </tr>
                </thead>
                <tbody>
				<?php
				$no = 1;
				foreach($semester_aktif as $ls): 
				?>
                
				<tr>											
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $ls->tahun_pelajaran; ?></td>                 
                  <td><?php echo $ls->smt; ?></td>                                   				
                  <td><?php echo $ls->status; ?></td>
				  <td style="width: 5px">							
						<a href='<?php echo base_url() . 'admin/aktifkan_semester/' . $ls->id; ?>' class="btn btn-warning"> <i class='fa fa-edit'> Aktifkan </i> </a>
  				  </td>						  				  
                  <td style="width: 5px">		
						<a href='<?php echo base_url() . 'admin/hapus_semester/' . $ls->id; ?>' class="btn btn-danger"> <i class='fa fa-delete'> Hapus </i> </a>
			      </td>
                </tr>                
				
				<?php
				endforeach;
				?>
                </tbody>
              </table>
            </div>
			
			
			
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href='<?php echo base_url() . 'admin/matikan_semester'?>' class="btn btn-danger"> <i class='fa fa-delete'> Matikan Semester Aktif </i> </a>
            </div>          
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    
    
    <script>
    
    function runJquery()
    {
        $('#list_tahun').DataTable();
    }
    
    </script>
    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>            
						
			<?php echo validation_errors(); ?>			
			
			 <?php echo form_open('admin/tambah_pengajar_kelas'); ?> 
                <div class="box-body">
					<div class="form-group">
						<label for='nip'>NIP</label>				
						<input id='nip' type='text' class='form-control' name='nip' placeholder='Input NIP Disini'/>
						<br/>
						<button id="cari" name='cari' class="btn btn-primary"> Cari Data </button>
					</div>
					<div class="form-group">
						<label for='nama'>Nama</label>
						<input type='text' class='form-control' name='nama' id='nama' />
					</div>
					<hr>
					<div id='info-pengajar'></div>
					<hr>
					<div class="form-group">
						<label for='kelas'>Kelas</label>
						<select name='kelas' class='form-control'>
							<option value=''>~Pilih Kelas~</option>
							<?php foreach($kelas as $ls): ?>
							<option value='<?= $ls->kd_kelas; ?>'><?= $ls->kelas; ?></option>
							<?php endforeach; ?>
						</select>					
					</div>
					<div class="form-group">
						<label for='mapel'>Mapel</label>
						<select name='mapel' class='form-control'>
							<option value=''>~Pilih Mapel~</option>							
							<?php foreach($mapel as $ls): ?>
							<option value='<?= $ls->kd_mapel; ?>'><?= $ls->mapel; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
	
	<script>
	
	function runJquery()
	{
		
		$('#cari').click(function(e)
		{
			e.preventDefault();			
			
			var nip = $('#nip').val();
			
			$.post('<?php echo base_url('admin/cari_pengajar'); ?>',
			{
				nip: nip
			},
			function(data)
			{
				//console.log(data);				
				
				var dataJsonParse = $.parseJSON(data);				
				
				//console.log(dataJsonParse.status);
				
				if(dataJsonParse.status == false)
				{
					//console.log(dataJsonParse.pesan);
					alert(dataJsonParse.pesan);
					$('#nama').attr('value', null);
					$('#info-pengajar').empty();
				}
				else
				{											
					$('#nama').attr('value', dataJsonParse.kelas_mapel[0].nama);
					
					//menampilkan data kelas yang (sudah) diajar
					$('#info-pengajar').empty();
					$('#info-pengajar').append('<h3><u>Daftar Kelas dan Mapel Pengajar<u><h3>');
					
					var no = 1;
					$.each(dataJsonParse.kelas_mapel, function(k, v)
					{
						//console.log(v.kelas);
						$('#info-pengajar').append
						(
							'<p>'+(no++)+'. Kelas: '+v.kelas+
							' || Mapel: '+v.mapel+
							'</p>'
						);
					});
				}
				
			});
			
		});
		
	}
	
	</script>
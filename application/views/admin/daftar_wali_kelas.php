    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Kelas</th>                  
                  <th colspan='3'> Aksi </th>
                </tr>
				<?php foreach($walikelas as $ls): ?>
                
				<tr>											
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $ls->nip; ?></td>                 
                  <td><?php echo $ls->nama; ?></td>                 
                  <td><?php echo $ls->kelas; ?></td>                                   
				  <td style="width: 5px">	
					<a href='<?php echo base_url() . 'admin/edit_data_pengajar/' . $ls->nip; ?>' class="btn btn-warning"> <i class='fa fa-edit'> Edit </i> </a>														
  				  </td>
                  <td style="width: 5px">		
					<a href='<?php echo base_url() . 'admin/hapus_pengajar/' . $ls->nip; ?>' class="btn btn-danger"> <i class='fa fa-delete'> Hapus </i> </a>
			      </td>
                </tr>                
				
				<?php endforeach; ?>
              </table>
            </div>
			
			
			
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>          
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->
	
	<button id='asd'>Klik saya</button>
	
    </section>
    <!-- /.content -->
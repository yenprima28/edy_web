<!--header-->
<?php include_once 'includes/header.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php include_once 'includes/header-top.php'; ?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <?php include_once 'includes/side_menu.php'; ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator
        <small>Anda Coeg Sekalehh...:v</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title_dashboard; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form"> -->
						
			<?php echo validation_errors(); ?>			
			
			<?php echo $error;?>
			
			 <?php echo form_open('admin/insert_mapel_kelas_db'); ?> 
			 <div class="box-body">
					<div class="form-group">
						<label for="mapel"> Mata Pelajaran </label>					  					  																  
						<select class="form-control select2" style="width: 100%;" name='mapel'>
						  <option selected="selected"> --Pilih-- </option>
						  <?php 
						  foreach($mapel as $ls):
						  ?>
						  
						  <option value='<?= $ls->id_mapel; ?>'> <?= $ls->nama_mapel; ?> </option>
						  
						  <?php endforeach; ?>
						</select>		
					</div>         								
					<div class="form-group">													
						<label> Kelas </label>
						<select class="form-control select2" style="width: 100%;" name='kelas'>
						  <option selected="selected"> --Pilih-- </option>
						  <?php 
						  foreach($kelas as $ls):
						  ?>
						  
						  <option value='<?= $ls->id_kelas; ?>'> <?= $ls->kelas; ?> </option>
						  
						  <?php endforeach; ?>
						</select>					
					</div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->			
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div> 
  <!-- /.content-wrapper -->

  <?php include_once 'includes/footer.php'; ?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

  <?php include_once 'includes/footer-js.php'; ?>
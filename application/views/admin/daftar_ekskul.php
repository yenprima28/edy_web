    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
              <table class="table table-bordered" id='list_ekskul'>
                <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>ID Kelas</th>
                      <th>Nama Kelas</th>
                      <th>Jadwal</th>
                      <th>Lokasi</th>
                      <th> Aksi </th>
                      <th> </th>
                    </tr>
                </thead>
                <tbody>
				<?php foreach($ekskul as $ls): ?>
                
				<tr>											
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $ls->id_ekskul; ?></td>                 
                  <td><?php echo $ls->ekskul; ?></td>                                   
                  <td><?php echo $ls->jadwal; ?></td>                                   
                  <td><?php echo $ls->lokasi; ?></td>                                   
				  <td style="width: 5px">							
						<a href='<?php echo base_url() . 'admin/edit_data_ekskul/' . $ls->id_ekskul; ?>' class="btn btn-warning"> <i class='fa fa-edit'> Edit </i> </a>																	
  				  </td>
						
  				  </td>
                  <td style="width: 5px">		
						<a href='<?php echo base_url() . 'admin/hapus_ekskul/' . $ls->id_ekskul; ?>' class="btn btn-danger"> <i class='fa fa-delete'> Hapus </i> </a>
			      </td>
                </tr>                
				
				<?php endforeach; ?>
                </tbody>
              </table>
            </div>
						
            <!-- /.box-body -->
            <div class="box-footer clearfix">              
            </div>          
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    
    
    <script>
    
    function runJquery()
    {
        $('#list_ekskul').DataTable();
    }
    
    </script>
    
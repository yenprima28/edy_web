    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form"> -->
						
			<?php echo validation_errors(); ?>			
			
			 <?php echo form_open('admin/edit_data_mapel/'); ?> 
			 <!--<?php foreach($mapel as $ls): ?>-->
              <div class="box-body">
                <div class="form-group">
                  <label for="id_mapel"> ID Mapel </label>
                  <input type="text" class="form-control" name='id_mapel' value='<?= $ls->kd_mapel; ?>' placeholder="Masukkan Kode Mapel...">
                </div>
                <div class="form-group">
                  <label for="mapel"> Nama Mapel </label>
                  <input type="text" class="form-control" name='mapel' value='<?= $ls->mapel; ?>' placeholder="Masukkan Nama Mapel...">
                </div>				
              	<div class="form-group">
                  <label for="kkm"> KKM </label>
                  <input type="text" class="form-control" name='kkm' value='<?= $ls->kkm; ?>' placeholder="Masukkan KKM...">
                </div>				
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<!--<?php endforeach; ?>-->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->            
						
			<?php echo validation_errors(); ?>			
			
			 <?php echo form_open_multipart('admin/tambah_data_siswa'); ?> 
              <div class="box-body">
                <div class="form-group">
                  <label for="nis"> NIS </label>
                  <input type="text" class="form-control" name='nis' id="nis" placeholder="Masukkan NIS...">
                </div>
                <div class="form-group">
                  <label for="nama"> Nama Lengkap </label>
                  <input type="text" class="form-control" name='nama' id="nama" placeholder="Masukkan Nama Lengkap...">
                </div>
				<div class="form-group">
					<label> Jenis Kelamin </label>
					<select class="form-control select2" style="width: 100%;" name='jk'>
					  <option selected="selected"> --Pilih-- </option>
					  <option value='L'> Laki - Laki</option>
					  <option value='P'> Perempuan </option>					  
					</select>
				</div>
                <div class="form-group">
                  <label for="alamat"> Alamat </label>
                  <textarea class="form-control" name='alamat' id="alamat" placeholder="Alamat..."></textarea>
                </div>
				<div class="form-group">
					<label> TTL </label>
					<h6 class="help-block"> <i> Format : Tahun/Bulan/Tanggal </i> </h6>
					<div class="input-group date">
					  <div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					  </div>
					  <input type="text" class="form-control pull-right" name='ttl' id="datepicker" data-date-format='yyyy-mm-dd'>
					</div>
                </div>                
                <div class="form-group">
                  <label for="exampleInputFile"> Gambar Siswa </label>
                  <input type="file" name='gambar' id="exampleInputFile">

                  <!--<p class="help-block"> Example block-level help text here. </p>-->
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->

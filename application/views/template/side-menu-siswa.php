<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
         <?php            
            $gambar = $this->siswa_model->get_gambar();
            
            // echo $gambar[0]->gambar;
            
            // var_dump( $gambar[0]->gambar );
            
            if( $gambar[0]->gambar != '' ){
            ?>            
                <img src="<?php echo base_url('assets/foto/siswa/' . $gambar[0]->gambar); ?>" class="img-circle" alt="User Image">          
            <?php } else { ?>
                <img src="<?php echo base_url('assets/foto/siswa/user1-128x128.jpg'); ?>" class="img-circle" alt="User Image">          
            <?php } ?>
        </div>
        <div class="pull-left info">          
          <p>NIS : <?php echo $this->session->username; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Siswa </a>
        </div>
      </div>      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kelola Profil</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('siswa/profil'); ?>"><i class="fa fa-circle-o"></i> Profil Saya </a></li>            
            <li><a href="<?php echo base_url('siswa/ganti_password'); ?>"><i class="fa fa-circle-o"></i> Ganti Password </a></li>            
          </ul>
        </li> 
		<li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Nilaiku</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('siswa/nilai_saya'); ?>"><i class="fa fa-circle-o"></i> Cek Nilai </a></li>
          </ul>
        </li> 
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Prestasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('siswa/prestasiku'); ?>"><i class="fa fa-circle-o"></i> Prestasiku </a></li>            
            <li><a href="<?php echo base_url('siswa/ekskulku'); ?>"><i class="fa fa-circle-o"></i> Ekskulku </a></li>            
          </ul>
        </li> 
        
        <li class="header">Diskusi</li>
        <li><a href="<?php echo base_url('chat/siswa'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Chat</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
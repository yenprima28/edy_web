<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <?php
            $gambar = $this->Pengajar_model->get_gambar();
            ?>
            
            <img src="<?php echo base_url('assets/foto/pengajar/' . $gambar[0]->gambar); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
         <p>NIP : <?php echo $this->session->username; ?></p>
          
          <?php
          if( $this->session->level == 'guru' )
          {
              $level = 'Guru';
          } 
          else if( $this->session->level == 'walikelas' )
          {
              $level = 'Wali Kelas';
          }
          ?>
          <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $level; ?> </a>
        </div>
      </div>      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kelola Data Siswa</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>wali_kelas/kelola_nilai_siswa"><i class="fa fa-circle-o"></i> Daftar Nilai Siswa </a></li>
            <?php if( $this->session->smt_aktif % 2 == 0 ){ ?>
            <li><a href="<?php echo base_url(); ?>wali_kelas/kelola_naik_kelas"><i class="fa fa-circle-o"></i> Kenaikan Kelas </a></li>            
            <?php } ?>
            <!--<li><a href="<?php echo base_url(); ?>wali_kelas/kelola_jurusan_siswa"><i class="fa fa-circle-o"></i> Kelola Jurusan Siswa </a></li>-->
            <li><a href="<?php echo base_url(); ?>wali_kelas/kelola_absensi_siswa"><i class="fa fa-circle-o"></i> Kelola Absensi Siswa </a></li>
            <li><a href="<?php echo base_url(); ?>wali_kelas/kelola_ekskul_siswa"><i class="fa fa-circle-o"></i> Kelola Ekstrakulikuler Siswa </a></li>           
            <li><a href="<?php echo base_url(); ?>wali_kelas/kelola_prestasi_siswa"><i class="fa fa-circle-o"></i> Kelola Prestasi Siswa </a></li>                       
          </ul>
        </li>        
        <li class="header">Diskusi</li>
        <li><a href="<?php echo base_url(); ?>chat/wali_kelas"><i class="fa fa-circle-o text-red"></i> <span>Chat</span></a></li>
        
        <?php
		//$data['level_guru'] = $this->Pengajar_model->get_level();
		$data['level_guru'] = $this->Pengajar_model->get_level();
		
		$num_rows = count($data['level_guru']);
		
		if($num_rows == 2)
		{ 
			echo '<li><a href=' . base_url("wali_kelas/guru_mode") . '><i class="fa fa-circle-o text-blue"></i> <span>Berganti ke Guru</span></a></li>';
		}
		?>
        
      </ul>
    </section>
    <!-- /.sidebar -->
</aside> 

<?php // var_dump( $this->session ); ?>
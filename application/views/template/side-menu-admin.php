<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/dist/img/yaranaika.png'); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Yaranaika</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Administrator </a>
        </div>
      </div>      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kelola Data Pengajar</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/daftar_pengajar'); ?>"><i class="fa fa-circle-o"></i> Daftar Pengajar </a></li>
            <li><a href="<?php echo base_url('admin/tambah_data_pengajar'); ?>"><i class="fa fa-circle-o"></i> Tambah Data Pengajar </a></li>
            <li><a href="<?php echo base_url('admin/tambah_pengajar_kelas'); ?>"><i class="fa fa-circle-o"></i> Tambah Pengajar Kelas</a></li>
            <li><a href="<?php echo base_url('admin/kelola_jabatan_pengajar'); ?>"><i class="fa fa-circle-o"></i> Kelola Jabatan Pengajar </a></li>
            <li><a href="<?php echo base_url('admin/kelola_kelas_pengajar'); ?>"><i class="fa fa-circle-o"></i> Kelola Kelas Pengajar </a></li>
            <li><a href="<?php echo base_url('admin/daftar_jabatan_pengajar'); ?>"><i class="fa fa-circle-o"></i> Daftar Jabatan Pengajar </a></li>            
            <li><a href="<?php echo base_url('admin/daftar_wali_kelas'); ?>"><i class="fa fa-circle-o"></i> Daftar Wali Kelas </a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Kelola Data Siswa</span>
            <span class="pull-right-container">      
				<i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/daftar_siswa'); ?>"><i class="fa fa-circle-o"></i> Daftar Siswa </a></li>
            <li><a href="<?php echo base_url('admin/tambah_data_siswa'); ?>"><i class="fa fa-circle-o"></i> Tambah Data Siswa </a></li>            
          </ul>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-th"></i> 
			<span>Kelola Mata Pelajaran</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
		  <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/daftar_mapel'); ?>"><i class="fa fa-circle-o"></i> Daftar Mata Pelajaran </a></li>
            <li><a href="<?php echo base_url('admin/tambah_data_mapel'); ?>"><i class="fa fa-circle-o"></i> Tambah Mata Pelajaran </a></li>
            <!--
			<li><a href="<?php echo base_url('admin/kelola_pengajar_mapel'); ?>"><i class="fa fa-circle-o"></i> Kelola Pengajar Mata Pelajaran </a></li>  
			<li><a href="<?php echo base_url('admin/daftar_pengajar_mapel'); ?>"><i class="fa fa-circle-o"></i> Daftar Pengajar Mapel </a></li>				
			-->
		  </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Kelola Kelas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/daftar_kelas'); ?>"><i class="fa fa-circle-o"></i> Daftar Kelas </a></li>
            <li><a href="<?php echo base_url('admin/tambah_data_kelas'); ?>"><i class="fa fa-circle-o"></i> Tambah Kelas </a></li>
            <li><a href="<?php echo base_url('admin/tambah_siswa_perkelas'); ?>"><i class="fa fa-circle-o"></i> Tambah Siswa (Kelas) </a></li>
            <!--<li><a href="<?php echo base_url('admin/kelola_kelas_mapel'); ?>"><i class="fa fa-circle-o"></i> Kelola Pelajan Perkelas </a></li>-->
            <!-- <li><a href="<?php echo base_url(); ?>admin/kelola_kelas"><i class="fa fa-circle-o"></i> Kelola Kelas </a></li> -->
          </ul>
        </li>        
		<li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Kelola Ekstrakulikuler</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/daftar_ekskul'); ?>"><i class="fa fa-circle-o"></i> Daftar Ekstrakulikuler </a></li>
            <li><a href="<?php echo base_url('admin/tambah_data_ekskul'); ?>"><i class="fa fa-circle-o"></i> Tambah Ekstrakulikuler </a></li>
            <!-- <li><a href="<?php echo base_url(); ?>admin/kelola_ekskul"><i class="fa fa-circle-o"></i> Kelola Ekstrakulikuler </a></li> -->
          </ul>
        </li>  
		<li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Kelola Tahun Pelajaran</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/daftar_tahun_pelajaran'); ?>"><i class="fa fa-circle-o"></i> Daftar Tahun Pelajaran </a></li>
            <li><a href="<?php echo base_url('admin/tambah_data_tahun_pelajaran'); ?>"><i class="fa fa-circle-o"></i> Tambah Tahun Pelajaran </a></li>
            <!-- <li><a href="<?php echo base_url(); ?>admin/kelola_ekskul"><i class="fa fa-circle-o"></i> Kelola Ekstrakulikuler </a></li> -->
          </ul>
        </li>  
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Kelola Akun</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('admin/reset_password_siswa_pengajar'); ?>"><i class="fa fa-circle-o"></i> Reset Password </a></li>            
            <li><a href="<?php echo base_url('admin/reset_admin_pass'); ?>"><i class="fa fa-circle-o"></i> Ganti Password </a></li>
          </ul>
        </li>  
        <!--
		<li class="header">Diskusi</li>
        <li><a href="<?php echo base_url('chat/index'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Chat</span></a></li>
		-->
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>
		 </div>
		<!-- /.content-wrapper -->
		
		<footer class="main-footer">
			<div class="pull-right hidden-xs">			  
			</div>			
		</footer>
		
		<!-- jQuery 2.2.3 -->
		<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-3.1.0.min.js"></script>
        <!-- jQuery 2.2.3 -->
		<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
		<!-- SlimScroll -->
		<script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- FastClick -->
		<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
		<!--<script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>-->
		<!-- bootstrap datepicker -->
		<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
		<!-- AdminLTE App -->
		<script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

		<script>
		//Date picker
		$('#datepicker').datepicker({	  
			autoclose: true
		});		
		
		$('#send_chat').click(function(){
			//var data = $('.form').serialize();
			var pesan = $('#chat_text').val();
			
			if(pesan != ''){											
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url();?>chat/kirim_pesan',
					data: 'pesans='+pesan}).done(function(){
						
						$('#chat_text').val('');
						$('#target').load('<?php echo base_url();?>chat/tampil_pesan');
						
					});
			} else {
				alert('Pesan Kosong');
			}
			
			
		});
		
		setInterval(function(){
			$('#target').load('<?php echo base_url();?>chat/tampil_pesan');
		}, 2000);		
		
		if(typeof runJquery === 'function')
		{
			runJquery();
		} 
		else 
		{
			console.log('Coeg');
		}
		</script>
	</body>
</html>

<!DOCTYPE html>
<html>
	<head>
	  <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <title>
		<?php echo UCfirst($this->session->level); ?> 
	  </title>
	  <!-- Tell the browser to be responsive to screen width -->
	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- jQuery DataTable -->
	  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.css'); ?>">
	  <!-- Bootstrap 3.3.6 -->
	  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
      <!-- jQuery DataTable -->
	  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css'); ?>">
	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/css/font-awesome.min.css'); ?>">
	  <!-- Ionicons -->
	  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/css/ionicons.min.css'); ?>">
	  <!-- daterange picker -->
	  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css"> -->
	  <!-- bootstrap datepicker -->
	  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css'); ?>">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>">
	  <!-- AdminLTE Skins. Choose a skin from the css/skins
		   folder instead of downloading all of them to reduce the load. -->
	  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>"> 

	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<!-- Site wrapper -->
		<div class="wrapper">
			<header class="main-header">
			<!-- Logo -->
			<a href="../../index2.html" class="logo">
			  <!-- mini logo for sidebar mini 50x50 pixels -->
			  <span class="logo-mini"><b>S</b>MA</span>
			  <!-- logo for regular state and mobile devices -->
			  <span class="logo-lg"><b>SMA</b>PULPIS</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
			  <!-- Sidebar toggle button-->
			  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </a>

			  <div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
				  
				  <!-- User Account: style can be found in dropdown.less -->
				  <li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  
                        <img src="<?php echo base_url('assets/dist/img/yaranaika.png'); ?>" class="user-image" alt="User Image">
					  <!-- <span class="hidden-xs">Yaranaika</span> -->
					</a>
					<ul class="dropdown-menu">
					  <!-- User image -->
					  <li class="user-header">
						<img src="<?php echo base_url('assets/dist/img/yaranaika.png'); ?>" class="img-circle" alt="User Image">

						<p> <?php echo $this->session->username . '(Nama disini)'; ?> </p>
                        <p> Siswa
                          <!-- <small>Member since Nov. 2012</small> -->
						</p>
					  </li>              
					  <!-- Menu Footer-->
					  <li class="user-footer">
						
						<div class="pull-left">
						  <a href="#" class="btn btn-default btn-flat">Profil</a>
						</div>
						
						<div class="pull-right">
						  <a href="<?php echo base_url(); ?>login/logout" class="btn btn-default btn-flat">Sign out</a>
						</div>
					  </li>
					</ul>
				  </li>          
				</ul>
			  </div>
			</nav>
		  </header>
		  
		  <!-- Content Wrapper. Contains page content -->
		  <div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
			  <h1>
				<?php echo UCfirst($this->session->level); ?> 
				<small>Anda Coeg Sekalehh...:v</small>
			  </h1>      
			</section>
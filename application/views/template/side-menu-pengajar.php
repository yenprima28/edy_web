<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <?php
            if( $this->session->level == 'guru' || $this->session->level == 'walikelas' )
            {
                $tipe = 'pengajar/';
                $gambar = $this->Pengajar_model->get_gambar();  
            } 
            else 
            {
                $this->load->model('Siswa_model');
                
                $tipe = 'siswa/';
                
                $gambar = $this->Siswa_model->get_gambar();
            }
            ?>
            
            <img src="<?php echo base_url('assets/foto/' . $tipe . $gambar[0]->gambar); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>NIP : <?php echo $this->session->username; ?></p>
          
          <?php
          if( $this->session->level == 'guru' )
          {
              $level = 'Guru';
          } 
          else if( $this->session->level == 'wali kelas' )
          {
              $level = 'Wali Kelas';
          }
          ?>
          <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $level; ?> </a>
        </div>
      </div>            
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kelola Nilai Siswa</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('guru/daftar_nilai_siswa'); ?>"><i class="fa fa-circle-o"></i> Input Nilai Siswa </a></li>
            <!-- <li><a href="<?php echo base_url(); ?>guru/input_nilai_siswa"><i class="fa fa-circle-o"></i> Input Nilai Siswa </a></li> -->
          </ul>
        </li>        
        <li class="header">Diskusi</li>
        <li><a href="<?php echo base_url('chat/guru'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Chat</span></a></li>
		<li><a href="<?php echo base_url('guru/ganti_password'); ?>"><i class="fa fa-circle-o"></i> Ganti Password </a></li>
		<?php
		//$data['level_guru'] = $this->Pengajar_model->get_level();
		$data['level_guru'] = $this->Pengajar_model->get_level();
		
		$num_rows = count($data['level_guru']);
		
		if($num_rows == 2)
		{ 
			echo '<li><a href=' . base_url("guru/walikelas_mode") . '><i class="fa fa-circle-o text-blue"></i> <span>Berganti ke Wali Kelas</span></a></li>';
		}
		?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
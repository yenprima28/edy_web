    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"> Prestasiku </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered" id='list_prestasi'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Prestasi</th>
                        <th>Detail Prestasi</th>
                    </tr>
                <thead>            
                <tbody>
				<?php
                if( $prestasi != null )
                {
                    $no = 1;                
                    foreach($prestasi as $ls): 
                ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $ls->prestasi ?></td>
                            <td><?= $ls->detail_prestasi ?></td>
                        </tr>
                <?php 
                    endforeach; 
                }                 
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->		
    </section>
    <!-- /.content -->
    
    <script>
    
    function runJquery()
    {        
        $('#list_prestasi').DataTable();
    }    
    
    </script>
    
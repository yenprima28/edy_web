<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

<script>
	$(document).ready(function(){
		
		//Date picker
		$('#datepicker').datepicker({	  
		  autoclose: true
		});
		
		$('#send_chat').click(function(){
			//var data = $('.form').serialize();
			var pesan = $('#chat_text').val();
			
			if(pesan != ''){											
				$.ajax({
					type: 'POST',
					url: '<?php echo base_url();?>chat/kirim_pesan',
					data: 'pesans='+pesan}).done(function(){
						
						$('#chat_text').val('');
						$('#target').load('<?php echo base_url();?>chat/tampil_pesan');
						
					});
			} else {
				alert('Pesan Kosong');
			}
			
			
		});
		
		setInterval(function(){
			$('#target').load('<?php echo base_url();?>chat/tampil_pesan');
		}, 2000);
		
	});
</script>
</body>
</html>


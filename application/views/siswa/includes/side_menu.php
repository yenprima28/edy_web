<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/yaranaika.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Yaranaika</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Administrator </a>
        </div>
      </div>      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kelola Profil</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>admin/daftar_guru"><i class="fa fa-circle-o"></i> Profil Saya </a></li>            
          </ul>
        </li> 
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Nilaiku</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>siswa/nilai_saya/asd"><i class="fa fa-circle-o"></i> Cek Nilai </a></li>            
          </ul>
        </li> 
        <li class="header">Diskusi</li>
        <li><a href="<?php echo base_url(); ?>chat/siswa"><i class="fa fa-circle-o text-red"></i> <span>Chat</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
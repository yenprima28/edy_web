    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"> Nilai Saya </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th style="width: 600px">Mata Pelajaran</th>
                  <th style="width: 100px">KKM</th>
                  <th >Pengetahuan<br/>
					Nilai | Predikat
				  </th>               
                  <th>Keterampilan<br/>
					Nilai | Predikat
				  </th>
                </tr>
				<?php
				$no = 1;
				//foreach($guru as $ls): 
				?>
                
				<tr>											
                  <td> 1 </td>
                  <td> Matematika </td>                 
                  <td> 75 </td>                 
                  <td> 78 | C </td>				  
                  <td> 80 | B </td>                  
                </tr>                
				
				<tr>											
                  <td> 2 </td>
                  <td> Fisika </td>                 
                  <td> 75 </td>                 
                  <td> 78 | C </td>				  
                  <td> 78 | C </td>				  
                </tr>              
				
				<?php
				//endforeach;
				?>
              </table>
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
		<table class="table table-bordered" style="width: 300px">
			<thead>
				<th> Nilai </th>
				<th> Predikat </th>
			</thead>
			<tbody>
				<tr>
					<td> Nilai < 75 </td>
					<td> D </td>
				</tr>
				<tr>
					<td> 75 <= Nilai <= 79 </td>
					<td> C </td>
				</tr>
				<tr>
					<td> 80 <= Nilai <= 89 </td>
					<td> B </td>
				</tr>
				<tr>
					<td> 90 <= Nilai <= 100 </td>
					<td> A </td>
				</tr>
			</tbody>			
		</table>
    </section>
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"> Chat </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          
		  <div class='chat'>
			<div id="target" style="overflow: scroll; width:1070px; height: 300px;">
				<p>[waktu][nis/nik][pesan]</p>				
			</div>
 		  </div>
			
		  <br/>		  			
			<input class='form-control' type='text' id='chat_text' maxlength='100'>	<br/>		
			<button class='btn btn-primary' id='send_chat'>Kirim</button>		  		  
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
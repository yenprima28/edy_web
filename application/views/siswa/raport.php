    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3><br/><br/>
              
              <!--
              <?php echo 'NIS : ',$nilai[0]['nis']; ?> || <?php echo 'Nama : ',$nilai[0]['nama']; ?><br/>
              <?php echo 'Semester : ',$nilai[0]['semester']; ?> || <?php echo 'Tahun Pelajaran : ',$nilai[0]['tahun']; ?>
              -->
              
              <table>
                <tr>
                    <td><?php echo 'NIS : ',$nilai[0]['nis']; ?></td><td>&nbsp;</td><td><?php echo 'Nama : ',$nilai[0]['nama']; ?></td>
                </tr>
                <tr>
                    <td><?php echo 'Semester : ',$nilai[0]['semester']; ?></td><td>&nbsp;&nbsp;&nbsp;</td><td><?php echo 'Tahun Pelajaran : ',$nilai[0]['tahun']; ?></td>
                </tr>
              </table>
              
            </div>            			
				<div class="box-body">
					<table class="table table-bordered">
						<tr>
						  <th style="width: 10px">#</th>
						  <th style="width: 600px">Mata Pelajaran</th>
						  <th style="width: 100px">KKM</th>
						  <th >Pengetahuan<br/>
							Nilai | Predikat
						  </th>               
						  <th>Keterampilan<br/>
							Nilai | Predikat
						  </th>
						</tr>
					<?php foreach($nilai as $ls): ?>
					
						<tr>											
						  <td> <?php echo $no++;?> </td>
						  <td> <?php echo $ls['mapel']; ?> </td>                 
						  <td> <?php echo $ls['kkm']; ?> </td>                 
						  <td> <?php echo $ls['pengetahuan_angka']; ?> | <?php echo $ls['pengetahuan_grade']; ?> </td>				  
						  <td> <?php echo $ls['keterampilan_angka']; ?> | <?php echo $ls['keterampilan_grade']; ?> </td>				  						            
						</tr>                
										
					<?php endforeach; ?>
				  </table>            
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
               
              </div>			
          </div>
          <!-- /.box -->		  
      </div>
      <!-- /.box -->
	
		<table class="table table-bordered" style="width: 300px">
			<legend>Keterangan Nilai</legend>
			<thead>
				<th> Nilai </th>
				<th> Predikat </th>
			</thead>
			<tbody>
				<tr>
					<td> Nilai < 75 </td>
					<td> D </td>
				</tr>
				<tr>
					<td> 75 <= Nilai <= 79 </td>
					<td> C </td>
				</tr>
				<tr>
					<td> 80 <= Nilai <= 89 </td>
					<td> B </td>
				</tr>
				<tr>
					<td> 90 <= Nilai <= 100 </td>
					<td> A </td>
				</tr>
			</tbody>			
		</table>
	
    </section>
    <!-- /.content -->
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title, ' : ', $nama_kelas; ?> </h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
						  
			  <!--
			  <select id='kelas'>
				<option value=''> --Pilih Mapel-- </option>
				<option value=''> X </option>
			  </select>
			  -->
                
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama</th>
                            <th>Kelas</th>                                   
                            <th colspan='3'>Absensi <br/>
                                Ijin | Sakit | Alpa
                            </th>                 
                            <th colspan='2' width = '10px'> Aksi </th>
                        </tr>
                    </thead>				
                    <tbody id='daftar_siswa'></tbody>
                </table>
            </div>
		
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->
    
        <!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>				
			  </div>
			  <div class="modal-body">
				
				<form method='post'>
                    <div class="form-group">
						<label for="nis"> NIS </label>
						<input type="text" class="form-control" name='nis' id='nis_modal_id' placeholder="Masukkan NIS..." disabled/>							
					</div>
                    <div class="form-group">
						<label for="nama"> Nama Lengkap </label>                  
						<input type="text" class="form-control" name='nama' id='nama_lengkap_id' placeholder="Masukkan Nama Lengkap..." disabled/>				  				  
					</div>	
					<div class="form-group">
						<label for="tahun"> Tahun </label>
						<input type="text" class="form-control" name='tahun' id="tahun_id" disabled>
					</div>
					<div class="form-group">
						<label for="semester"> Semester </label>
						<input type="text" class="form-control" name='semester' id="smt_id" disabled>
					</div>
					<div class="form-group">									
						<label for="ijin"> Ijin </label>
						<input type="text" class="form-control" name='ijin' id="ijin_id" placeholder="...">
					</div>									
					<div class="form-group">									
						<label for="sakit"> Sakit </label>
						<input type="text" class="form-control" name='sakit' id="sakit_id" placeholder="...">
					</div>									
					<div class="form-group">									
						<label for="alpa"> Alpa </label>
						<input type="text" class="form-control" name='alpa' id="alpa_id" placeholder="...">
					</div>									
			  </div>
			  <div class="modal-footer">
				<button type="submit" name='submit' class="btn btn-primary" id='btn_simpan'> Simpan Data </button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
			
			</form>
			
		  </div>
		</div>
    
    </section>
    <!-- /.content -->
	    
    <script src='<?= base_url("assets/web/js/wali_kelas/kelola_absensi_siswa.js"); ?>'></script>
    
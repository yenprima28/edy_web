    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?> : - </h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">				
			
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Nama</th>
                            <th>Ekskul yang diminati</th>                            
                            <th colspan='2' width = '10px'> Aksi </th>
                        </tr>
                    </thead>				
                    <tbody id='daftar_siswa'></tbody>
                </table>
							
              </table>
            </div>
			            
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->
    
       <!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>				
			  </div>
			  <div class="modal-body">
				
				<form method='post'>
                    <div class="form-group">
						<label for="nis"> NIS </label>
						<input type="text" class="form-control" name='nis' id='nis_modal_id' placeholder="Masukkan NIS..." disabled/>							
					</div>
                    <div class="form-group">
						<label for="nama"> Nama Lengkap </label>                  
						<input type="text" class="form-control" name='nama' id='nama_lengkap_id' placeholder="Masukkan Nama Lengkap..." disabled/>				  				  
					</div>						
					<!--Checkbox
                    <div class="form-group" id='chkbox'>
						<label for="ekskul"> Daftar Ekskul </label> <br/>						
					</div>		
                    -->                    
                    <div class="form-group">
						<label for="ekskul1"> Daftar Ekskul 1 </label> <br/>						
						<select name='ekskul1' id='ekskul1_id'></select>
					</div>						
                    <div class="form-group">
						<label for="ekskul2"> Daftar Ekskul 2 </label> <br/>						
						<select name='ekskul2' id='ekskul2_id'></select>
					</div>						
			  </div>
			  <div class="modal-footer">
				<button type="submit" name='submit' class="btn btn-primary" id='btn_simpan'> Simpan Data </button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
    
    </section>
    <!-- /.content -->    
    <script src='<?= base_url("assets/web/js/wali_kelas/kelola_ekskul_siswa.js"); ?>'></script>
    
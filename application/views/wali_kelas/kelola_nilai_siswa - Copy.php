    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title, ' : ', $nama_kelas; ?>  </h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
			
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>NIS</th>
                  <th>Nama Siswa</th>
                  <th>Semester</th>                                   
                  <!-- <th colspan='3' width = '10px'> Aksi </th> -->
                </tr>
                <!-- <?php if( $data_siswa != false ){ ?> -->
                    <?php foreach($data_siswa as $ls): ?>
                    
                    <tr>											
                      <td><?= $no++; ?></td>
                      <td><?= $ls->nis ?></td>				  
                      <td><?= $ls->nama; ?></td>                      
                      <td><?= $smt_aktif; ?></td>
                      <!--
                      <td style="width: 5px">	
                        <a href='<?php echo base_url() . 'wali_kelas/nilai/' . $ls->nis; ?>' class="btn btn-info"> *.Xlsx </a>
                      </td>    
                      <td width = '10px'>
                        <button type="button" class="btn btn-danger"> *.PDF </button>
                      </td>
                      -->
                    </tr>                
                    
                    <?php endforeach; ?>
                <!--
				<?php } else { ?>
                    <p> Data tidak ditemukan </p>
                <?php } ?>
                -->
              </table>
            </div>
			
			
			
            <!-- /.box-body -->
            <!--
			<div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>          
			-->
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->

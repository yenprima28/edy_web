    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title, ' : ', $nama_kelas; ?>  </h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
			
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>NIS</th>
                  <th>Nama Siswa</th>
                  <th>Kelas</th>                                   
                  <th>Semester</th>                                   
                  <th colspan='3' width = '10px'> Aksi </th>
                </tr>
				<?php foreach($data_siswa as $ls): ?>
                
				<tr>											
                  <td><?= $no++; ?></td>
				  <td><?= $ls->nis ?></td>				  
                  <td><?= $ls->nama; ?></td>     
                  <td><?= $ls->kelas; ?></td>     
				  <td>2</td>
				  <td style="width: 5px">	
					<a href='<?php echo base_url() . 'wali_kelas/naik_kelas/' . $ls->nis; ?>' class="btn btn-info"> Naik Kelas </a>
  				  </td>    
				  <td width = '10px'>
					<button type="button" class="btn btn-danger"> Tinggal Kelas </button>
				  </td>
                </tr>                
				
				<?php endforeach; ?>
				
              </table>
            </div>
			
			
			
            <!-- /.box-body -->
            <!--
			<div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>          
			-->
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->
		
		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			  </div>
			  <div class="modal-body">				
				<div class="form-group">
					<?php echo form_open('guru/insert_nilai_siswa_proses', 'id=formInputNilai'); ?> 
					<div class="box-body">
						<div class="form-group">
							<label for="nis"> NIS </label>
							<input type="text" class="form-control" name='nis' id='nis_modal' placeholder="Masukkan NIS..." disabled/>							
						</div>
						<div class="form-group">
							<label for="nama"> Nama Lengkap </label>                  
							<input type="text" class="form-control" name='nama' id='nama_lengkap' placeholder="Masukkan Nama Lengkap..." disabled/>				  				  
						</div>				
						<div class="form-group">
							<label for="n_pengetahuan"> Nilai Pengetahuan </label>                  
							<input type="text" class="form-control" name='n_pengetahuan' id='n_pengetahuan' placeholder="Masukkan Nilai Pengetahuan..." />
						</div>
							<div class="form-group">
							<label for="n_pengetahuan_pre"> Predikat Nilai Pengetahuan </label>                  
							<input type="text" class="form-control" name='n_pengetahuan_pre' id='n_pengetahuan_pre' placeholder="Masukkan Nilai Pengetahuan..." disabled />
						</div>
						<div class="form-group">
							<label for="n_keterampilan"> Nilai Keterampilan </label>                  
							<input type="text" class="form-control" name='n_keterampilan' id='n_keterampilan' placeholder="Masukkan Nilai Keterampilan..." />
						</div>
						<div class="form-group">
							<label for="n_keterampilan_pre"> Predikat Nilai Keterampilan </label>                  
							<input type="text" class="form-control" name='n_keterampilan_pre' id='n_keterampilan_pre' placeholder="Masukkan Nilai Keterampilan..." disabled />
						</div>
					</div>					
					<?php echo form_close(); ?>
				</div>
			  <div class="modal-footer">				
				<button type="button" class="btn btn-primary" id='simpan_nilai' data-dismiss="modal">Simpan Nilai</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		<!--Modals Nilai-->
		
    </section>
    <!-- /.content -->

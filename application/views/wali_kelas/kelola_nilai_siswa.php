    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title, ' : ', $nama_kelas; ?>  </h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
			
              <table class="table table-bordered">
                <thead>
                    <tr>
                      <th style="width: 10px">#</th>                  
                      <th>Mapel</th>
                      <th>Semester</th>                                   
                      <th> Aksi </th>
                      <th> Status </th>
                    </tr>              
                </thead>
                <tbody id='daftar_mapel'>
				</tbody>                
                    <!-- 
                    <?php foreach($mapel as $ls): ?>
                    
                    <tr>											
                      <td><?= $no++; ?></td>                      
                      <td><?= $ls->mapel; ?></td>                      
                      <td><?= $smt_aktif; ?></td>                    
                      <td style="width: 5px">	
                        <a href='<?php echo base_url() . 'wali_kelas/nilai/' . $ls->kd_mapel; ?>' class="btn btn-info"> Download *.Xlsx </a>
                      </td>
                      <td class='statusNilai'> - </td>                    
                    </tr>                
                    
                    <?php endforeach; ?> 
                    -->
                
              </table>
            </div>
			         
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
    <script>
       //console.log('asd');
    </script>
    
    <script src='<?= base_url("assets/web/js/wali_kelas/kelola_nilai_siswa.js"); ?>'></script>
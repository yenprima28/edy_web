    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title, ' : ', $nama_kelas; ?>  </h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
			
              <table class="table table-bordered">
				<thead>
					<tr>
					  <th style="width: 10px">#</th>
					  <th>NIS</th>
					  <th>Nama Siswa</th>
					  <th>Kelas</th>
					  <th> Aksi </th>
					</tr>	
				</thead>
				<tbody id='daftar_siswa'>
				</tbody>
              </table>
            </div>
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->
		
		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Kenaikan Kelas Siswa</h4>
			  </div>
			  <div class="modal-body">				
				<div class="form-group">
					<?php echo form_open('guru/insert_nilai_siswa_proses', 'id=formInputNilai'); ?> 
					<div class="box-body">
						<div class="form-group">
							<label for="nis"> NIS </label>
							<input type="text" class="form-control" name='nis' id='nis_modal_id' placeholder="Masukkan NIS..." disabled/>							
						</div>
						<div class="form-group">
							<label for="nama"> Nama Lengkap </label>                  
							<input type="text" class="form-control" name='nama' id='nama_lengkap_id' placeholder="Masukkan Nama Lengkap..." disabled/>				  				  
						</div>				
						<div class="form-group">
							<label for="kelas_skr"> Kelas Sekarang </label>                  
							<input type="text" class="form-control" name='kelas_skr' id='kelas_skr_id' disabled/>
						</div>						
						<div class="form-group">
							<label for="kelas_baru"> Naikkan ke Kelas </label>                  
							<select class="form-control select2" name='kelas_baru' id='kelas_baru_id'>
								<option selected="selected"> --Pilih Kelas-- </option>							  
							</select>
						</div>
					</div>					
					<?php echo form_close(); ?>
				</div>
			  <div class="modal-footer">				
				<button type="button" class="btn btn-primary" id='simpan_nilai_id' data-dismiss="modal">Simpan Kelas</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		<!--/.Modals-->
		
    </section>
    <!-- /.content -->
	
    <script src='<?= base_url("assets/web/js/wali_kelas/kenaikan_kelas.js"); ?>'></script>
	
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/yaranaika.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Yaranaika</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Wali Kelas : X A </a>
        </div>
      </div>      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kelola Data Siswa</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>wali_kelas/kelola_naik_kelas"><i class="fa fa-circle-o"></i> Kenaikan Kelas </a></li>
            <!--<li><a href="<?php echo base_url(); ?>wali_kelas/kelola_jurusan_siswa"><i class="fa fa-circle-o"></i> Kelola Jurusan Siswa </a></li>-->
            <li><a href="<?php echo base_url(); ?>wali_kelas/kelola_absensi_siswa"><i class="fa fa-circle-o"></i> Kelola Absensi Siswa </a></li>
            <li><a href="<?php echo base_url(); ?>wali_kelas/kelola_ekskul_siswa"><i class="fa fa-circle-o"></i> Kelola Ekstrakulikuler Siswa </a></li>           
            <li><a href="<?php echo base_url(); ?>wali_kelas/kelola_prestasi_siswa"><i class="fa fa-circle-o"></i> Kelola Prestasi Siswa </a></li>           
          </ul>
        </li>        
        <li class="header">Diskusi</li>
        <li><a href="<?php echo base_url(); ?>chat/wali_kelas"><i class="fa fa-circle-o text-red"></i> <span>Chat</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside> 
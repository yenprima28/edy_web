    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
			
			<?php echo validation_errors(); ?>						

			<?php echo form_open('guru/ganti_password'); ?> 
			
			<div class="box-body">
				<div class="form-group">                    
                    <label for="nis_nip"> NIS </label>
					<input type="text" class="form-control" name='nis' id="nis_id" value='<?= $this->session->username; ?>' placeholder="Masukkan Username Admin..." disabled />											
					<input type="hidden" class="form-control" name='nis_nip' id="nis_nip_id" value='<?= $this->session->username; ?>'>
				</div>                
                <div class="form-group">
					<label for="old_pass"> Password Sekarang </label>
					<input type="password" class="form-control" name='old_pass' id="old_pass_id" placeholder="Masukkan Password Sekarang...">											
				</div>                
                <div class="form-group">
					<label for="new_pass"> Password Baru </label>
					<input type="password" class="form-control" name='new_pass' id="new_pass_id" placeholder="Masukkan Password Baru...">											
				</div>				
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
				<input type="submit" name='submit' class="btn btn-primary" value='Simpan Data'>
            </div>
            <!-- </form> -->			
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
		
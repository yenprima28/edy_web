<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/yaranaika.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Yaranaika</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Guru | Matematika </a>
        </div>
      </div>      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Kelola Nilai Siswa</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>guru/daftar_siswa_perkelas"><i class="fa fa-circle-o"></i> Input Nilai Siswa </a></li>
            <!-- <li><a href="<?php echo base_url(); ?>guru/input_nilai_siswa"><i class="fa fa-circle-o"></i> Input Nilai Siswa </a></li> -->
          </ul>
        </li>        		
        <li class="header">Diskusi</li>
        <li><a href="<?php echo base_url(); ?>chat/guru"><i class="fa fa-circle-o text-red"></i> <span>Chat</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
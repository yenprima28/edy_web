    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?> : Mata Pelajaran (<?php echo $this->session->mapel; ?>) </h3>			  
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
			
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>NIS</th>
                  <th>Nama</th>                  
                  <th>Nilai Pengetahuan <br/>
					Angka | Grade
				  </th>                                   
                  <th>Nilai Keterampilan <br/>
					Angka | Grade
				  </th>                                            
                  <th colspan='2' width = '10px'> Aksi </th>
                </tr>
				
				<tr>											
				  
				  <!--Siswa Berdasarkan NIS-->
				  <?php foreach($siswa as $ls): ?>				  
				  
					<td><?= $no++; ?></td>
					<td><?= $ls->nis; ?></td>  				  
					<td><?= $ls->nama; ?></td>  				  
                    				
					<td><?= $ls->pengetahuan_angka; ?> | <?= $ls->pengetahuan_grade; ?></td>
					<td><?= $ls->keterampilan_angka; ?> | <?= $ls->keterampilan_grade; ?></td>                  
					<td style="width: 5px">							
						<!--<a href='<?php echo base_url() . 'guru/input_nilai_siswa/' . $ls->nis; ?>' class="btn btn-info"> Atur Nilai </a>-->
						
						<!-- Trigger the modal with a button -->
						<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
					</td>    
					<td width = '10px'>
						<button type="button" class="btn btn-danger"> Hapus Nilai </button>
					</td>
					
                </tr>                								
				
					<?php endforeach; ?>
				
              </table>
            </div>
			
			
			
            <!-- /.box-body -->
            <!--
			<div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>          
			-->
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
	
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
		  </div>
		  <div class="modal-body">
			<p>Some text in the modal.</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>
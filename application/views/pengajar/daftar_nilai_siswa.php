    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Nilai Siswa </h3>			
            </div>
            <!-- /.box-header -->
            			
			<div class="box-body">
										
				<label> Mata Pelajaran </label>
				<select class="form-control select2" name='mapel' id='mapel'>
				  <option selected="selected"> --Pilih Mapel-- </option>
				  <?php foreach($mapel as $ls): ?>
				  
				  <option value='<?= $ls->kd_mapel; ?>'> <?= $ls->mapel; ?> </option>
				  
				  <?php endforeach; ?>
				</select>			
				
			<hr>			
				<div class="form-group">
					<select class="form-control select2" name='kelas' id='kelas'>
					  <option selected="selected"> --Pilih Kelas-- </option>					
					</select>						
				</div>	
				
				<button type="submit" id='tampil_siswa' name='submit' value='tampil_siswa' class="btn btn-primary"> Tampilkan Siswa </button>
			</form>         						
			  
			<h2 class='judul_tabel'>Daftar Siswa Kelas </h2><p class='status'></p>
			<table class='table table-bordered' id='list_siswa'>
				<thead>
					<tr>
						<th>No</th>
						<th>NIS</th>
						<th>Nama</th>
						<th>Kelas</th>
						<th>Nilai Pengetahuan<br/>Grade|Angka</th>
						<th>Nilai Keterampilan<br/>Grade|Angka</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody id='daftar_siswa'>
				</tbody>
			</table>
          </div>
          <!-- /.box -->
		  
      </div>
      <!-- /.box -->
		
		<!--Modals Nilai-->
		<!-- Trigger the modal with a button -->
		<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			  </div>
			  <div class="modal-body">				
				<div class="form-group">
					<?php echo form_open('guru/insert_nilai_siswa_proses', 'id=formInputNilai'); ?> 
					<div class="box-body">
						<div class="form-group">
							<label for="nis"> NIS </label>
							<input type="text" class="form-control" name='nis' id='nis_modal' placeholder="Masukkan NIS..." disabled/>							
						</div>
						<div class="form-group">
							<label for="nama"> Nama Lengkap </label>                  
							<input type="text" class="form-control" name='nama' id='nama_lengkap_modal' placeholder="Masukkan Nama Lengkap..." disabled/>				  				  
						</div>				
						<div class="form-group">
							<label for="n_pengetahuan"> Nilai Pengetahuan </label>                  
							<input type="text" class="form-control" name='n_pengetahuan' id='n_pengetahuan_modal' placeholder="Masukkan Nilai Pengetahuan..." />
						</div>
							<div class="form-group">
							<label for="n_pengetahuan_pre"> Predikat Nilai Pengetahuan </label>                  
							<input type="text" class="form-control" name='n_pengetahuan_pre' id='n_pengetahuan_pre_modal' placeholder="Masukkan Nilai Pengetahuan..." disabled />
						</div>
						<div class="form-group">
							<label for="n_keterampilan"> Nilai Keterampilan </label>                  
							<input type="text" class="form-control" name='n_keterampilan' id='n_keterampilan_modal' placeholder="Masukkan Nilai Keterampilan..." />
						</div>
						<div class="form-group">
							<label for="n_keterampilan_pre"> Predikat Nilai Keterampilan </label>                  
							<input type="text" class="form-control" name='n_keterampilan_pre' id='n_keterampilan_pre_modals' placeholder="Masukkan Nilai Keterampilan..." disabled />
						</div>
					</div>					
					<?php echo form_close(); ?>
				</div>
			  <div class="modal-footer">				
				<button type="button" class="btn btn-primary" id='simpan_nilai' data-dismiss="modal">Simpan Nilai</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		<!--Modals Nilai-->
		
		
    </section>
    <!-- /.content -->
	
	<script src='<?= base_url("assets/web/js/pengajar/daftar_nilai_siswa.js"); ?>'></script>    
    
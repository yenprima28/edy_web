<!--header-->
<?php include_once 'includes/header.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php include_once 'includes/header-top.php'; ?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <?php include_once 'includes/side_menu.php'; ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Guru
        <small>Anda Coeg Sekalehh...:v</small>
      </h1>      
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title_dashboard; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!-- <form role="form"> -->
						
			<?php echo validation_errors(); ?>			
			
			<?php echo $error;?>
			
			 <?php echo form_open('guru/insert_nilai_siswa_proses'); ?> 
              <div class="box-body">
                <div class="form-group">
                  <label for="nis"> NIS </label>
                  <input type="text" class="form-control" name='nis' value='<?= $nis; ?>' placeholder="Masukkan NIS..." disabled>
                  <input type="hidden" name='nis_val' value='<?= $nis; ?>'>
                </div>
                <div class="form-group">
                  <label for="nama"> Nama Lengkap </label>                  
				  <input type="text" class="form-control" name='nama' value='<?php echo $siswa[0]->nama_lengkap; ?>' placeholder="Masukkan Nama Lengkap..." disabled>				  				  
				</div>				
                <div class="form-group">
                  <label for="n_pengetahuan"> Nilai Pengetahuan </label>                  
				  <input type="text" class="form-control" name='n_pengetahuan' value='<?php echo $siswa[0]->pengetahuan_angka; ?>' placeholder="Masukkan Nilai Pengetahuan...">
				</div>				
				<div class="form-group">
                  <label for="n_keterampilan"> Nilai Keterampilan </label>                  
				  <input type="text" class="form-control" name='n_keterampilan' value='<?php echo $siswa[0]->keterampilan_angka; ?>' placeholder="Masukkan Nilai Keterampilan...">
				</div>				
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name='submit' class="btn btn-primary"> Simpan Data </button>
              </div>
            <!-- </form> -->
			<?php echo form_close(); ?>
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include_once 'includes/footer.php'; ?>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

  <?php include_once 'includes/footer-js.php'; ?>
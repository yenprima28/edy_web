    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">          
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $title; ?></h3>
            </div>
            <!-- /.box-header -->
            
			<div class="box-body">
			  <!--
			  <p>
				<a href='<?php echo base_url(); ?>admin/tambah_siswa_perkelas' class='btn btn-primary'> Tambah Siswa </a>
			  </p>
			  -->
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>NIS</th>
                  <th>Nama</th>                  
                </tr>
				<?php foreach($siswa as $ls): ?>
                
				<tr>											
                  <td><?php echo $page + $no++; ?></td>
                  <td><?php echo $ls->nis; ?></td>
                  <td><?php echo $ls->nama_lengkap; ?></td>
                </tr>
				
				<?php endforeach; ?>
              </table>
            </div>
			
			
			
            <!-- /.box-body -->
            <div class="box-footer clearfix">
				<nav aria-label="Page navigation">
					<ul class="pagination">				
						<?php echo $links; ?>
					</ul>
				</nav>
            </div>          
			
          </div>
          <!-- /.box -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
function runJquery()
{	
    $('#mapel').change(function()
	{			
		var mapel = $('#mapel').val();			
		
		$('#kelas').empty();			
		$.post('get_daftar_kelas_pengajar_mapel',
		{
			mapel: mapel				
		},
		function(data)
		{
			var parseJsonData = $.parseJSON(data);				
			console.log(parseJsonData);								
			
			$.each(parseJsonData.kelas, function(k, v)
			{					
				$('#kelas').append('<option value="'+v.kd_kelas+'">'+v.kelas+'</option>');
				//$('#kelas').append('asd');
			});
		});
	});
	
	function status_pengisian_nilai()
	{
		var id_mapel = $('#mapel').val();
		var kelas = $('#kelas').val();
		var tahun = '2017';
		var semester = '1';
					
		$.post('status_pengisian_nilai',
		{
			'id_mapel' : id_mapel,
			'kelas' : kelas,				
			'tahun' : tahun,
			'semester' : semester
			
			// 'id_mapel' : 'M001',
			// 'kelas' : 'K001',
			// 'tahun' : '2017',
			// 'semester' : '1'
			
		},
		function(data)
		{
			//console.log(data);
			
			var jsonParse = $.parseJSON(data);
			var jlh_nilai_siswa_terisi_by_kelas = jsonParse.status_pengisian;				
			
			var jlh_siswa_kelas = null;
			var persentase_pengisian = 0;
			
			$.post('count_siswa_kelas',
			{					
				'kelas' : kelas
			},
			function(data1)
			{
				//console.log(data1);
				var jsonParse1 = $.parseJSON(data1);
				
				jlh_siswa_kelas = jsonParse1.count_siswa_perkelas;
				
				persentase_pengisian = (jlh_nilai_siswa_terisi_by_kelas / jlh_siswa_kelas) * 100;				
				
				$('.status').text(' Status Pengisian Data : '+persentase_pengisian+'%');
				
				//console.log(persentase_pengisian);
			});
					
		});
	}
	
	function tampil_siswa()
	{
		var mapel = $('#mapel').val();
		var kelas = $('#kelas').val();
		
		var kelas_nama = $('#kelas').text();
		$('.judul_tabel').text('Daftar Siswa Kelas : '+kelas_nama);		
		
        var tampung = '';        
		
		$.post('get_daftar_siswa_mapel_kelas_no_nilai',
		{
			//mapel: mapel,
			kelas: kelas
		},
		function(data)
		{
            // console.log(data);
			var parseJsonData = $.parseJSON(data);
			// console.log(parseJsonData);
							
			$('#daftar_siswa').empty();
			
			var no = 1;
            
            tampung = parseJsonData;
            
			$.each(parseJsonData.siswa, function(k, v)
			{																					
				//tampilkan daftar siswa disini		                
				//mencari nilai siswa 
				$.post('get_nilai_siswa', 
				{
					mapel: mapel,
					nis: v.nis
				}, 
				function(data)
				{
					console.log(data);
					nilai_siswa = $.parseJSON(data);                    
				});	
									
			});
		}).done( function(data)
        {            
            console.log(data);
			var parseJsonData = $.parseJSON(data);
			console.log(parseJsonData);
							
			$('#daftar_siswa').empty();
			
			var no = 1;
            
            tampung = parseJsonData;
            
			$.each(parseJsonData.siswa, function(k, v)
			{																					
				//tampilkan daftar siswa disini		                
				//mencari nilai siswa 
				$.post('get_nilai_siswa', 
				{
					mapel: mapel,
					nis: v.nis
				}, 
				function(data)
				{
					console.log(data);
					nilai_siswa = $.parseJSON(data);
                    
					if(nilai_siswa.nilai_siswa == null)
					{
						$('#daftar_siswa').append
						(
						'<tr>'+
							'<td>'+(no++)+'</td>'+
							'<td>'+v.nis+'</td>'+
							'<td>'+v.nama+'</td>'+
							'<td>'+v.kelas+'</td>'+							
							'<td>- | -</td>'+
							'<td>- | -</td>'+								
							'<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Nilai</a></td>'+
						'<tr>'
						);
					}
					else 
					{
						$('#daftar_siswa').append
						(
						'<tr>'+
							'<td>'+(no++)+'</td>'+
							'<td>'+v.nis+'</td>'+
							'<td>'+v.nama+'</td>'+
							'<td>'+v.kelas+'</td>'+							
							'<td>'+nilai_siswa.nilai_siswa.pengetahuan_grade+' | '+nilai_siswa.nilai_siswa.pengetahuan_angka+'</td>'+
							'<td>'+nilai_siswa.nilai_siswa.keterampilan_grade+' | '+nilai_siswa.nilai_siswa.keterampilan_angka+'</td>'+
							'<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Nilai</a></td>'+
						'<tr>'
						);
					}						
					
				});	
				
			});
            
            $('#list_siswa').DataTable( { data: tampung} );
        });
	}
	
	$('#tampil_siswa').click(function(e)
	{
		e.preventDefault();
		console.log('submit');
        
		tampil_siswa();
        
        $('#list_siswa').DataTable();
        
		status_pengisian_nilai();
	});		
	
	//$('#simpan_nilai').click(function()
	$(document).on('click','#linkData', function()
	{
		var nis = $(this).data('nis');			
		
		console.log(nis);
		
		$('#nis_modal').val(nis);
		
		$.post('get_nama_siswa', 
		{
			nis: nis
		}, 
		function(data)
		{
			//console.log(data);
			var parseJsonData = $.parseJSON(data);
			
			//console.log(parseJsonData.siswa.nama);
			
			$('#nama_lengkap_modal').val(parseJsonData.siswa.nama);
			
			//mendapatkan nilai terbaru siswa ke modals
			var mapel = $('#mapel').val();
			$.post('get_nilai_siswa_by_nis', 
			{
				nis: nis,
				id_mapel: mapel
			}, 
			function(data1)
			{
				//console.log(data);
				var parseJsonData1 = $.parseJSON(data1);
				//console.log(parseJsonData1.nilai_siswa_nis);
				
				$('#n_pengetahuan_modal').val(parseJsonData1.nilai_siswa_nis.pengetahuan_angka);
				$('#n_pengetahuan_pre_modal').val(parseJsonData1.nilai_siswa_nis.pengetahuan_grade);
				$('#n_keterampilan_modal').val(parseJsonData1.nilai_siswa_nis.keterampilan_angka);
				$('#n_keterampilan_pre_modals').val(parseJsonData1.nilai_siswa_nis.keterampilan_grade);
			});
		});
	});
	
	//Membuat grade nilai berdasarkan inputan user
	//nilai pengetahuan
	$('#n_pengetahuan_modal').change(function()
	{
		var nilai = parseInt($('#n_pengetahuan_modal').val());
		var grade = '';
		
		//console.log(nilai);
		
		if(nilai >= 90)
		{
			grade = 'A';
		} 
		else if(nilai >= 80)
		{
			grade = 'B';
		}
		else if(nilai >= 75)
		{
			grade = 'C';
		} 
		else
		{
			grade = 'D';
		}
		
		$('#n_pengetahuan_pre_modal').val(grade);
	});
	
	//nilai keterampilan
	$('#n_keterampilan_modal').change(function()
	{
		var nilai = parseInt($('#n_keterampilan_modal').val());
		var grade = '';
		
		//console.log(nilai);
		
		if(nilai >= 90)
		{
			grade = 'A';
		} 
		else if(nilai >= 80)
		{
			grade = 'B';
		}
		else if(nilai >= 75)
		{
			grade = 'C';
		} 
		else
		{
			grade = 'D';
		}
		
		$('#n_keterampilan_pre_modals').val(grade);
	});
	
	//Untuk menyimpan nilai siswa pada modals
	$('#simpan_nilai').click(function(e)		
	{
		e.preventDefault();
		
		//var data = $('#formInputNilai').serialize();			
		//console.log(data);
		
		var nis = $('#nis_modal').val();
		var id_mapel = $('#mapel').val();
		var pengetahuan_grade = $('#n_pengetahuan_pre_modal').val();			
		var keterampilan_grade = $('#n_keterampilan_pre_modals').val();
		var pengetahuan_angka = $('#n_pengetahuan_modal').val();
		var keterampilan_angka = $('#n_keterampilan_modal').val();
		var tahun = '2017';
		var semester = '1';
		
		
		// console.log(nis);
		// console.log(id_mapel);
		// console.log(pengetahuan_angka);
		// console.log(pengetahuan_grade);
		// console.log(keterampilan_angka);
		// console.log(keterampilan_grade);
		// console.log(tahun);
		// console.log(semester);
					
		$.post('input_nilai_siswa_proses',
		{
			'id_mapel' : id_mapel,
			'nis' : nis,
			'pengetahuan_angka' : pengetahuan_angka,
			'pengetahuan_grade' : pengetahuan_grade,
			'keterampilan_angka' : keterampilan_angka,
			'keterampilan_grade' : keterampilan_grade,
			'tahun' : tahun,
			'semester' : semester
			
		},
		function(data)
		{
			
		});
		
		tampil_siswa();
		
		alert('nilai_tersimpan');
	});
	    
}
	
function runJquery()
{    
    //console.log('asd');
    
    var data_tahun_smt = '';
    
    function get_list_siswa()
    {
        
        $.get('get_data_siswa_diampu', 
           function(data)
           {
                console.log(data);
               var jsonParse = $.parseJSON(data);
               
               var no = 1;
               
               //console.log(jsonParse);
               
               $.each(jsonParse, function(k, v)
               {     
                    //console.log(v.nis);
                    
                    //data absensi berdasarkan nis masing" siswa
                    $.post('get_absensi_siswa', 
                    {
                        nis: v.nis
                    }, 
                    function(data_absensi)
                    {
                        //console.log(data_absensi);
                        
                        var jsonParseAbsensi = $.parseJSON(data_absensi);
                        
                        // console.log(jsonParseAbsensi);                        
                        
                        if( jsonParseAbsensi[0] == null)
                        {
                            $('#daftar_siswa').append
                            (
                                '<tr>'+
                                    '<td>'+(no++)+'</td>'+
                                    '<td>'+v.nama+'</td>'+
                                    '<td>'+v.kelas+'</td>'+
                                    '<td>-</td>'+
                                    '<td>-</td>'+
                                    '<td>-</td>'+                                    
                                    '<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Absensi</a></td>'+
                                '<tr>'
                            );
                        }
                        else
                        {
                            $('#daftar_siswa').append
                            (
                                '<tr>'+
                                    '<td>'+(no++)+'</td>'+
                                    '<td>'+v.nama+'</td>'+
                                    '<td>'+v.kelas+'</td>'+
                                    '<td>'+jsonParseAbsensi[0].ijin+'</td>'+
                                    '<td>'+jsonParseAbsensi[0].sakit+'</td>'+
                                    '<td>'+jsonParseAbsensi[0].alpa+'</td>'+
                                    '<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Absensi</a></td>'+
                                '<tr>'
                            );
                        }
                        
                    });
                    
               });
           }
        );        
    }
    
    function get_semester_aktif()
    {        
        $.get('get_smt_aktif', 
            function(data_smt)
            {
                //console.log(data);
                data_tahun_smt = data_smt;
            }
        );        
    }    
    
    function insert_absensi()
    {                
        var nis = $('#nis_modal_id').val();        
        var ijin = $('#ijin_id').val();
        var sakit = $('#sakit_id').val();
        var alpa = $('#alpa_id').val();
        
        $.post('tambah_data_absensi',
        {
            nis: nis,
            ijin: ijin,
            sakit: sakit,
            alpa: alpa
        }, 
        function(data)
        {
            console.log(data);
        });
    }   
    
    $('form').submit(function( e )
    {
        e.preventDefault();
        insert_absensi();  
        
        $('#daftar_siswa').empty();
        
        get_list_siswa();
    });
    
    //Main function
    $(document).on('click','#linkData', function()
    {
        var nis = $(this).data('nis');			
			
		console.log(nis);
		
		get_semester_aktif();
        
        var jsonParseTahunSmt = $.parseJSON(data_tahun_smt);
        
        console.log(jsonParseTahunSmt);
        
        $('#tahun_id').val(jsonParseTahunSmt.tahun);
        $('#smt_id').val(jsonParseTahunSmt.smt);
        
        $.post('get_data_siswa', 
            {
                nis: nis
            }, 
            function(data)
            {
                // console.log(data);
                var jsonParseDataSiswa = $.parseJSON( data );
                
                console.log(jsonParseDataSiswa);
                
                $('#nis_modal_id').val(jsonParseDataSiswa[0].nis);
                $('#nama_lengkap_id').val(jsonParseDataSiswa[0].nama);
            }
        );
                
    });
	
    //Constructor
    get_list_siswa();
    get_semester_aktif();
}

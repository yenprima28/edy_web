function runJquery()
{    
    // console.log('asd');
    
    var data_tahun_smt = '';
    
    function get_list_siswa()
    {
        
        $.get('get_data_siswa_diampu', 
           function(data)
           {
               console.log(data);
               var jsonParse = $.parseJSON(data);
               
               var no = 1;
               
               //console.log(jsonParse);
               
               $.each(jsonParse, function(k, v)
               {     
                    //console.log(v.nis);
                    
                    //data absensi berdasarkan nis masing" siswa
                    $.post('get_ekskul_siswa', 
                    {
                        nis: v.nis
                    }, 
                    function(data_absensi)
                    {
                        //console.log(data_absensi);
                        
                        var jsonParseAbsensi = $.parseJSON(data_absensi);
                        
                        // console.log(jsonParseAbsensi[0]);
                        
                        if( jsonParseAbsensi[0] == null)
                        {
                            $('#daftar_siswa').append
                            (
                                '<tr>'+
                                    '<td>'+(no++)+'</td>'+
                                    '<td>'+v.nama+'</td>'+                                    
                                    '<td>-</td>'+                                    
                                    '<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Ekskul</a></td>'+
                                '<tr>'
                            );
                        }
                        else
                        {
                            $('#daftar_siswa').append
                            (
                                '<tr>'+
                                    '<td>'+(no++)+'</td>'+
                                    '<td>'+v.nama+'</td>'+
                                    '<td>'+jsonParseAbsensi[0].id_ekskul+'</td>'+                                    
                                    '<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Ekskul</a></td>'+
                                '<tr>'
                            );
                        }
                        
                    });
                    
               });
           }
        );        
    }
    
    function get_semester_aktif()
    {        
        $.get('get_smt_aktif', 
            function(data_smt)
            {
                //console.log(data);
                data_tahun_smt = data_smt;
            }
        );        
    }
    
    $('form').submit(function( e )
    {
        e.preventDefault();        
        
        var id_ekskul1 = $('#ekskul1_id').val();
        var id_ekskul2 = $('#ekskul2_id').val();
        var nis = $('#nis_modal_id').val();
        
        var id_ekskul_combine;
        
        if(id_ekskul2 == 0)
        {
            id_ekskul_combine = id_ekskul1;
        } else {
            id_ekskul_combine = id_ekskul1 + ';' + id_ekskul2;
        }
        
        $.post('tambah_data_ekskul', 
        {
            'nis' : nis,
            'id_ekskul' : id_ekskul_combine
        },
        function(data)
        {
            console.log(data);
        });
        
        // console.log(id_ekskul_combine);
        
        console.log('Submitted');
    });
    
    //Ekskul function
    function get_ekskul_list()
    {
        $.get('get_all_ekskul', function(data)
        {
            // console.log(data);
            var jsonParseEkskulList = $.parseJSON(data);
            
            console.log(jsonParseEkskulList);
            
            $('#ekskul1_id').append
            (
                '<option value="0"> ~Pilih~ </option>'
            )
            
            $('#ekskul2_id').append
            (
                '<option value="0"> ~Pilih~ </option>'
            )
            
            $.each(jsonParseEkskulList, function(k, v)
            {
                
                $('#ekskul1_id').append
                (
                    '<option value="'+v.id_ekskul+'">'+v.ekskul+'</option>'
                )
                
                $('#ekskul2_id').append
                (
                    '<option value="'+v.id_ekskul+'">'+v.ekskul+'</option>'
                )
                
            });            
        });
        
    }
    
    
    //Main function
    $(document).on('click','#linkData', function()
    {
        var nis = $(this).data('nis');        
        
		console.log(nis);
		
		get_semester_aktif();
        
        var jsonParseTahunSmt = $.parseJSON(data_tahun_smt);
        
        console.log(jsonParseTahunSmt);
        
        $('#tahun_id').val(jsonParseTahunSmt.tahun);
        $('#smt_id').val(jsonParseTahunSmt.smt);
        
        $.post('get_data_siswa', 
            {
                nis: nis
            }, 
            function(data)
            {
                // console.log(data);
                var jsonParseDataSiswa = $.parseJSON( data );
                
                console.log(jsonParseDataSiswa);
                
                $('#nis_modal_id').val(jsonParseDataSiswa[0].nis);
                $('#nama_lengkap_id').val(jsonParseDataSiswa[0].nama);
            }
        );
                
    });
	
    //Constructor
    get_list_siswa();
    get_semester_aktif();
    get_ekskul_list();
}

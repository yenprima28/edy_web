function runJquery()
{    
    // console.log('asd');
    
    var data_tahun_smt = '';
    
    function get_list_siswa()
    {
        
        $.get('get_data_siswa_diampu', 
            function(data)
            {
                // console.log(data);
                var jsonParse = $.parseJSON(data);
                
                var no = 1;
                
                // console.log(jsonParse);
                
                $.each(jsonParse, function(k, v)
                {     
                    // console.log(v.nis);                    
                    
                    $.post('get_prestasi_siswa', 
                        {
                            nis: v.nis
                        }, 
                        function(data1)
                        {                
                            var jsonParse2 = $.parseJSON(data1);
                            
                            if( jsonParse2[0] == null ) 
                            {
                                $('#daftar_prestasi').append
                                (
                                    '<tr>'+
                                        '<td>'+(no++)+'</td>'+
                                        '<td>'+v.nama+'</td>'+
                                        '<td>-</td>'+                                        
                                        '<td>-</td>'+                                        
                                        '<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Prestasi</a></td>'+
                                    '<tr>'
                                );
                            }
                            else
                            {
                                $('#daftar_prestasi').append
                                (
                                    '<tr>'+
                                        '<td>'+(no++)+'</td>'+
                                        '<td>'+v.nama+'</td>'+
                                        '<td>'+jsonParse2[0].prestasi+'</td>'+
                                        '<td>'+jsonParse2[0].detail_prestasi+'</td>'+                                        
                                        '<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Prestasi</a></td>'+
                                    '<tr>'
                                );
                            }
                        }
                    );
                                        
                });
            }
        );        
    }
    
    function get_prestasi_siswa(nis)
    {        
        $.post('get_prestasi_siswa', 
            {
                nis: nis
            }, 
            function(data)
            {                
                if(data !== true) 
                    console.log('asd');
                else
                console.log(data);                
            }
        );
    }
    
    function get_semester_aktif()
    {        
        $.get('get_smt_aktif', 
            function(data_smt)
            {
                //console.log(data);
                data_tahun_smt = data_smt;
            }
        );        
    }      
    
    function insert_prestasi()
    {
        var nis = $('#nis_modal_id').val();
        var prestasi = $('#prestasi_id').val();
        var d_prestasi = $('#d_prestasi_id').val();        
        
        $.post('tambah_data_prestasi', 
            {
                nis: nis,
                prestasi: prestasi,
                detail_prestasi: d_prestasi
            }, 
            function(data)
            {
                console.log(data);
            }
        );
    }
    
    $('form').submit(function( e )
    {
        e.preventDefault();       
        
        insert_prestasi();
        $('#daftar_prestasi').empty();
        
        get_list_siswa();
    });
    
    //Main function
    $(document).on('click','#linkData', function()
    {
        var nis = $(this).data('nis');			
			
		console.log(nis);
		
		get_semester_aktif();
        
        var jsonParseTahunSmt = $.parseJSON(data_tahun_smt);
        
        console.log(jsonParseTahunSmt);
        
        $('#tahun_id').val(jsonParseTahunSmt.tahun);
        $('#smt_id').val(jsonParseTahunSmt.smt);
        
        $.post('get_data_siswa', 
            {
                nis: nis
            }, 
            function(data)
            {
                // console.log(data);
                var jsonParseDataSiswa = $.parseJSON( data );
                
                console.log(jsonParseDataSiswa);
                
                $('#nis_modal_id').val(jsonParseDataSiswa[0].nis);
                $('#nama_lengkap_id').val(jsonParseDataSiswa[0].nama);
            }
        );
                
    });
	
    //Constructor
    get_list_siswa();
    get_semester_aktif();
}

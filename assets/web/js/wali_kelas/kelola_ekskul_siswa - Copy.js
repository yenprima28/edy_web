function runJquery()
{    
    console.log('asd');
    
    var data_tahun_smt = '';
    
    function get_list_siswa()
    {
        
        $.get('get_data_siswa_diampu', 
           function(data)
           {
               console.log(data);
               var jsonParse = $.parseJSON(data);
               
               var no = 1;
               
               //console.log(jsonParse);
               
               $.each(jsonParse, function(k, v)
               {     
                    //console.log(v.nis);
                    
                    //data absensi berdasarkan nis masing" siswa
                    $.post('get_absensi_siswa', 
                    {
                        nis: v.nis
                    }, 
                    function(data_absensi)
                    {
                        //console.log(data_absensi);
                        
                        var jsonParseAbsensi = $.parseJSON(data_absensi);
                        
                        // console.log(jsonParseAbsensi);                        
                        
                        if( jsonParseAbsensi[0] == null)
                        {
                            $('#daftar_siswa').append
                            (
                                '<tr>'+
                                    '<td>'+(no++)+'</td>'+
                                    '<td>'+v.nama+'</td>'+                                    
                                    '<td>-</td>'+                                    
                                    '<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Absensi</a></td>'+
                                '<tr>'
                            );
                        }
                        else
                        {
                            $('#daftar_siswa').append
                            (
                                '<tr>'+
                                    '<td>'+(no++)+'</td>'+
                                    '<td>'+v.nama+'</td>'+
                                    '<td>'+v.kelas+'</td>'+                                    
                                    '<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Absensi</a></td>'+
                                '<tr>'
                            );
                        }
                        
                    });
                    
               });
           }
        );        
    }
    
    function get_semester_aktif()
    {        
        $.get('get_smt_aktif', 
            function(data_smt)
            {
                //console.log(data);
                data_tahun_smt = data_smt;
            }
        );        
    }
    
    $('form').submit(function( e )
    {
        e.preventDefault();        
        
        var asd = $( 'form input:checkbox:checked' );
        console.log(asd);
        
        var id_eks = '';
        var counter1 = 0;
        var counter2 = 0;
        
        //counter item
        $.each(asd, function(k, v){
            counter1++;
        });
        
        $.each(asd, function(k, v)
        {
            // console.log(v.value);            
            counter2++;
            
            if( counter2 < counter1 )
            {
                id_eks += v.value;
                id_eks += ';';                
            } 
            else
            {
                id_eks += v.value;
            }            
        });
                
        console.log(id_eks);
        
        // $('#daftar_siswa').empty();
        
        // get_list_siswa();
    });
    
    //Ekskul function
    function get_ekskul_list()
    {
        $.get('get_all_ekskul', function(data)
        {
            // console.log(data);
            var jsonParseEkskulList = $.parseJSON(data);
            
            console.log(jsonParseEkskulList);
            
            $.each(jsonParseEkskulList, function(k, v)
            {
                // console.log(v.id_ekskul);
                /*
                $('#chkbox').append
                (
                    '<input type="checkbox" name="ekskul[]" id="ekskul_list_id" value="'+v.id_ekskul+'" /> '+v.ekskul+' <br/>'
                );
                */                
                
                $('#ekskul1_id').append
                (
                    '<option> ~Pilih~ </option>'
                )
                
            });            
        });
        
    }
    
    
    //Main function
    $(document).on('click','#linkData', function()
    {
        var nis = $(this).data('nis');			
		
        alert('asd');
        
		console.log(nis);
		
		get_semester_aktif();
        
        var jsonParseTahunSmt = $.parseJSON(data_tahun_smt);
        
        console.log(jsonParseTahunSmt);
        
        $('#tahun_id').val(jsonParseTahunSmt.tahun);
        $('#smt_id').val(jsonParseTahunSmt.smt);
        
        $.post('get_data_siswa', 
            {
                nis: nis
            }, 
            function(data)
            {
                // console.log(data);
                var jsonParseDataSiswa = $.parseJSON( data );
                
                console.log(jsonParseDataSiswa);
                
                $('#nis_modal_id').val(jsonParseDataSiswa[0].nis);
                $('#nama_lengkap_id').val(jsonParseDataSiswa[0].nama);
            }
        );
                
    });
	
    //Constructor
    get_list_siswa();
    get_semester_aktif();
    get_ekskul_list();
}

function runJquery()
{
	var data_siswa;
    
	$.get('tampil_data_siswa_ampu', 
	function(data)
	{
		console.log(data);
		
		parseJsonData = $.parseJSON(data);
		var no = 1;	
		
		console.log(parseJsonData);
		
		$.each(parseJsonData.data_siswa, function(k, v)
		{
            // if( data != false )
            // {
                $('#daftar_siswa').append
                (
                    '<tr>'+
                        '<td>'+(no++)+'</td>'+
                        '<td>'+v.nis+'</td>'+
                        '<td>'+v.nama+'</td>'+
                        '<td>'+v.kelas+'</td>'+							
                        '<td><a href="#myModal" class="btn btn-primary" data-nis="'+v.nis+'" id="linkData" data-toggle="modal">Atur Kelas</a></td>'+
                    '<tr>'
                );
            // }
            // else
            // {
                // $('#daftar_siswa').append
                // (
                    // '<p> Data Tidak ditemukan </p>'
                // );
            // }
		});
	});
	
	$(document).on('click','#linkData', function()
	{
		var nis = $(this).data('nis');
		
		console.log(nis);
		
		$('#nis_modal_id').val(nis);
		        
        $('#kelas_baru_id').empty();
        
		$.get('tampil_data_siswa_ampu',
		function(data)
		{
			//console.log(data);
			
            var parseJsonData = $.parseJSON(data);				
						
			$('#nama_lengkap_id').val(parseJsonData.data_siswa[0].nama);
			$('#kelas_skr_id').val(parseJsonData.data_siswa[0].kelas);
            
			$.get('get_data_kelas',
			function(data_kelas)
			{
				console.log(data_kelas);
				var parseJsonDataKelas = $.parseJSON(data_kelas);				                
                
				$.each(parseJsonDataKelas.daftar_kelas, function(k, v)
                {                        
                    $('#kelas_baru_id').append(
                        '<option value="'+v.kd_kelas+'">'+v.kelas+'</option>'
                    );
                });
				
			});
		});
	});
    
    $('#simpan_nilai_id').click(function()
    {
       var nis = $('#nis_modal_id').val();
       var kelas_baru = $('#kelas_baru_id').val();
       
       $.post('naikkan_kelas_siswa', 
       {
           nis: nis,
           kelas_baru: kelas_baru
       },
       function(data)
       {
           console.log(data);
       });
    });
	
}
